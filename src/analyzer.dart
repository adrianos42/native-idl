import 'dart:typed_data';

import 'parser.dart';
import 'attributes.dart';

class IdlObject {
  IdlObject();

  void _resolveImports(IdlFile idlFile) {}

  void _setEnums(IdlFile idlFile) {
    idlFile.enums.forEach((name, value) {
      assert(name == value.name);
      if (!_enums.containsKey(value.name)) {
        throw Exception('Error creating enum type: ${value.name}');
      }

      final fieldBuilder = idlObjectFieldsFactory.createEnumFieldBuilder(this);
      final fields = <IdlEnumField>[];

      value.fields.forEach((fieldObject) {
        final field = fieldBuilder.fromObjectField(fieldObject);
        if (field != null) fields.add(field);
      });

      _enums[value.name] = IdlEnum(value.name, fields);
    });
  }

  void _setStructs(IdlFile idlFile) {
    idlFile.structs.forEach((name, value) {
      assert(value.name == name);
      if (!_structs.containsKey(value.name)) {
        throw Exception('Error creating struct type: ${value.name}.');
      }

      final fieldBuilder =
          idlObjectFieldsFactory.createStructFieldBuilder(this);
      final fields = <IdlStructField>[];

      value.fields.forEach((fieldObject) {
        final field = fieldBuilder.fromObjectField(fieldObject);
        if (field == null) {
          throw Exception('Invalid field for struct during compilation.');
        }
        fields.add(field);
      });

      _structs[value.name] = IdlStruct(value.name, fields);
    });
  }

  void _setInterfaces(IdlFile idlFile) {
    idlFile.interfaces.forEach((name, value) {
      assert(value.name == name);
      if (!_interfaces.containsKey(value.name)) {
        throw Exception('Error creating interface type: ${value.name}');
      }

      final fieldBuilder =
          idlObjectFieldsFactory.createInterfaceFieldBuilder(this);
      final fields = <IdlInterfaceField>[];

      value.fields.forEach((fieldObject) {
        final field = fieldBuilder.fromObjectField(fieldObject);
        if (field == null) {
          throw Exception('Invliad field for interface during compilation.');
        }
        fields.add(field);
      });

      _interfaces[value.name] = IdlInterface(value.name, fields);
    });
  }

  void _setStrings(IdlFile idlFile) {
    idlFile.strings.forEach((name, value) {
      assert(value.name == name);
      if (!_strings.containsKey(value.name)) {
        throw Exception('Error creating string type: ${value.name}');
      }

      final fields = <IdlConstField>[];

      value.fields.forEach((key, value) {
        fields.add(IdlConstField(key, value));
      });

      _strings[value.name] = IdlConst(value.name, fields);
    });
  }

  void _setInts(IdlFile idlFile) {
    idlFile.ints.forEach((name, value) {
      assert(value.name == name);
      if (!_ints.containsKey(value.name)) {
        throw Exception('Error creating int type: ${value.name}');
      }

      final fields = <IdlConstField>[];

      value.fields.forEach((key, value) {
        fields.add(IdlConstField(key, value));
      });

      _ints[value.name] = IdlConst(value.name, fields);
    });
  }

  void _setFloats(IdlFile idlFile) {
    idlFile.floats.forEach((name, value) {
      assert(value.name == name);
      if (!_floats.containsKey(value.name)) {
        throw Exception('Error creating float type: ${value.name}');
      }

      final fields = <IdlConstField>[];

      value.fields.forEach((key, value) {
        fields.add(IdlConstField(key, value));
      });

      _floats[value.name] = IdlConst(value.name, fields);
    });
  }

  void _addEnums(IdlFile idlFile) {
    idlFile.enums.forEach((name, value) {
      assert(name == value.name);
      if (_enums.containsKey(value.name)) {
        throw Exception('Duplicate enum type: ${value.name}');
      }
      if (_interfaces.containsKey(value.name) ||
          _structs.containsKey(value.name) ||
          _strings.containsKey(value.name) ||
          _ints.containsKey(value.name) ||
          _floats.containsKey(value.name)) {
        throw Exception('Type alrady exists: ${value.name}');
      }

      _enums[value.name] = null;
    });
  }

  void _addStructs(IdlFile idlFile) {
    idlFile.structs.forEach((name, value) {
      assert(value.name == name);
      if (_structs.containsKey(value.name)) {
        throw Exception('Duplicate struct type: ${value.name}.');
      }
      if (_interfaces.containsKey(value.name) ||
          _enums.containsKey(value.name) ||
          _strings.containsKey(value.name) ||
          _ints.containsKey(value.name) ||
          _floats.containsKey(value.name)) {
        throw Exception('Type alrady exists: ${value.name}');
      }

      _structs[value.name] = null;
    });
  }

  void _addInterfaces(IdlFile idlFile) {
    idlFile.interfaces.forEach((name, value) {
      assert(value.name == name);
      if (_interfaces.containsKey(value.name)) {
        throw Exception('Duplicate interface type: ${value.name}');
      }
      if (_enums.containsKey(value.name) ||
          _structs.containsKey(value.name) ||
          _strings.containsKey(value.name) ||
          _ints.containsKey(value.name) ||
          _floats.containsKey(value.name)) {
        throw Exception('Type already exists: ${value.name}');
      }

      _interfaces[value.name] = null;
    });
  }

  void _addStrings(IdlFile idlFile) {
    idlFile.strings.forEach((name, value) {
      assert(value.name == name);
      if (_strings.containsKey(value.name)) {
        throw Exception('Duplicate string type: ${value.name}');
      }
      if (_enums.containsKey(value.name) ||
          _structs.containsKey(value.name) ||
          _interfaces.containsKey(value.name) ||
          _ints.containsKey(value.name) ||
          _floats.containsKey(value.name)) {
        throw Exception('Type already exists: ${value.name}');
      }

      _strings[value.name] = null;
    });
  }

  void _addInts(IdlFile idlFile) {
    idlFile.ints.forEach((name, value) {
      assert(value.name == name);
      if (_ints.containsKey(value.name)) {
        throw Exception('Duplicate int type: ${value.name}');
      }
      if (_enums.containsKey(value.name) ||
          _structs.containsKey(value.name) ||
          _interfaces.containsKey(value.name) ||
          _strings.containsKey(value.name) ||
          _floats.containsKey(value.name)) {
        throw Exception('Type already exists: ${value.name}');
      }

      _ints[value.name] = null;
    });
  }

  void _addFloats(IdlFile idlFile) {
    idlFile.floats.forEach((name, value) {
      assert(value.name == name);
      if (_floats.containsKey(value.name)) {
        throw Exception('Duplicate float type: ${value.name}');
      }
      if (_enums.containsKey(value.name) ||
          _structs.containsKey(value.name) ||
          _interfaces.containsKey(value.name) ||
          _strings.containsKey(value.name) ||
          _ints.containsKey(value.name)) {
        throw Exception('Type already exists: ${value.name}');
      }

      _floats[value.name] = null;
    });
  }

  factory IdlObject.compile(IdlFile idlFile) {
    return IdlObject()
      .._name = idlFile.libraryName
      .._resolveImports(idlFile)
      .._addEnums(idlFile)
      .._addStructs(idlFile)
      .._addInterfaces(idlFile)
      .._addStrings(idlFile)
      .._addInts(idlFile)
      .._addFloats(idlFile)
      .._setEnums(idlFile)
      .._setStructs(idlFile)
      .._setInterfaces(idlFile)
      .._setStrings(idlFile)
      .._setInts(idlFile)
      .._setFloats(idlFile);
  }

  final _imports = <String, IdlObject>{};
  final _enums = <String, IdlEnum>{};
  final _structs = <String, IdlStruct>{};
  final _interfaces = <String, IdlInterface>{};
  final _strings = <String, IdlConst>{};
  final _ints = <String, IdlConst>{};
  final _floats = <String, IdlConst>{};

  bool isInterface(String value) => _interfaces.containsKey(value);
  bool isStruct(String value) => _structs.containsKey(value);
  bool isEnum(String value) => _enums.containsKey(value);
  bool isString(String value) => _strings.containsKey(value);
  bool isInt(String value) => _ints.containsKey(value);
  bool isFloat(String value) => _floats.containsKey(value);

  IdlObjectFieldsFactory _idlObjectFieldsFactory;
  IdlObjectFieldsFactory get idlObjectFieldsFactory =>
      _idlObjectFieldsFactory ?? DefaultIdlObjectFieldsFactory();

  String _name;

  String get name => _name;
  Map<String, IdlObject> get imports => _imports;
  Map<String, IdlEnum> get enums => _enums;
  Map<String, IdlStruct> get structs => _structs;
  Map<String, IdlInterface> get interfaces => _interfaces;
  Map<String, IdlConst> get strings => _strings;
  Map<String, IdlConst> get ints => _ints;
  Map<String, IdlConst> get floats => _floats;
}

abstract class IdlObjectFieldsFactory {
  AttributeFieldCompiler<IdlStructField, IdlFileStructObject>
      createStructFieldBuilder(IdlObject parent);
  AttributeFieldCompiler<IdlEnumField, IdlFileEnumObject>
      createEnumFieldBuilder(IdlObject parent);
  AttributeFieldCompiler<IdlInterfaceField, IdlFileInterfaceObject>
      createInterfaceFieldBuilder(IdlObject parent);
}

class DefaultIdlObjectFieldsFactory implements IdlObjectFieldsFactory {
  @override
  AttributeFieldCompiler<IdlStructField, IdlFileStructObject>
      createStructFieldBuilder(IdlObject parent) =>
          IdlStructFieldBuilder(parent);
  @override
  AttributeFieldCompiler<IdlEnumField, IdlFileEnumObject>
      createEnumFieldBuilder(IdlObject parent) => IdlEnumFieldBuilder();
  @override
  AttributeFieldCompiler<IdlInterfaceField, IdlFileInterfaceObject>
      createInterfaceFieldBuilder(IdlObject parent) =>
          IdlInterfaceFieldBuilder(parent);
}

class IdlInterface {
  const IdlInterface(this.name, this.fields);
  final String name;
  final List<IdlInterfaceField> fields;
}

class IdlInterfaceField {
  const IdlInterfaceField(this.fieldName, this.type, this.interfaceAttributes);
  final String fieldName;
  final IdlType type;
  final Set<IdlInterfaceAttributes> interfaceAttributes;

  @override
  String toString() {
    return '$fieldName: ${type.toString()} with ${interfaceAttributes.toString()}';
  }
}

enum IdlInterfaceAttributes {
  propertyGet,
  propertySet,
  stream,
  aFactory,
  mAsync,
  mSync,
}

class IdlInterfaceFieldBuilder
    implements
        AttributeFieldCompiler<IdlInterfaceField, IdlFileInterfaceObject> {
  @override
  IdlInterfaceField fromObjectField(IdlFileInterfaceObject fieldObject) {
    final idlAtt = <IdlInterfaceAttributes>{};

    fieldObject.attributes.attributes.forEach((atts) {
      if (atts.every((att) => att is IdlFileAttribute)) {
        atts.forEach((value) {
          var att = value as IdlFileAttribute;
          switch (att.value) {
            case 'get':
              idlAtt.add(IdlInterfaceAttributes.propertyGet);
              break;
            case 'set':
              idlAtt.add(IdlInterfaceAttributes.propertySet);
              break;
            case 'stream':
              idlAtt.add(IdlInterfaceAttributes.stream);
              break;
            case 'factory':
              idlAtt.add(IdlInterfaceAttributes.aFactory);
              break;
            case 'async':
              idlAtt.add(IdlInterfaceAttributes.mAsync);
              break;
            case 'sync':
              idlAtt.add(IdlInterfaceAttributes.mSync);
              break;
            default:
          }
        });

        if (idlAtt.contains(IdlInterfaceAttributes.stream) ||
            idlAtt.contains(IdlInterfaceAttributes.aFactory) ||
            idlAtt.contains(IdlInterfaceAttributes.mSync) ||
            idlAtt.contains(IdlInterfaceAttributes.mAsync)) {
          if (idlAtt.length > 1) {
            throw Exception('Invalid attribute usage.');
          }
        } else if (idlAtt.difference({
          IdlInterfaceAttributes.propertyGet,
          IdlInterfaceAttributes.propertySet
        }).isNotEmpty) {
          throw Exception('Invalid property attribute usage.');
        }
      }
    });

    final idlType = IdlType.fromTypeObject(
      idlObject,
      fieldObject.type,
    );

    if (idlAtt.contains(IdlInterfaceAttributes.propertySet) &&
        idlType.type == IdlTypes.nInterface) {
      throw Exception('Use of set property not supported for interfaces.');
    }

    return IdlInterfaceField(
      fieldObject.name,
      idlType,
      idlAtt,
    );
  }

  IdlInterfaceFieldBuilder(this.idlObject);
  final IdlObject idlObject;
}

class IdlStruct {
  const IdlStruct(this.name, this.fields);
  final String name;
  final List<IdlStructField> fields;
}

class IdlStructFieldBuilder
    implements AttributeFieldCompiler<IdlStructField, IdlFileStructObject> {
  @override
  IdlStructField fromObjectField(IdlFileStructObject fieldObject) {
    return IdlStructField(
      fieldObject.name,
      IdlType.fromTypeObject(
        idlObject,
        fieldObject.type,
        supportsTypeList: false,
        supportsTuple: false,
        supportsInterfaceType: false,
        supportsMethod: false,
      ),
    );
  }

  IdlStructFieldBuilder(this.idlObject);
  final IdlObject idlObject;
}

class IdlStructField {
  const IdlStructField(this.fieldName, this.type);
  final String fieldName;
  final IdlType type;
}

class IdlEnum {
  const IdlEnum(this.name, this.fields);
  final String name;
  final List<IdlEnumField> fields;
}

class IdlEnumFieldBuilder
    implements AttributeFieldCompiler<IdlEnumField, IdlFileEnumObject> {
  IdlEnumField _previousField;

  @override
  IdlEnumField fromObjectField(IdlFileEnumObject fieldObject) {
    final value = _previousField != null ? _previousField.value + 1 : 0;
    _previousField = IdlEnumField(fieldObject.name, value);
    return IdlEnumField(fieldObject.name, value);
  }
}

class IdlEnumField {
  const IdlEnumField(this.fieldName, this.value);
  final String fieldName;
  final int value;
}

class IdlType {
  const IdlType(this.type, {this.object});

  factory IdlType.fromTypeObject(
    IdlObject object,
    dynamic type, {
    bool supportsStructType = true,
    bool supportsInterfaceType = true,
    bool supportsEnumType = true,
    bool supportsTypeList = true,
    bool supportsTuple = true,
    bool supportsArray = true,
    bool supportsMethod = true,
  }) {
    if (type is IdlFileTypeName) {
      if (object.isInterface(type.value)) {
        if (supportsInterfaceType) {
          return IdlType(IdlTypes.nInterface, object: type.value);
        } else {
          throw Exception('Invalid use of interface type: ${type.value}.');
        }
      } else if (object.isStruct(type.value)) {
        if (supportsStructType) {
          return IdlType(IdlTypes.nStruct, object: type.value);
        } else {
          throw Exception('Invalid use of struct type: ${type.value}.');
        }
      } else if (object.isEnum(type.value)) {
        if (supportsEnumType) {
          return IdlType(IdlTypes.nEnum, object: type.value);
        } else {
          throw Exception('Invalid use of enum type: ${type.value}.');
        }
      } else if (object.isString(type.value)) {
        return const IdlType(IdlTypes.nString);
      } else if (object.isInt(type.value)) {
        return const IdlType(IdlTypes.nInt);
      } else if (object.isFloat(type.value)) {
        return const IdlType(IdlTypes.nFloat);
      } else {
        throw Exception('Invalid typename in idlfile: ${type.value}');
      }
    } else if (type is IdlFileNativeTypeName) {
      switch (type.value) {
        case 'int':
          return const IdlType(IdlTypes.nInt);
        case 'bool':
          return const IdlType(IdlTypes.nBool);
        case 'float':
          return const IdlType(IdlTypes.nFloat);
        case 'string':
          return const IdlType(IdlTypes.nString);
        case 'bytes':
          return const IdlType(IdlTypes.nBytes);
        default:
          throw Exception('Built-in type not supported: ${type.value}.');
      }
    } else if (type is IdlFileArrayType) {
      if (!supportsArray) {
        throw Exception('Invalid use of array: ${type.value}');
      }

      return IdlType(IdlTypes.nArray,
          object: IdlType.fromTypeObject(
            object,
            type.value,
            supportsEnumType: supportsEnumType,
            supportsStructType: supportsStructType,
            supportsInterfaceType: false,
            supportsTuple: false,
            supportsTypeList: false, // TODO
          ));
    } else if (type is IdlFileTypeMethod) {
      if (!supportsMethod) {
        throw Exception('Invalid use of method: ${type.toString()}');
      }

      return IdlType(IdlTypes.nMethod,
          object: IdlMethod(
              IdlType.fromTypeObject(
                object,
                type.argTypes,
                supportsTuple: true,
                supportsEnumType: supportsEnumType,
                supportsStructType: supportsStructType,
                supportsTypeList: supportsTypeList,
                supportsArray: false,
                supportsInterfaceType: false,
                supportsMethod: false,
              ),
              IdlType.fromTypeObject(
                object,
                type.returnType,
                supportsArray: supportsArray,
                supportsEnumType: supportsEnumType,
                supportsStructType: supportsStructType,
                supportsTypeList: supportsTypeList,
                supportsTuple: false,
                supportsInterfaceType: false,
                supportsMethod: false,
              )));
    } else if (type is IdlFileTypeList) {
      if (!supportsTypeList) {
        throw Exception('Invalid use of type list: ${type.value}');
      }

      final typeList = type.value
          .map((value) => IdlType.fromTypeObject(
                object,
                value,
                supportsEnumType: supportsEnumType,
                supportsInterfaceType: supportsInterfaceType,
                supportsStructType: supportsStructType,
                supportsTuple: supportsTuple,
                supportsArray: false,
                supportsTypeList: false,
              ))
          .toList(growable: false);
      return IdlType(IdlTypes.nTypeList, object: typeList);
    } else if (type is IdlFileTypeTuple) {
      if (!supportsTuple) {
        throw Exception('Invalid use of tuple: ${type.value}');
      }
      final tupleList = type.value.map((key, value) => MapEntry(
          key,
          IdlType.fromTypeObject(
            object,
            value,
            supportsEnumType: supportsEnumType,
            supportsStructType: supportsStructType,
            supportsInterfaceType: false, // TODO
            supportsTypeList: false,
            supportsArray: false,
          )));

      return IdlType(IdlTypes.nTuple, object: tupleList);
    } else if (type is IdlFileTypeEmpty) {
      return IdlType(IdlTypes.nEmpty, object: null);
    }

    throw Exception('Could not create type: { ${type.toString()} }');
  }

  final dynamic object;
  final IdlTypes type;

  // If it isn't an allocated value.
  bool get isPrimitive =>
      type == IdlTypes.nBool ||
      type == IdlTypes.nInt ||
      type == IdlTypes.nFloat ||
      type == IdlTypes.nEmpty ||
      type == IdlTypes.nEnum;

  @override
  String toString() {
    return 'IdlType: ${type.toString()} { ${object.toString()} }';
  }
}

enum IdlTypes {
  nInt,
  nFloat,
  nBool,
  nEnum,
  nString,
  nBytes,
  nStruct,
  nConstString,
  nConstInt,
  nMethod,
  nConstFloat,
  nInterface,
  nTypeList,
  nTuple,
  nEmpty,
  nArray,
}

class IdlConst {
  IdlConst(this.name, this.fields);

  final String name;
  final List<IdlConstField> fields;
}

class IdlConstField {
  IdlConstField(this.fieldName, this.fieldValue);
  final String fieldName;
  final String fieldValue;
}

class IdlMethod {
  IdlMethod(this.args, this.returnType);

  final IdlType args;
  final IdlType returnType;
}