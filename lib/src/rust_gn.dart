import 'lexer.dart';

class IdlRust {
  IdlRust._(this._rustIdl);

  factory IdlRust.generate(IdlObject idlObject) {
    final rustIdl = IdlLanguageServer()
      ..addEnums(idlObject.enums)
      ..addStructs(idlObject.structs)
      ..addInterfaces(idlObject.interfaces)
      ..addStrings(idlObject.strings)
      ..addInts(idlObject.ints)
      ..addFloats(idlObject.floats)
      ..finalize();
    return IdlRust._(rustIdl);
  }

  final IdlLanguageServer _rustIdl;

  @override
  String toString() => _rustIdl.asString;
}
