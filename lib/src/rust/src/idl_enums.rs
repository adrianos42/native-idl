pub enum IdlAbiError {
    Ok,
    InvalidArg,
    NullPtr,
    Abort,
    CallbackException,
    Unimplemented,
    NotAllowedOperatoin,
}

pub enum IdlInterfaceAttributes {
    PGet,
    PSet,
    MStream,
    MFactory,
    MAsync,
    MSync,
}

pub enum IdlTypes {
    NInt,
    NFloat,
    NBool,
    NEnum,
    NString,
    NBytes,
    NStruct,
    NMethod,
    NInterface,
    NTypeList,
    NTuple,
    NVoid,
    NNone,
    NArray,
}