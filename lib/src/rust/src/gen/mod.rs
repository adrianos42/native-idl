use crate::idl::*;

use proc_macro::TokenStream;

mod enums;

pub fn create_instance() -> Box<dyn IdlLanguageServer> {
    Box::new(IdlLanguageServerImpl {
        enums: None,
        floats: None,
        structs: None,
        interfaces: None,
        ints: None,
        strings: None,
        enum_module: Vec::new(),
        //        code: String::new(),
    })
}

struct IdlLanguageServerImpl {
    enums: Option<Vec<IdlEnum>>,
    structs: Option<Vec<IdlStruct>>,
    interfaces: Option<Vec<IdlInterface>>,
    strings: Option<Vec<IdlConst>>,
    ints: Option<Vec<IdlConst>>,
    floats: Option<Vec<IdlConst>>,
    enum_module: Vec<TokenStream>,
    // code: Vec<TokenStream>,
}

impl IdlLanguageServer for IdlLanguageServerImpl {
    fn add_enums(&mut self, enums: Vec<IdlEnum>) {
        self.enums = Some(enums);
    }

    fn add_structs(&mut self, structs: Vec<IdlStruct>) {
        self.structs = Some(structs);
    }

    fn add_interfaces(&mut self, interfaces: Vec<IdlInterface>) {
        self.interfaces = Some(interfaces);
    }

    fn add_strings(&mut self, strings: Vec<IdlConst>) {
        self.strings = Some(strings);
    }

    fn add_ints(&mut self, ints: Vec<IdlConst>) {
        self.ints = Some(ints);
    }

    fn add_floats(&mut self, floats: Vec<IdlConst>) {
        self.floats = Some(floats);
    }

    fn finalize(&mut self) {
        self.set_enums();
    }

    fn get_as_string(&self) -> String {
        "".to_string()
    }
}

impl IdlLanguageServerImpl {
    fn set_enums(&mut self) {
        if let Some(enums) = &self.enums {
            for enum_value in enums.iter() {
                let name = enum_value.name.to_owned();

                self.enum_module.push(quote! {
                    enum #name {
                        lala,
                        lalallala,
                    }
                }.into());
            }
        }
    }
}

#[cfg(test)]
mod test {
    #[test]
    fn mase() {
        let token = quote! {
            enum Shit {
                no,
                nweer,
            }
        };

        println!("{}", token.to_string());
    }
}
