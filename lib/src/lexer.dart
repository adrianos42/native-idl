import 'dart:typed_data';

import 'parser.dart';
import 'attributes.dart';
import 'interface.dart';

export 'interface.dart';

class IdlObject {
  IdlObject();

  void _resolveImports(IdlFile idlFile) {}

  void _setEnums(IdlFile idlFile) {
    idlFile.enums.forEach((name, value) {
      assert(name == value.name);
      if (!_enums.containsKey(value.name)) {
        throw Exception('Error creating enum type: ${value.name}');
      }

      final fieldBuilder = idlObjectFieldsFactory.createEnumFieldBuilder(this);
      final fields = <IdlEnumField>[];

      value.fields.forEach((fieldObject) {
        final field = fieldBuilder.fromObjectField(fieldObject);
        if (field != null) fields.add(field);
      });

      _enums[value.name] = IdlEnum(name: value.name, fieldNames: fields);
    });
  }

  void _setStructs(IdlFile idlFile) {
    idlFile.structs.forEach((name, value) {
      assert(value.name == name);
      if (!_structs.containsKey(value.name)) {
        throw Exception('Error creating struct type: ${value.name}.');
      }

      final fieldBuilder =
          idlObjectFieldsFactory.createStructFieldBuilder(this);
      final fields = <IdlStructField>[];

      value.fields.forEach((fieldObject) {
        final field = fieldBuilder.fromObjectField(fieldObject);
        if (field == null) {
          throw Exception('Invalid field for struct during compilation.');
        }
        fields.add(field);
      });

      _structs[value.name] = IdlStruct(name: value.name, fields: fields);
    });
  }

  void _setInterfaces(IdlFile idlFile) {
    idlFile.interfaces.forEach((name, value) {
      assert(value.name == name);
      if (!_interfaces.containsKey(value.name)) {
        throw Exception('Error creating interface type: ${value.name}');
      }

      final fieldBuilder =
          idlObjectFieldsFactory.createInterfaceFieldBuilder(this);
      final fields = <IdlInterfaceField>[];

      value.fields.forEach((fieldObject) {
        final field = fieldBuilder.fromObjectField(fieldObject);
        if (field == null) {
          throw Exception('Invliad field for interface during compilation.');
        }
        fields.add(field);
      });

      _interfaces[value.name] = IdlInterface(name: value.name, fields: fields);
    });
  }

  void _setStrings(IdlFile idlFile) {
    idlFile.strings.forEach((name, value) {
      assert(value.name == name);
      if (!_strings.containsKey(value.name)) {
        throw Exception('Error creating string type: ${value.name}');
      }

      final fields = <IdlConstField>[];

      value.fields.forEach((key, value) {
        fields.add(IdlConstField(fieldName: key, fieldValue: value));
      });

      _strings[value.name] = IdlConst(name: value.name, fields: fields);
    });
  }

  void _setInts(IdlFile idlFile) {
    idlFile.ints.forEach((name, value) {
      assert(value.name == name);
      if (!_ints.containsKey(value.name)) {
        throw Exception('Error creating int type: ${value.name}');
      }

      final fields = <IdlConstField>[];

      value.fields.forEach((key, value) {
        fields.add(IdlConstField(fieldName: key, fieldValue: value));
      });

      _ints[value.name] = IdlConst(name: value.name, fields: fields);
    });
  }

  void _setFloats(IdlFile idlFile) {
    idlFile.floats.forEach((name, value) {
      assert(value.name == name);
      if (!_floats.containsKey(value.name)) {
        throw Exception('Error creating float type: ${value.name}');
      }

      final fields = <IdlConstField>[];

      value.fields.forEach((key, value) {
        fields.add(IdlConstField(fieldName: key, fieldValue: value));
      });

      _floats[value.name] = IdlConst(name: value.name, fields: fields);
    });
  }

  void _addEnums(IdlFile idlFile) {
    idlFile.enums.forEach((name, value) {
      assert(name == value.name);
      if (_enums.containsKey(value.name)) {
        throw Exception('Duplicate enum type: ${value.name}');
      }
      if (_interfaces.containsKey(value.name) ||
          _structs.containsKey(value.name) ||
          _strings.containsKey(value.name) ||
          _ints.containsKey(value.name) ||
          _floats.containsKey(value.name)) {
        throw Exception('Type alrady exists: ${value.name}');
      }

      _enums[value.name] = null;
    });
  }

  void _addStructs(IdlFile idlFile) {
    idlFile.structs.forEach((name, value) {
      assert(value.name == name);
      if (_structs.containsKey(value.name)) {
        throw Exception('Duplicate struct type: ${value.name}.');
      }
      if (_interfaces.containsKey(value.name) ||
          _enums.containsKey(value.name) ||
          _strings.containsKey(value.name) ||
          _ints.containsKey(value.name) ||
          _floats.containsKey(value.name)) {
        throw Exception('Type alrady exists: ${value.name}');
      }

      _structs[value.name] = null;
    });
  }

  void _addInterfaces(IdlFile idlFile) {
    idlFile.interfaces.forEach((name, value) {
      assert(value.name == name);
      if (_interfaces.containsKey(value.name)) {
        throw Exception('Duplicate interface type: ${value.name}');
      }
      if (_enums.containsKey(value.name) ||
          _structs.containsKey(value.name) ||
          _strings.containsKey(value.name) ||
          _ints.containsKey(value.name) ||
          _floats.containsKey(value.name)) {
        throw Exception('Type already exists: ${value.name}');
      }

      _interfaces[value.name] = null;
    });
  }

  void _addStrings(IdlFile idlFile) {
    idlFile.strings.forEach((name, value) {
      assert(value.name == name);
      if (_strings.containsKey(value.name)) {
        throw Exception('Duplicate string type: ${value.name}');
      }
      if (_enums.containsKey(value.name) ||
          _structs.containsKey(value.name) ||
          _interfaces.containsKey(value.name) ||
          _ints.containsKey(value.name) ||
          _floats.containsKey(value.name)) {
        throw Exception('Type already exists: ${value.name}');
      }

      _strings[value.name] = null;
    });
  }

  void _addInts(IdlFile idlFile) {
    idlFile.ints.forEach((name, value) {
      assert(value.name == name);
      if (_ints.containsKey(value.name)) {
        throw Exception('Duplicate int type: ${value.name}');
      }
      if (_enums.containsKey(value.name) ||
          _structs.containsKey(value.name) ||
          _interfaces.containsKey(value.name) ||
          _strings.containsKey(value.name) ||
          _floats.containsKey(value.name)) {
        throw Exception('Type already exists: ${value.name}');
      }

      _ints[value.name] = null;
    });
  }

  void _addFloats(IdlFile idlFile) {
    idlFile.floats.forEach((name, value) {
      assert(value.name == name);
      if (_floats.containsKey(value.name)) {
        throw Exception('Duplicate float type: ${value.name}');
      }
      if (_enums.containsKey(value.name) ||
          _structs.containsKey(value.name) ||
          _interfaces.containsKey(value.name) ||
          _strings.containsKey(value.name) ||
          _ints.containsKey(value.name)) {
        throw Exception('Type already exists: ${value.name}');
      }

      _floats[value.name] = null;
    });
  }

  factory IdlObject.compile(IdlFile idlFile) {
    return IdlObject()
      .._name = idlFile.libraryName
      .._resolveImports(idlFile)
      .._addEnums(idlFile)
      .._addStructs(idlFile)
      .._addInterfaces(idlFile)
      .._addStrings(idlFile)
      .._addInts(idlFile)
      .._addFloats(idlFile)
      .._setEnums(idlFile)
      .._setStructs(idlFile)
      .._setInterfaces(idlFile)
      .._setStrings(idlFile)
      .._setInts(idlFile)
      .._setFloats(idlFile);
  }

  final _imports = <String, IdlObject>{};
  final _enums = <String, IdlEnum>{};
  final _structs = <String, IdlStruct>{};
  final _interfaces = <String, IdlInterface>{};
  final _strings = <String, IdlConst>{};
  final _ints = <String, IdlConst>{};
  final _floats = <String, IdlConst>{};

  bool isInterface(String value) => _interfaces.containsKey(value);
  bool isStruct(String value) => _structs.containsKey(value);
  bool isEnum(String value) => _enums.containsKey(value);
  bool isString(String value) => _strings.containsKey(value);
  bool isInt(String value) => _ints.containsKey(value);
  bool isFloat(String value) => _floats.containsKey(value);

  IdlObjectFieldsFactory _idlObjectFieldsFactory;
  IdlObjectFieldsFactory get idlObjectFieldsFactory =>
      _idlObjectFieldsFactory ?? DefaultIdlObjectFieldsFactory();

  String _name;

  String get name => _name;
  List<IdlObject> get imports => _imports.values.toList();
  List<IdlEnum> get enums => _enums.values.toList();
  List<IdlStruct> get structs => _structs.values.toList();
  List<IdlInterface> get interfaces => _interfaces.values.toList();
  List<IdlConst> get strings => _strings.values.toList();
  List<IdlConst> get ints => _ints.values.toList();
  List<IdlConst> get floats => _floats.values.toList();
}

abstract class IdlObjectFieldsFactory {
  AttributeFieldCompiler<IdlStructField, IdlFileStructObject>
      createStructFieldBuilder(IdlObject parent);
  AttributeFieldCompiler<IdlEnumField, IdlFileEnumObject>
      createEnumFieldBuilder(IdlObject parent);
  AttributeFieldCompiler<IdlInterfaceField, IdlFileInterfaceObject>
      createInterfaceFieldBuilder(IdlObject parent);
}

class DefaultIdlObjectFieldsFactory implements IdlObjectFieldsFactory {
  @override
  AttributeFieldCompiler<IdlStructField, IdlFileStructObject>
      createStructFieldBuilder(IdlObject parent) =>
          IdlStructFieldBuilder(parent);
  @override
  AttributeFieldCompiler<IdlEnumField, IdlFileEnumObject>
      createEnumFieldBuilder(IdlObject parent) => IdlEnumFieldBuilder();
  @override
  AttributeFieldCompiler<IdlInterfaceField, IdlFileInterfaceObject>
      createInterfaceFieldBuilder(IdlObject parent) =>
          IdlInterfaceFieldBuilder(parent);
}

class IdlInterfaceFieldBuilder
    implements
        AttributeFieldCompiler<IdlInterfaceField, IdlFileInterfaceObject> {
  @override
  IdlInterfaceField fromObjectField(IdlFileInterfaceObject fieldObject) {
    final idlAtt = <IdlInterfaceAttributes>{};

    fieldObject.attributes.attributes.forEach((atts) {
      if (atts.every((att) => att is IdlFileAttribute)) {
        atts.forEach((value) {
          var att = value as IdlFileAttribute;
          switch (att.value) {
            case 'get':
              idlAtt.add(IdlInterfaceAttributes.pGet);
              break;
            case 'set':
              idlAtt.add(IdlInterfaceAttributes.pSet);
              break;
            case 'stream':
              idlAtt.add(IdlInterfaceAttributes.mStream);
              break;
            case 'factory':
              idlAtt.add(IdlInterfaceAttributes.mFactory);
              break;
            case 'async':
              idlAtt.add(IdlInterfaceAttributes.mAsync);
              break;
            case 'sync':
              idlAtt.add(IdlInterfaceAttributes.mSync);
              break;
            default:
          }
        });

        if (idlAtt.contains(IdlInterfaceAttributes.mStream) ||
            idlAtt.contains(IdlInterfaceAttributes.mFactory) ||
            idlAtt.contains(IdlInterfaceAttributes.mSync) ||
            idlAtt.contains(IdlInterfaceAttributes.mAsync)) {
          if (idlAtt.length > 1) {
            throw Exception('Invalid attribute usage.');
          }
        } else if (idlAtt.difference({
          IdlInterfaceAttributes.pGet,
          IdlInterfaceAttributes.pSet
        }).isNotEmpty) {
          throw Exception('Invalid property attribute usage.');
        }
      }
    });

    final idlType = _IdlTypeAnalyzer.fromTypeObject(
      idlObject,
      fieldObject.type,
    );

    if (idlAtt.contains(IdlInterfaceAttributes.pSet) &&
        idlType.objectType == IdlTypes.nInterface) {
      throw Exception('Use of set property not supported for interfaces.');
    }

    return IdlInterfaceField(
      fieldName: fieldObject.name,
      idlType: idlType,
      interfaceAttributes: idlAtt.toList(growable: false),
    );
  }

  IdlInterfaceFieldBuilder(this.idlObject);
  final IdlObject idlObject;
}

class IdlStructFieldBuilder
    implements AttributeFieldCompiler<IdlStructField, IdlFileStructObject> {
  @override
  IdlStructField fromObjectField(IdlFileStructObject fieldObject) {
    return IdlStructField(
      fieldName: fieldObject.name,
      idlType: _IdlTypeAnalyzer.fromTypeObject(
        idlObject,
        fieldObject.type,
        supportsTuple: false,
        supportsInterfaceType: false,
        supportsMethod: false,
      ),
    );
  }

  IdlStructFieldBuilder(this.idlObject);
  final IdlObject idlObject;
}

class IdlEnumFieldBuilder
    implements AttributeFieldCompiler<IdlEnumField, IdlFileEnumObject> {
  @override
  IdlEnumField fromObjectField(IdlFileEnumObject fieldObject) {
    return IdlEnumField(fieldName: fieldObject.name);
  }
}

class _IdlTypeAnalyzer {
  static IdlType fromTypeObject(
    IdlObject object,
    dynamic type, {
    bool supportsStructType = true,
    bool supportsInterfaceType = true,
    bool supportsEnumType = true,
    bool supportsTypeList = true,
    bool supportsTuple = true,
    bool supportsArray = true,
    bool supportsMethod = true,
  }) {
    if (type is IdlFileTypeName) {
      if (object.isInterface(type.value)) {
        if (supportsInterfaceType) {
          return IdlType(objectType: IdlTypes.nInterface, object: type.value);
        } else {
          throw Exception('Invalid use of interface type: ${type.value}.');
        }
      } else if (object.isStruct(type.value)) {
        if (supportsStructType) {
          return IdlType(objectType: IdlTypes.nStruct, object: type.value);
        } else {
          throw Exception('Invalid use of struct type: ${type.value}.');
        }
      } else if (object.isEnum(type.value)) {
        if (supportsEnumType) {
          return IdlType(objectType: IdlTypes.nEnum, object: type.value);
        } else {
          throw Exception('Invalid use of enum type: ${type.value}.');
        }
      } else if (object.isString(type.value)) {
        return IdlType(objectType: IdlTypes.nString);
      } else if (object.isInt(type.value)) {
        return IdlType(objectType: IdlTypes.nInt);
      } else if (object.isFloat(type.value)) {
        return IdlType(objectType: IdlTypes.nFloat);
      } else {
        throw Exception('Invalid typename in idlfile: ${type.value}');
      }
    } else if (type is IdlFileNativeTypeName) {
      switch (type.value) {
        case 'int':
          return IdlType(objectType: IdlTypes.nInt);
        case 'bool':
          return IdlType(objectType: IdlTypes.nBool);
        case 'float':
          return IdlType(objectType: IdlTypes.nFloat);
        case 'string':
          return IdlType(objectType: IdlTypes.nString);
        case 'bytes':
          return IdlType(objectType: IdlTypes.nBytes);
        case 'none':
          return IdlType(objectType: IdlTypes.nNone);
        default:
          throw Exception('Built-in type not supported: ${type.value}.');
      }
    } else if (type is IdlFileArrayType) {
      if (!supportsArray) {
        throw Exception('Invalid use of array: ${type.value}');
      }

      return IdlType(
          objectType: IdlTypes.nArray,
          object: _IdlTypeAnalyzer.fromTypeObject(
            object,
            type.value,
            supportsEnumType: supportsEnumType,
            supportsStructType: supportsStructType,
            supportsInterfaceType: false,
            supportsTuple: false,
            supportsTypeList: false, // TODO
          ));
    } else if (type is IdlFileTypeMethod) {
      if (!supportsMethod) {
        throw Exception('Invalid use of method: ${type.toString()}');
      }

      return IdlType(
          objectType: IdlTypes.nMethod,
          object: IdlMethod(
              args: _IdlTypeAnalyzer.fromTypeObject(
                object,
                type.argTypes,
                supportsTuple: true,
                supportsEnumType: supportsEnumType,
                supportsStructType: supportsStructType,
                supportsTypeList: supportsTypeList,
                supportsArray: supportsArray,
                supportsInterfaceType: false,
                supportsMethod: false,
              ),
              returnType: _IdlTypeAnalyzer.fromTypeObject(
                object,
                type.returnType,
                supportsArray: supportsArray,
                supportsEnumType: supportsEnumType,
                supportsStructType: supportsStructType,
                supportsTypeList: supportsTypeList,
                supportsTuple: false,
                supportsInterfaceType: false,
                supportsMethod: false,
              )));
    } else if (type is IdlFileTypeList) {
      if (!supportsTypeList) {
        throw Exception('Invalid use of type list: ${type.value}');
      }

      final typeList = type.value
          .map((value) => _IdlTypeAnalyzer.fromTypeObject(
                object,
                value,
                supportsEnumType: supportsEnumType,
                supportsInterfaceType: supportsInterfaceType,
                supportsStructType: supportsStructType,
                supportsTuple: supportsTuple,
                supportsArray: false,
                supportsTypeList: false,
              ))
          .toList(growable: false);
      return IdlType(
          objectType: IdlTypes.nTypeList, object: IdlTypeList(typeList: typeList));
    } else if (type is IdlFileTypeTuple) {
      if (!supportsTuple) {
        throw Exception('Invalid use of tuple: ${type.value}');
      }
      final tupleList = type.value.entries
          .map((entry) => IdlTupleEntry(
              name: entry.key,
              idlType: _IdlTypeAnalyzer.fromTypeObject(
                object,
                entry.value,
                supportsEnumType: supportsEnumType,
                supportsStructType: supportsStructType,
                supportsInterfaceType: false, // TODO
                supportsTypeList: false,
                supportsArray: supportsArray,
              )))
          .toList();

      return IdlType(objectType: IdlTypes.nTuple, object: IdlTypeTuple(typeList: tupleList));
    } else if (type is IdlFileTypeEmpty) {
      return IdlType(objectType: IdlTypes.nVoid, object: null);
    }

    throw Exception('Could not create type: { ${type.toString()} }');
  }
}

extension HisType on IdlTypes {
  // If it isn't an allocated value.
  bool get isPrimitive =>
      this == IdlTypes.nBool ||
      this == IdlTypes.nInt ||
      this == IdlTypes.nFloat ||
      this == IdlTypes.nVoid ||
      this == IdlTypes.nNone ||
      this == IdlTypes.nEnum;
}
