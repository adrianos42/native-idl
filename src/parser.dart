import 'dart:collection';

import 'dart:io';

const kKeywords = [
  'enum',
  'struct',
  'interface',
  'import',
  'library',
];

const kReserved = [
  'abstract',
  'as',
  'assert',
  'attribute',
  'await',
  'break',
  'case',
  'catch',
  'class',
  'const',
  'continue',
  'covariant',
  'default',
  'deferred',
  'do',
  'dynamic',
  'else',
  'export',
  'extends',
  'extension',
  'external',
  'factory',
  'false',
  'finally',
  'for',
  'get',
  'hide',
  'if',
  'implements',
  'in',
  'is',
  'mixin',
  'new',
  'null',
  'on',
  'operator',
  'part',
  'structure',
  'private',
  'public',
  'rethrow',
  'return',
  'service',
  'set',
  'show',
  'static',
  'super',
  'switch',
  'this',
  'throw',
  'time',
  'true',
  'try',
  'typedef',
  'value',
  'var',
  'version',
  'void',
  'while',
  'with',
  'yield',
];

const kNativeTypes = [
  'int',
  'float',
  'bool',
  'string',
  'bytes',
];

const kReservedTypes = [
  'uint',
  'i8',
  'i16',
  'i32',
  'i64',
  'i128',
  'u8',
  'u16',
  'u32',
  'u64',
  'u128',
  'f32',
  'f64',
  'double',
  'void',
  'date',
  'time',
  'list',
  'char',
  'decimal',
  'bigint',
];

const kAttributesNames = [
  'get',
  'set',
  'stream',
  'factory',
  'sync',
  'async',
];

const kReservedAttributesNames = [
  'abstract',
  'assert',
  'attribute',
  'const',
  'default',
  'deferred',
  'dynamic',
  'export',
  'extends',
  'extension',
  'external',
  'hide',
  'implements',
  'in',
  'is',
  'new',
  'null',
  'on',
  'operator',
  'private',
  'public',
  'return',
  'service',
  'static',
  'time',
  'value',
  'version',
  'void',
];

void verifyAttributeNameAllowed(String value) {
  final hvalue = value.toLowerCase();
  if (!kAttributesNames.contains(hvalue) &&
      (kKeywords.contains(hvalue) ||
          kReservedAttributesNames.contains(hvalue) ||
          kNativeTypes.contains(hvalue) ||
          kReservedTypes.contains(hvalue))) {
    throw Exception('Attribute cannot be a reserved name: $value.');
  }
}

void verifyNameAllowed(String value) {
  final hvalue = value.toLowerCase();
  if (kKeywords.contains(hvalue) ||
      kReserved.contains(hvalue) ||
      kNativeTypes.contains(hvalue) ||
      kReservedTypes.contains(hvalue)) {
    throw Exception('Name cannot be a keyword: $value.');
  }
}

void verifyIsNativeType(String value) {
  if (!kNativeTypes.contains(value)) {
    throw Exception('Not a native type: $value.');
  }
}

void verifyTypeNameAllowed(String value) {
  final hvalue = value.toLowerCase();
  if (kNativeTypes.contains(hvalue) ||
      kReservedTypes.contains(hvalue) ||
      kKeywords.contains(hvalue)) {
    throw Exception('Cannot be a built-in type: $value.');
  }
}

class IdlFileAttribute {
  IdlFileAttribute(this.value) {
    verifyAttributeNameAllowed(value);
  }
  final String value;

  @override
  bool operator ==(dynamic other) =>
      other is IdlFileAttribute && value == other.value;

  @override
  String toString() => value.toString();
}

class IdlFileAttributes {
  factory IdlFileAttributes.createAttribute(String attText) {
    final attributes = <List<dynamic>>[];

    final attReg = RegExp(r'\[(.*?)\]');

    for (var attMatch in attReg.allMatches(attText)) {
      final attList = attMatch.group(1);
      assert(attList != null);

      // Matches each element in list.
      final attListReg = RegExp(r'(\b[a-z]+)|(?:"(.*?)")');
      final att = <dynamic>[];
      for (var attListMatch in attListReg.allMatches(attList)) {
        final attribute = attListMatch.group(1);
        if (attribute != null) {
          att.add(IdlFileAttribute(attribute)); // Regular attribute.
        } else {
          final attributeString = attListMatch.group(2); // Attribute string.
          assert(attributeString != null);
          att.add(attributeString);
        }
      }
      attributes.add(att);
    }

    return IdlFileAttributes(attributes);
  }

  const IdlFileAttributes(this.attributes);
  final List<List<dynamic>> attributes;

  @override
  String toString() {
    var value = '';

    attributes.forEach((atts) {
      value += '[';

      for (var i = 0; i < atts.length; i++) {
        if (i > 0 && i < atts.length) {
          value += ', ';
        }

        if (atts[i] is String) {
          final att = atts[i] as String;
          value += '"$att"';
        } else {
          final att = atts[i] as IdlFileAttribute;
          value += att.value;
        }
      }

      value += ']';
    });

    return value;
  }

  @override
  bool operator ==(dynamic other) {
    if (other is! IdlFileAttributes) return false;

    // TODO Find a way to compare lists without doing this.
    if (attributes.length != other.attributes.length) return false;
    for (var i = 0; i < attributes.length; i++) {
      if (attributes[i].length != other.attributes[i].length) return false;
      for (var j = 0; j < attributes[i].length; j++) {
        if (!attributes[i].contains(other.attributes[i][j])) return false;
      }
    }

    return true;
  }
}

// interface
class IdlFileInterface {
  IdlFileInterface(this.name, this.fields) {
    verifyTypeNameAllowed(name);
  }

  factory IdlFileInterface.createFields(String name, String fieldsText) {
    assert(fieldsText != null, 'Fields in interface cannot be null');

    // TODO Set to recognize any string.
    final fieldReg =
        RegExp(r'\s*(\[(?:.*?)\])\s*\b([a-z]+(?:[A-Z][a-z]+)*)\s*:\s*(.*),\s*');

    final fields = <IdlFileInterfaceObject>[];

    for (var match in fieldReg.allMatches(fieldsText)) {
      final attributesGroup = match.group(1);
      assert(attributesGroup != null);

      final attributes = IdlFileAttributes.createAttribute(attributesGroup);

      final fieldName = match.group(2);
      assert(fieldName != null);

      if (fields.any((value) => value.name == fieldName)) {
        throw Exception('Interface cannot have duplicate fields.');
      }

      final type = IdlFileType.createType(match.group(3));

      fields.add(IdlFileInterfaceObject(fieldName, attributes, type));
    }

    if (fieldsText.replaceAll(fieldReg, '').isNotEmpty) {
      throw Exception('Invalid interface field declaration.');
    }

    return IdlFileInterface(name, fields);
  }

  final List<IdlFileInterfaceObject> fields;
  final String name;

  @override
  bool operator ==(dynamic other) {
    if (other is! IdlFileInterface ||
        fields.length != other.fields.length ||
        name != other.name) {
      return false;
    }

    // TODO List compare.
    for (var i = 0; i < fields.length; i++) {
      if (fields[i] != other.fields[i]) return false;
    }

    return true;
  }
}

class IdlFileInterfaceObject {
  IdlFileInterfaceObject(this.name, this.attributes, this.type) {
    verifyNameAllowed(name);
  }

  final IdlFileAttributes attributes;
  final IdlFileType type;
  final String name;

  @override
  bool operator ==(dynamic other) =>
      other is IdlFileInterfaceObject &&
      name == other.name &&
      attributes == other.attributes &&
      type == other.type;
}

abstract class IdlFileType {
  factory IdlFileType.createType(String typeText) {
    assert(typeText != null, 'Type field cannot be null');

    final typesReg = RegExp(
        r'(^\s*\(\b(.*?)\)\s*->\s*\b(.*?)\s*$)|(^\s*(?:(\b[a-z]+)|(\b[A-Z][a-z]+(?:[A-Z][a-z]+)*)|(?:\[(.*?)\])|(?:\((.*?)\)))((?:\[\])+)?\s*$)'); //  TODO ? accept array inside typelist but not tuple.

    if (typeText.replaceAll(typesReg, '').isNotEmpty) {
      throw Exception('Invalid interface type declaration: $typeText');
    }

    final typesMatches = typesReg.allMatches(typeText);
    final typesMatch = typesMatches.elementAt(0);

    if (typesMatch.group(1) != null) {
      return IdlFileTypeMethod(
          IdlFileTypeTuple.createTypeTuple(typesMatch.group(2)),
          IdlFileType.createType(typesMatch.group(3)));
    } else if (typesMatch.group(4) != null) {
      final arrayList = typesMatch.group(9);
      if (arrayList != null) {
        if (typesMatch.group(8) != null) {
          throw Exception('Tuple cannot be an array.');
        }

        final type = typesMatch.group(5) ??
            typesMatch.group(6) ??
            typesMatch.group(7); // TODO group 7, which is for type list.

        if (type == null) {
          throw Exception('Invalid type in array.');
        }

        return IdlFileArrayType.createArrayType(type + arrayList);
      }

      final nativeTypeName = typesMatch.group(5);
      if (nativeTypeName != null) {
        return IdlFileNativeTypeName(nativeTypeName);
      }

      final typeName = typesMatch.group(6);
      if (typeName != null) {
        return IdlFileTypeName(typeName);
      }

      final typeListText = typesMatch.group(7);
      if (typeListText != null) {
        return IdlFileTypeList.createTypeList(typeListText);
      }

      final tupleText = typesMatch.group(8);
      if (tupleText == null) {
        /// In this case, this shouldn't be null at all.
        throw Exception('Invalid data type.');
      }

      return IdlFileTypeMethod(IdlFileTypeTuple.createTypeTuple(tupleText), IdlFileTypeEmpty());
    } else {
      throw Exception('Invalid data type.');
    }
  }
}

class IdlFileTypeTuple implements IdlFileType {
  factory IdlFileTypeTuple.createTypeTuple(String typeTupleText) {
    assert(typeTupleText != null, 'Type tuple text cannot be null.');

    // Matches either a native type or custom type.  // TODO
    final typeTupleReg = RegExp(
        r'(?:(\b[a-z]+(?:[A-Z][a-z]+)*))\s*:\s*(?:(\b[a-z]+)|(\b[A-Z][a-z]+(?:[A-Z][a-z]+)*))');

    final typeTupleList = <String, IdlFileType>{};

    for (var typeTupleMatch in typeTupleReg.allMatches(typeTupleText)) {
      final name = typeTupleMatch.group(1);

      if (name.isEmpty) {
        throw Exception('Argument name cannont be empty.');
      }

      final typeNativeName = typeTupleMatch.group(2);
      if (typeNativeName != null) {
        typeTupleList[name] = IdlFileNativeTypeName(typeNativeName);
        continue;
      }
      final typeName = typeTupleMatch.group(3);
      if (typeName == null) throw Exception('Should have at least a type name');
      typeTupleList[name] = IdlFileTypeName(typeName);
    }

    return IdlFileTypeTuple(typeTupleList);
  }

  const IdlFileTypeTuple(this.value);
  final Map<String, IdlFileType> value;

  @override
  bool operator ==(dynamic other) =>
      other is IdlFileTypeTuple &&
      value.length == other.value.length &&
      value.keys.every((key) => value[key] == other.value[key]);

  @override
  String toString() {
    var result = '(';

    result += value.keys.fold('', (p, key) {
      if (p.isNotEmpty) p += ', ';
      return p + '$key: ${value.toString()}';
    });

    return result + ')';
  }
}

class IdlFileTypeMethod implements IdlFileType {
  const IdlFileTypeMethod(this.argTypes, this.returnType);
  final IdlFileTypeTuple argTypes;
  final IdlFileType returnType;

  @override
  bool operator ==(dynamic other) =>
      other is IdlFileTypeMethod &&
      argTypes == other.argTypes &&
      returnType == other.returnType;

  @override
  String toString() => argTypes.toString() + ' -> ' + returnType.toString();
}

class IdlFileTypeList implements IdlFileType {
  factory IdlFileTypeList.createTypeList(String typeListText) {
    assert(typeListText != null, 'Type list text cannot be null.');

    // Matches a tuple, native type, or any other type.
    final typeListReg =
        RegExp(r'((\b[a-z]+)|(\b[A-Z][a-z]+(?:[A-Z][a-z]+)*)|(?:\((.*?)\)))');
    final typeListMatches = typeListReg.allMatches(typeListText);

    final typeList = <dynamic>[];

    for (var typeListMatch in typeListMatches) {
      final nativeTypeName = typeListMatch.group(2);
      if (nativeTypeName != null) {
        if (typeList.contains(IdlFileNativeTypeName(nativeTypeName))) {
          throw Exception(
              'Idl file type list cannot duplicate types: $nativeTypeName.');
        }
        typeList.add(IdlFileNativeTypeName(nativeTypeName));
        continue;
      }
      final typeName = typeListMatch.group(3);
      if (typeName != null) {
        if (typeList.contains(IdlFileTypeName(typeName))) {
          throw Exception(
              'Idl file type list cannot duplicate types: $typeName.');
        }
        typeList.add(IdlFileTypeName(typeName));
        continue;
      }
      //final tupleTypeText = typeListMatch.group(4);
      throw Exception('Tuple in list is not supported.');
      //typeList.add(IdlFileTypeTuple.createTypeTuple(tupleTypeText));
    }

    if (typeList == null) throw Exception('Must have at least one type.');

    return IdlFileTypeList(typeList);
  }

  const IdlFileTypeList(this.value);
  final List<dynamic> value;

  @override
  bool operator ==(dynamic other) {
    if (other is! IdlFileTypeList || value.length != other.value.length) {
      return false;
    }

    // TODO List compare.
    for (var i = 0; i < value.length; i++) {
      if (value[i] != other.value[i]) return false;
    }

    return true;
  }

  @override
  String toString() {
    var result = '[';

    for (var i = 0; i < value.length; i++) {
      if (i > 0 && i < value.length) result += ', ';
      result += value[i].toString();
    }

    return result + ']';
  }
}

class IdlFileArrayType implements IdlFileType {
  factory IdlFileArrayType.createArrayType(String arrayText) {
    assert(arrayText != null, 'Type list text cannot be null.');

    dynamic type;

    final arrayReg = RegExp(
        r'(?:(?:(\b[a-z]+)|(\b[A-Z][a-z]+(?:[A-Z][a-z]+)*))((?:\[\])*))\[\]');
    final arrayMatches = arrayReg.allMatches(arrayText);

    if (arrayMatches.length != 1) {
      throw Exception('Invalid type in array.');
    }

    final arrayMatch = arrayMatches.elementAt(0);

    final arrayList = arrayMatch.group(3);

    if (arrayList.isNotEmpty) {
      type = IdlFileArrayType.createArrayType(
          (arrayMatch.group(2) ?? arrayMatch.group(1)) + arrayList);
    } else {
      final nativeTypeName = arrayMatch.group(1);
      if (nativeTypeName != null) {
        type = IdlFileNativeTypeName(nativeTypeName);
      } else {
        final typeName = arrayMatch.group(2);
        if (typeName != null) {
          type = IdlFileTypeName(typeName);
        }
      }
    }

    return IdlFileArrayType(type);
  }

  const IdlFileArrayType(this.value);

  final dynamic value;

  @override
  bool operator ==(dynamic other) =>
      other is IdlFileArrayType && value == other.value;

  @override
  String toString() => value.toString() + '[]';
}

class IdlFileTypeEmpty implements IdlFileType {
  @override
  String toString() => '';
}

class IdlFileNativeTypeName implements IdlFileType {
  IdlFileNativeTypeName(this.value) {
    verifyIsNativeType(value);
  }

  final String value;

  @override
  bool operator ==(dynamic other) =>
      other is IdlFileNativeTypeName && value == other.value;

  @override
  String toString() => value;
}

class IdlFileTypeName implements IdlFileType {
  IdlFileTypeName(this.value) {
    verifyNameAllowed(value);
  }

  final String value;

  @override
  bool operator ==(dynamic other) =>
      other is IdlFileTypeName && value == other.value;

  @override
  String toString() => value.toString();
}

// enum
class IdlFileEnum implements IdlFileType {
  IdlFileEnum(this.name, this.fields) {
    verifyTypeNameAllowed(name);
  }

  factory IdlFileEnum.createFields(String name, String enumText) {
    assert(enumText != null, 'Enum text cannot be null.');

    final enumReg =
        RegExp(r'\s*(\[(?:.*?)\])*\s*\b([a-z]+(?:[A-Z][a-z]+)*)\s*,\s*');
    final fields = <IdlFileEnumObject>[];

    for (var match in enumReg.allMatches(enumText)) {
      IdlFileAttributes attributes;
      final attributesGroup = match.group(1);
      if (attributesGroup != null) {
        attributes = IdlFileAttributes.createAttribute(attributesGroup);
      }

      final field = match.group(2);
      if (field == null || field.isEmpty) {
        throw Exception('Invalid field in enum.');
      }

      if (fields.any((value) => value.name == field)) {
        throw Exception('Enum cannot have duplicate fields.');
      }

      fields.add(IdlFileEnumObject(field, attributes));
    }

    if (enumText.replaceAll(enumReg, '').isNotEmpty) {
      throw Exception('Invalid enum field.');
    }

    return IdlFileEnum(name, fields);
  }

  final List<IdlFileEnumObject> fields;
  final String name;

  @override
  bool operator ==(dynamic other) {
    if (other is! IdlFileEnum ||
        fields.length != other.fields.length ||
        name != other.name) {
      return false;
    }

    // TODO List compare.
    for (var i = 0; i < fields.length; i++) {
      if (fields[i] != other.fields[i]) return false;
    }

    return true;
  }

  @override
  String toString() => fields.toString();
}

class IdlFileEnumObject {
  IdlFileEnumObject(this.name, this.attributes) {
    verifyNameAllowed(name);
  }

  final IdlFileAttributes attributes;
  final String name;

  @override
  bool operator ==(dynamic other) =>
      other is IdlFileEnumObject &&
      attributes == other.attributes &&
      name == other.name;

  @override
  String toString() {
    var result = 'Field name: \'$name\'; ';
    result += 'Attributes: ';
    result += attributes?.toString() ?? 'none';
    return result;
  }
}

// struct
class IdlFileStruct implements IdlFileType {
  IdlFileStruct(this.name, this.fields) {
    verifyTypeNameAllowed(name);
  }

  factory IdlFileStruct.createFields(String name, String fieldsText) {
    assert(fieldsText != null, 'Fields in struct cannot be null');

    final fieldReg = RegExp(
        r'\s*(\[(?:.*?)\])*\s*\b([a-z]+(?:[A-Z][a-z]+)*)\s*:\s*(.*),\s*');

    final fields = <IdlFileStructObject>[];

    for (var match in fieldReg.allMatches(fieldsText)) {
      IdlFileAttributes attributes;
      final attributesGroup = match.group(1);
      if (attributesGroup != null) {
        attributes = IdlFileAttributes.createAttribute(attributesGroup);
      }

      final fieldName = match.group(2);
      if (fieldName == null || fieldName.isEmpty) {
        throw Exception('Invalid field in struct.');
      }

      if (fields.any((value) => value.name == fieldName)) {
        throw Exception('Struct cannot have duplicate fields.');
      }

      final typeName = match.group(3);
      if (typeName == null) {
        throw Exception('Struct must have type name.');
      }

      // Matches a normal type or a tuple.
      final type = IdlFileType.createType(typeName);

      fields.add(IdlFileStructObject(fieldName, type, attributes));
    }

    if (fieldsText.replaceAll(fieldReg, '').isNotEmpty) {
      throw Exception('Invalid struct field.');
    }

    return IdlFileStruct(name, fields);
  }

  final List<IdlFileStructObject> fields;
  final String name;

  @override
  bool operator ==(dynamic other) {
    if (other is! IdlFileStruct ||
        fields.length != other.fields.length ||
        name != other.name) {
      return false;
    }

    // TODO List compare.
    for (var i = 0; i < fields.length; i++) {
      if (fields[i] != other.fields[i]) return false;
    }

    return true;
  }
}

class IdlFileStructObject {
  IdlFileStructObject(this.name, this.type, this.attributes) {
    verifyNameAllowed(name);
  }

  final IdlFileType type;
  final IdlFileAttributes attributes;
  final String name;

  @override
  bool operator ==(dynamic other) =>
      other is IdlFileStructObject &&
      type == other.type &&
      attributes == other.attributes &&
      name == other.name;

  @override
  String toString() {
    var result = 'Field name: \'$name\'; ';
    result += 'Type: \'${type.toString()}\' ';
    result += 'Attributes: ';
    result += attributes?.toString() ?? 'none';
    return result;
  }
}

class IdlFileString {
  IdlFileString(this.name, this.fields) {
    verifyTypeNameAllowed(name);
  }

  factory IdlFileString.createFields(String name, String stringText) {
    assert(stringText != null, 'String text cannot be null.');

    final stringReg =
        RegExp(r'\s*\b([a-z]+(?:[A-Z][a-z]+)*)\s*=\s*"(.*?)"\s*,\s*');
    final fields = <String, String>{};

    for (var match in stringReg.allMatches(stringText)) {
      final field = match.group(1);
      if (field == null || field.isEmpty) {
        throw Exception('Invalid field in string.');
      }

      if (fields.containsKey(field)) {
        throw Exception('Strings cannot have duplicate fields.');
      }

      final stringValue = match.group(2);
      if (stringValue == null || stringValue.isEmpty) {
        throw Exception('String value is invalid.');
      }

      fields[field] = stringValue;
    }

    if (stringText.replaceAll(stringReg, '').isNotEmpty) {
      throw Exception('Invalid string field.');
    }

    return IdlFileString(name, fields);
  }

  final Map<String, String> fields;
  final String name;

  @override
  bool operator ==(dynamic other) {
    if (other is! IdlFileString ||
        fields.length != other.fields.length ||
        name != other.name) {
      return false;
    }

    // TODO List compare.
    for (var i = 0; i < fields.length; i++) {
      if (fields[i] != other.fields[i]) return false;
    }

    return true;
  }

  @override
  String toString() => fields.toString();
}

class IdlFileInt {
  IdlFileInt(this.name, this.fields) {
    verifyTypeNameAllowed(name);
  }

  factory IdlFileInt.createFields(String name, String intText) {
    assert(intText != null, 'Int text cannot be null.');

    final intReg = RegExp(
        r'\s*\b([a-z]+(?:[A-Z][a-z]+)*)\s*=\s*(?:(-?0x[0-9A-F]+)|(-?[0-9]+))\s*,\s*');
    final fields = <String, String>{};

    for (var match in intReg.allMatches(intText)) {
      final field = match.group(1);
      if (field == null || field.isEmpty) {
        throw Exception('Invalid field in int.');
      }

      if (fields.containsKey(field)) {
        throw Exception('Ints cannot have duplicate fields.');
      }

      final intValue = match.group(2) ?? match.group(3);
      if (intValue == null || intValue.isEmpty) {
        throw Exception('Int value is invalid.');
      }

      fields[field] = intValue;
    }

    if (intText.replaceAll(intReg, '').isNotEmpty) {
      throw Exception('Invalid int field.');
    }

    return IdlFileInt(name, fields);
  }

  final Map<String, String> fields;
  final String name;

  @override
  bool operator ==(dynamic other) {
    if (other is! IdlFileInt ||
        fields.length != other.fields.length ||
        name != other.name) {
      return false;
    }

    // TODO List compare.
    for (var i = 0; i < fields.length; i++) {
      if (fields[i] != other.fields[i]) return false;
    }

    return true;
  }

  @override
  String toString() => fields.toString();
}

class IdlFileFloat {
  IdlFileFloat(this.name, this.fields) {
    verifyTypeNameAllowed(name);
  }

  factory IdlFileFloat.createFields(String name, String floatText) {
    assert(floatText != null, 'Float text cannot be null.');

    final floatReg = RegExp(
        r'\s*\b([a-z]+(?:[A-Z][a-z]+)*)\s*=\s*(-?[0-9]+\.?[0-9]*)\s*,\s*');
    final fields = <String, String>{};

    for (var match in floatReg.allMatches(floatText)) {
      final field = match.group(1);
      if (field == null || field.isEmpty) {
        throw Exception('Invalid field in float.');
      }

      if (fields.containsKey(field)) {
        throw Exception('Floats cannot have duplicate fields.');
      }

      final floatValue = match.group(2);
      if (floatValue == null || floatValue.isEmpty) {
        throw Exception('Float value is invalid.');
      }

      fields[field] = floatValue;
    }

    if (floatText.replaceAll(floatReg, '').isNotEmpty) {
      throw Exception('Invalid float field.');
    }

    return IdlFileFloat(name, fields);
  }

  final Map<String, String> fields;
  final String name;

  @override
  bool operator ==(dynamic other) {
    if (other is! IdlFileFloat ||
        fields.length != other.fields.length ||
        name != other.name) {
      return false;
    }

    // TODO List compare.
    for (var i = 0; i < fields.length; i++) {
      if (fields[i] != other.fields[i]) return false;
    }

    return true;
  }

  @override
  String toString() => fields.toString();
}

// File
class IdlFile {
  const IdlFile({
    this.libraryName,
    this.imports,
    this.interfaces,
    this.enums,
    this.structs,
    this.strings,
    this.ints,
    this.floats,
  });

  final String libraryName;
  final HashSet<String> imports;
  final Map<String, IdlFileInterface> interfaces;
  final Map<String, IdlFileEnum> enums;
  final Map<String, IdlFileStruct> structs;
  final Map<String, IdlFileString> strings;
  final Map<String, IdlFileInt> ints;
  final Map<String, IdlFileFloat> floats;

  static Future<IdlFile> parseFile(File file) async {
    var text = await file.readAsString();
    text = text.trim();

    // TODO Support for comments.
    text = text.replaceAll(RegExp(r'--.*$\n?', multiLine: true), '');
    // Library
    final libraryReg =
        RegExp(r'^(library)\s*\b([a-z_]+)\s*;', multiLine: false);

    final libraryMatches = libraryReg.allMatches(text);
    if (libraryMatches.length != 1) {
      throw Exception('Invalid library name declaration.');
    }

    final libraryMatch = libraryMatches.elementAt(0);
    final libraryName = libraryMatch.group(2);

    if (libraryMatch.group(1) != 'library' || libraryName == null) {
      return null;
    }

    text = text.replaceAll(libraryReg, '').trim();

    final imports = HashSet<String>();

    // Imports
    while (RegExp('import').allMatches(text).isNotEmpty) {
      final importReg =
          RegExp(r'^(import)\s*(\b[a-z_]+)\s*;', multiLine: false);
      var importMatches = importReg.allMatches(text);
      if (importMatches.length == 1) {
        final importName = importMatches.elementAt(0).group(2);

        if (importMatches.elementAt(0).group(1) != 'import' ||
            importName == null) {
          throw Exception('Invalid import delaration.');
        }

        imports.add(importName);

        text = text.replaceAll(importReg, '');
        text = text.trim();
      } else {
        throw Exception('Invalid import declaration.');
      }
    }

    final structs = <String, IdlFileStruct>{};
    final interfaces = <String, IdlFileInterface>{};
    final enums = <String, IdlFileEnum>{};
    final strings = <String, IdlFileString>{};
    final ints = <String, IdlFileInt>{};
    final floats = <String, IdlFileFloat>{};

    while (true) {
      final decReg = RegExp(
          r'(^interface\b)|(^enum\b)|(^struct\b)|(^string\b)|(^int\b)|(^float\b)',
          multiLine: false);

      final decMatch = decReg.firstMatch(text);
      if (decMatch == null) {
        // No match, but there's something else out there.
        if (text.isNotEmpty) throw Exception('Invalid idl file.');
        break;
      }

      if (decMatch.group(1) == 'interface') {
        final interfaceReg = RegExp(
            r'^interface\s+\b([A-Z][a-z]+(?:[A-Z][a-z]+)*)\b\s+{([^{]*?)}',
            multiLine: false);

        final match = interfaceReg.firstMatch(text);
        if (match == null) {
          throw Exception('Invalid interface declaration');
        }

        final interfaceName = match.group(1);
        final fieldsText = match.group(2);

        if (interfaceName == null || interfaceName.isEmpty) {
          throw Exception('Must have interface name.');
        }

        if (fieldsText == null || fieldsText.isEmpty) {
          throw Exception('Field cannot be empty.');
        }

        if (interfaces.containsKey(interfaceName)) {
          throw Exception('Only one interface definition allowed.');
        }

        interfaces[interfaceName] =
            IdlFileInterface.createFields(interfaceName, fieldsText);
        text = text.replaceAll(interfaceReg, '').trim();
      } else if (decMatch.group(2) == 'enum') {
        final enumReg = RegExp(
            r'^enum\s+\b([A-Z][a-z]+(?:[A-Z][a-z]+)*)\b\s+{([^{]*?)}',
            multiLine: false);

        final match = enumReg.firstMatch(text);
        if (match == null) {
          throw Exception('Invalid enum declaration');
        }

        final enumName = match.group(1);
        final fieldsText = match.group(2);

        if (enumName == null || enumName.isEmpty) {
          throw Exception('Must have struct name.');
        }

        if (fieldsText == null || fieldsText.isEmpty) {
          throw Exception('Field cannot be empty.');
        }

        if (enums.containsKey(enumName)) {
          throw Exception('Only one enum definition allowed.');
        }

        enums[enumName] = IdlFileEnum.createFields(enumName, fieldsText);
        text = text.replaceAll(enumReg, '').trim();
      } else if (decMatch.group(3) == 'struct') {
        // Struct
        final structReg = RegExp(
            r'^struct\s+\b([A-Z][a-z]+(?:[A-Z][a-z]+)*)\b\s+{([^{]*?)}',
            multiLine: false);

        final match = structReg.firstMatch(text);
        if (match == null) {
          throw Exception('Invalid struct declaration');
        }

        final structName = match.group(1);
        final fieldsText = match.group(2);

        if (structName == null || structName.isEmpty) {
          throw Exception('Must have struct name.');
        }

        if (fieldsText == null || fieldsText.isEmpty) {
          throw Exception('Field cannot be empty.');
        }

        if (structs.containsKey(structName)) {
          throw Exception('Only one struct definition allowed.');
        }

        structs[structName] =
            IdlFileStruct.createFields(structName, fieldsText);
        text = text.replaceAll(structReg, '').trim();
      } else if (decMatch.group(4) == 'string') {
        final stringReg = RegExp(
            r'^string\s+\b([A-Z][a-z]+(?:[A-Z][a-z]+)*)\b\s+{([^{]*?)}',
            multiLine: false);

        final match = stringReg.firstMatch(text);
        if (match == null) {
          throw Exception('Invalid string declaration');
        }

        final stringName = match.group(1);
        final fieldsText = match.group(2);

        if (stringName == null || stringName.isEmpty) {
          throw Exception('Must have struct name.');
        }

        if (fieldsText == null || fieldsText.isEmpty) {
          throw Exception('Field cannot be empty.');
        }

        if (strings.containsKey(stringName)) {
          throw Exception('Only one string definition allowed.');
        }

        strings[stringName] =
            IdlFileString.createFields(stringName, fieldsText);
        text = text.replaceAll(stringReg, '').trim();
      } else if (decMatch.group(5) == 'int') {
        final intReg = RegExp(
            r'^int\s+\b([A-Z][a-z]+(?:[A-Z][a-z]+)*)\b\s+{([^{]*?)}',
            multiLine: false);

        final match = intReg.firstMatch(text);
        if (match == null) {
          throw Exception('Invalid int declaration');
        }

        final intName = match.group(1);
        final fieldsText = match.group(2);

        if (intName == null || intName.isEmpty) {
          throw Exception('Must have struct name.');
        }

        if (fieldsText == null || fieldsText.isEmpty) {
          throw Exception('Field cannot be empty.');
        }

        if (ints.containsKey(intName)) {
          throw Exception('Only one int definition allowed.');
        }

        ints[intName] = IdlFileInt.createFields(intName, fieldsText);
        text = text.replaceAll(intReg, '').trim();
      } else if (decMatch.group(6) == 'float') {
        final floatReg = RegExp(
            r'^float\s+\b([A-Z][a-z]+(?:[A-Z][a-z]+)*)\b\s+{([^{]*?)}',
            multiLine: false);

        final match = floatReg.firstMatch(text);
        if (match == null) {
          throw Exception('Invalid float declaration');
        }

        final floatName = match.group(1);
        final fieldsText = match.group(2);

        if (floatName == null || floatName.isEmpty) {
          throw Exception('Must have struct name.');
        }

        if (fieldsText == null || fieldsText.isEmpty) {
          throw Exception('Field cannot be empty.');
        }

        if (floats.containsKey(floatName)) {
          throw Exception('Only one float definition allowed.');
        }

        floats[floatName] = IdlFileFloat.createFields(floatName, fieldsText);
        text = text.replaceAll(floatReg, '').trim();
      } else {
        throw Exception('Invalid idl file.');
      }
    }

    return IdlFile(
      enums: enums,
      imports: imports,
      interfaces: interfaces,
      libraryName: libraryName,
      structs: structs,
      strings: strings,
      ints: ints,
      floats: floats,
    );
  }

  @override
  String toString() {
    var result = '';
    
    // two spaces indent
    const indent = '  ';

    result += 'library $libraryName;\n';

    if (imports.isNotEmpty) {
      result += '\n';
      imports.forEach((value) => result += 'import $value;\n');
    }

    final printAtts = (idlAtt) {
      if (idlAtt != null && idlAtt.attributes.isNotEmpty) {
        result += idlAtt.toString();
        result += ' ';
      }
    };

    enums.forEach((key, value) {
      result += '\nenum $key {\n';

      value.fields.forEach((field) {
        result += indent;
        printAtts(field.attributes);
        result += '${field.name},\n';
      });

      result += '}\n';
    });

    final printWt = (name, key, value) {
      result += '\n$name $key {\n';

      value.fields.forEach((field) {
        result += indent;
        printAtts(field.attributes);
        result += '${field.name}: ';
        result += '${field.type.toString()},\n';
      });

      result += '}\n';
    };

    structs.forEach((key, value) => printWt('struct', key, value));
    interfaces.forEach((key, value) => printWt('interface', key, value));

    return result;
  }
}