import 'dart:ffi';
import 'dart:io' show Platform;

import 'package:ffi/ffi.dart';
import 'dart:typed_data';

import 'dart:convert';

DynamicLibrary openLibrary(String name, [String path]) {
  path ??= 'src/rust/target/release/';
  name = 'lib' + name;
  return Platform.isAndroid || Platform.isLinux
      ? DynamicLibrary.open(path + name + '.so')
      : Platform.isWindows
          ? DynamicLibrary.open(path + name + '.dll')
          : DynamicLibrary.process();
}

class NativeString extends Struct {
  @Int64()
  int length;
  Pointer<Uint8> data;
}

extension NativeStringPointer on Pointer<NativeString> {
  String asString() {
    return utf8.decode(Uint8List.view(
      ref.data.asTypedList(ref.length).buffer,
      0,
      ref.length,
    ));
  }

  void release() {
    free(ref.data);
    free(this);
  }
}

extension NativeStringInto on String {
  Pointer<NativeString> asNativeString() {
    final dataString = utf8.encode(this);
    final resultData = allocate<Uint8>(count: dataString.length + 0x8);
    resultData.asTypedList(dataString.length).setAll(0, dataString);

    final utf8String = allocate<NativeString>().ref
      ..data = resultData
      ..length = dataString.length;

    return utf8String.addressOf;
  }
}

class Bytes extends Struct {
  @Int64()
  int length;
  Pointer<Uint8> data;
}

extension BytesPointer on Pointer<Bytes> {
  void release() {
    free(ref.data);
    free(this);
  }

  Uint8List asUint8List() {
    return Uint8List.fromList(ref.data.asTypedList(ref.length));
  }
}

extension BytesInto on Uint8List {
  Pointer<Bytes> asNativeBytes() {
    final result = allocate<Uint8>(count: length + 0x8);
    final listSource = result.asTypedList(length);
    listSource.setAll(0, this);

    final bytes = allocate<Bytes>().ref
      ..length = length
      ..data = result;

    return bytes.addressOf;
  }
}

class Type extends Struct {
  Pointer<NativeString> name;
  Pointer<Void> ptrData;
}

extension TypePointer on Pointer<Type> {
  void release() {
    ref.name.release();
    free(this);
  }
}

class ArrayType extends Struct {
  @Int64()
  int length;
  Pointer<Void> data; 
}

extension ArrayTypePointer on Pointer<ArrayType> {
  void release() {
    free(this);
  }
}

class AbiInternalError {
  static const int none = 0x0;
  static const int invalidArg = 0x1;
  static const int nullPtr = 0x2;
  static const int abort = 0x3;
  static const int callbackException = 0x4;
  static const int unimplemented = 0x5;
  static const int type = 0x6;
  static const int notAllowedOperation = 0x7;
}

class CallbackValue<T> {
  const CallbackValue(this.handle, this.value);
  final Pointer<Void> handle;
  final T value;
}

extension PointerUint8Mem on Pointer<Uint8> {
  void release() => free(this);
}

extension PointerInt8Mem on Pointer<Int8> {
  void release() => free(this);
}

extension PointerUint16Mem on Pointer<Uint16> {
  void release() => free(this);
}

extension PointerInt16Mem on Pointer<Int16> {
  void release() => free(this);
}

extension PointerUint32Mem on Pointer<Uint32> {
  void release() => free(this);
}

extension PointerInt32Mem on Pointer<Int32> {
  void release() => free(this);
}

extension PointerUint64Mem on Pointer<Uint64> {
  void release() => free(this);
}

extension PointerInt64Mem on Pointer<Int64> {
  void release() => free(this);
}

extension PointerDoubleMem on Pointer<Double> {
  void release() => free(this);
}

extension PointerFloatMem on Pointer<Float> {
  void release() => free(this);
}
