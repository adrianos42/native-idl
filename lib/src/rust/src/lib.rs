use std::ffi::c_void;

#[macro_use]
extern crate quote;

mod ffi_internal;
mod idl;
pub mod idl_enums;
mod idl_impl;
mod gen;

use crate::ffi_internal::*;
use crate::idl_enums::*;

#[repr(C)]
pub struct IdlLanguageServer {
    instance: Box<dyn crate::idl::IdlLanguageServer>,
}

#[no_mangle]
fn release_idl_language_server(instance: *mut IdlLanguageServer) -> i64 {
    IdlAbiError::Ok as i64
}

#[no_mangle]
fn factory_idl_language_server_create_instance(value: *mut *const IdlLanguageServer) -> i64 {
    unsafe {
        *value = Box::into_raw(Box::new(IdlLanguageServer {
            instance: gen::create_instance(),
        }));
    }
    IdlAbiError::Ok as i64
}

#[no_mangle]
fn method_idl_language_server_sync_add_enums(
    instance: *mut IdlLanguageServer,
    enums: *mut ArrayType,
) -> i64 {
    let mut ins = unsafe { Box::from_raw(instance) };

    let enums_array = unsafe { enums.read() };
    let data = enums_array.data as *const *const IdlEnum;
    let length = enums_array.length as isize;

    let mut enums_vec = vec![];
    for i in 0..length {
        let idl_enum = unsafe { data.offset(i).read().read() };
        let name = unsafe { idl_enum.name.read().to_string() };
        let mut field_names = vec![];

        let fields_array = unsafe { idl_enum.field_names.read() };
        let fields_data = fields_array.data as *const *const IdlEnumField;
        let fields_length = fields_array.length as isize;

        for i in 0..fields_length {
            let idl_enum_field = unsafe { fields_data.offset(i).read().read() };
            let field_name = unsafe { idl_enum_field.field_name.read().to_string() };
            field_names.push(idl::IdlEnumField { field_name });
        }

        enums_vec.push(idl::IdlEnum { name, field_names });
    }

    ins.instance.add_enums(enums_vec);

    let _ = Box::into_raw(ins);

    IdlAbiError::Ok as i64
}

#[no_mangle]
fn method_idl_language_server_sync_add_structs(
    instance: *mut IdlLanguageServer,
    structs: *mut ArrayType,
) -> i64 {
    IdlAbiError::Ok as i64
}

#[no_mangle]
fn method_idl_language_server_sync_add_interfaces(
    instance: *mut IdlLanguageServer,
    interfaces: *mut ArrayType,
) -> i64 {
    IdlAbiError::Ok as i64
}

#[no_mangle]
fn method_idl_language_server_sync_add_strings(
    instance: *mut IdlLanguageServer,
    strings: *mut ArrayType,
) -> i64 {
    IdlAbiError::Ok as i64
}

#[no_mangle]
fn method_idl_language_server_sync_add_ints(
    instance: *mut IdlLanguageServer,
    ints: *mut ArrayType,
) -> i64 {
    IdlAbiError::Ok as i64
}

#[no_mangle]
fn method_idl_language_server_sync_add_floats(
    instance: *mut IdlLanguageServer,
    floats: *mut ArrayType,
) -> i64 {
    IdlAbiError::Ok as i64
}

#[no_mangle]
fn method_idl_language_server_sync_finalize(instance: *mut IdlLanguageServer) -> i64 {
    IdlAbiError::Ok as i64
}

#[no_mangle]
fn method_idl_language_server_get_as_string(
    instance: *mut IdlLanguageServer,
    value: *mut *const NativeString,
) -> i64 {
    let ins = unsafe { Box::from_raw(instance) };
    let w = ins.instance.get_as_string();

    unsafe {
        *value = NativeString::from_string(w.as_str());
    }

    let _ = Box::into_raw(ins);

    IdlAbiError::Ok as i64
}

#[no_mangle]
fn method_idl_language_server_release_as_string(
    instance: *mut IdlLanguageServer,
    value: *mut NativeString,
) -> i64 {
    IdlAbiError::Ok as i64
}

#[repr(C)]
struct IdlInterface {
    name: *mut NativeString,
    fields: *mut ArrayType,
}

#[repr(C)]
struct IdlInterfaceField {
    field_name: *mut NativeString,
    idl_type: *mut IdlType,
    interface_attributes: *mut ArrayType,
}

#[repr(C)]
struct IdlStruct {
    name: *mut NativeString,
    fields: *mut ArrayType,
}

#[repr(C)]
struct IdlStructField {
    field_name: *mut NativeString,
    idl_type: *mut IdlType,
}

#[repr(C)]
struct IdlEnum {
    name: *mut NativeString,
    field_names: *mut ArrayType,
}

#[repr(C)]
struct IdlEnumField {
    field_name: *mut NativeString,
}

#[repr(C)]
struct IdlConst {
    name: *mut NativeString,
    fields: *mut ArrayType,
}

#[repr(C)]
struct IdlConstFIeld {
    field_name: *mut NativeString,
    field_value: *mut NativeString,
}

#[repr(C)]
struct IdlType {
    object: Type,
    object_type: i64,
}

#[repr(C)]
struct IdlTypeList {
    type_list: *mut ArrayType,
}

#[repr(C)]
struct IdlTypeTuple {
    type_list: *mut ArrayType,
}

#[repr(C)]
struct IdlTupleEntry {
    name: *mut NativeString,
    idl_type: *mut IdlType,
}

#[repr(C)]
struct IdlTypeArray {
    array_type: *mut IdlType,
}

#[repr(C)]
struct IdlMethod {
    args: *mut IdlType,
    return_type: *mut IdlType,
}

// #[no_mangle]
// fn method_calculator_sync_fuck(_instance: *mut Calculator, value: *mut *const ArrayType) -> i64 {
//     let value0 = Shit { length: 43 };
//     let value1 = Shit { length: 23 };
//     let value2 = Shit { length: 42 };
//     let value3 = Shit { length: 53489345 };

//     let mut vect = vec![value0, value1, value2, value3].into_boxed_slice();
//     let vect_ptr: *mut c_void = vect.as_mut_ptr() as *mut c_void;
//     let vav = vect_ptr as i64;

//     let inn_arr = ArrayType {
//         length: vect.len() as i64,
//         data: vect_ptr,
//     };

//     std::mem::forget(vect);

//     let value0 = Shit { length: inn_arr.length };
//     let value1 = Shit { length: 4323 };
//     let value2 = Shit { length: 4342 };
//     let value3 = Shit { length: 345 };

//     let mut vect = vec![value0, value1, value2, value3].into_boxed_slice();
//     let vect_ptr: *mut c_void = vect.as_mut_ptr() as *mut c_void;

//     let inn_arr2 = ArrayType {
//         length: vect.len() as i64,
//         data: vect_ptr,
//     };

//     std::mem::forget(vect);

//      let value0 = Shit { length: vav as i64 };
//     let value1 = Shit { length: 4323 };
//     let value2 = Shit { length: 4342 };
//     let value3 = Shit { length: 345 };

//     let mut vect = vec![value0, value1, value2, value3].into_boxed_slice();
//     let vect_ptr: *mut c_void = vect.as_mut_ptr() as *mut c_void;

//     let inn_arr1 = ArrayType {
//         length: vect.len() as i64,
//         data: vect_ptr,
//     };

//     std::mem::forget(vect);

//     let mut vect = vec![inn_arr, inn_arr1, inn_arr2].into_boxed_slice();
//     let vect_ptr: *mut c_void = vect.as_mut_ptr() as *mut c_void;

//     let result = ArrayType {
//         length: vect.len() as i64,
//         data: vect_ptr,
//     };

//     std::mem::forget(vect);

//     unsafe {
//         *value = Box::into_raw(Box::new(result));
//     }

//     0
// }
