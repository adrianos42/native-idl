#include "idl_internal.h"

const int64_t IDL_TYPES_INT = 0x0;
const int64_t IDL_TYPES_FLOAT = 0x1;
const int64_t IDL_TYPES_BOOL = 0x2;
const int64_t IDL_TYPES_ENUM = 0x3;
const int64_t IDL_TYPES_STRING = 0x4;
const int64_t IDL_TYPES_BYTES = 0x5;
const int64_t IDL_TYPES_STRUCT = 0x6;
const int64_t IDL_TYPES_METHOD = 0x7;
const int64_t IDL_TYPES_INTERFACE = 0x8;
const int64_t IDL_TYPES_TYPE_LIST = 0x9;
const int64_t IDL_TYPES_TUPLE = 0xA;
const int64_t IDL_TYPES_EMPTY = 0xB;
const int64_t IDL_TYPES_ARRAY = 0xC;

const int64_t IDL_INTERFACE_ATTRIBUTES_P_GET = 0x0;
const int64_t IDL_INTERFACE_ATTRIBUTES_P_SET = 0x1;
const int64_t IDL_INTERFACE_ATTRIBUTES_M_STREAM = 0x2;
const int64_t IDL_INTERFACE_ATTRIBUTES_M_FACTORY = 0x3;
const int64_t IDL_INTERFACE_ATTRIBUTES_M_ASYNC = 0x4;
const int64_t IDL_INTERFACE_ATTRIBUTES_M_SYNC = 0x5;

typedef struct IdlLanguage {
} IdlLanguage;

int64_t idl_language_create_instance(IdlLanguage**);
int64_t idl_language_add_enums(IdlLanguage* instance, ArrayType* enums);
int64_t idl_language_add_interfaces(IdlLanguage* instance, ArrayType* interfaces);
int64_t idl_language_add_strings(IdlLanguage* instance, ArrayType* strings);
int64_t idl_language_add_ints(IdlLanguage* instance, ArrayType* ints);
int64_t idl_language_add_floats(IdlLanguage* instance, ArrayType* floats);
int64_t idl_language_compile(IdlLanguage* instance, NativeString** ret_value);

typedef struct IdlType {
  ArrayType* object;
  int64_t type;
} IdlType;

typedef struct IdlInterface {
  NativeString* name;
  ArrayType* fields;
} IdlInterface;

typedef struct IdlInterfaceField {
  NativeString* field_name;
  IdlType* type;
  ArrayType* IdlInterfaceAttributes;
} IdlInterfaceField;

typedef struct IdlStruct {
  NativeString* name;
  ArrayType* fields;
} IdlStruct;

typedef struct IdlStructField {
  NativeString* field_name;
  IdlType* type;
} IdlStructField;

typedef struct IdlEnum {
  NativeString* name;
  ArrayType* field_names;
} IdlEnums;

typedef struct IdlTypeList {
  ArrayType* list;
} IdlTypeList;

typedef struct IdlTypeTuple {
  ArrayType* list;
} IdlTypeTuple;

typedef struct IdlTypeArray {
  IdlType* type;
} IdlTypeArray;

typedef struct IdlMethod {
  IdlType* type;
  IdlType* return_type;
} IdlMethod;