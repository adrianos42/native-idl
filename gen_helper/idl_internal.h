#include "stdint.h"

const int64_t ERROR_NONE = 0x0;
const int64_t ERROR_INVALID_ARG = 0x1;
const int64_t ERROR_NULL_PTR = 0x2;
const int64_t ERROR_ABORT = 0x3;
const int64_t ERROR_CALLBACK_EXCEPTION = 0x4;
const int64_t ERROR_UNIMPLEMENTED = 0x5;
const int64_t ERROR_TYPE = 0x6;
const int64_t ERROR_NOT_ALLOWED_OPERATION = 0x7;

typedef struct NativeString {
  int64_t length;
  uint8_t* data;
} NativeString;

typedef struct Bytes {
  int64_t length;
  uint8_t* data;
} Bytes;

typedef struct Type {
  NativeString* name;
  void* ptrData;
} Type;

typedef struct ArrayType {
  int64_t* length;
  void* data;
} ArrayType;

typedef struct CallbackValue {
  void* handle;
  void* value;
} CallbackValue;
