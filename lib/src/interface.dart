import 'dart:ffi';
import 'dart:async';
import 'dart:typed_data';
import 'dart:collection';
import 'package:ffi/ffi.dart';
import 'ffi_internal.dart';

// ------------------------------------------
// This is generated code.
// Modifying it is pointless, don't be silly.
// ------------------------------------------

enum IdlAbiError {
  ok,
  invalidArg,
  nullPtr,
  abort,
  callbackException,
  unimplemented,
  notAllowedOperation,
}

enum IdlInterfaceAttributes {
  pGet,
  pSet,
  mStream,
  mFactory,
  mAsync,
  mSync,
}

enum IdlTypes {
  nInt,
  nFloat,
  nBool,
  nEnum,
  nString,
  nBytes,
  nStruct,
  nMethod,
  nInterface,
  nTypeList,
  nTuple,
  nVoid,
  nNone,
  nArray,
}

class _IdlInterface extends Struct {
  Pointer<NativeString> name;

  Pointer<ArrayType> fields;

  static Pointer<_IdlInterface> from(IdlInterface value) {
    final result = allocate<_IdlInterface>();
    final fValueName = value.name.asNativeString();
    result.ref.name = fValueName;
    final fValueFields = allocate<ArrayType>();
    final lengthFields = value.fields.length;
    final dataFields = allocate<Pointer<_IdlInterfaceField>>(count: lengthFields);
    for (var i = 0; i < lengthFields; i += 1) {
      final st = _IdlInterfaceField.from(value.fields[i]);
      dataFields.elementAt(i).value = st;
    }

    fValueFields.ref.data = dataFields.cast<Void>();
    fValueFields.ref.length = lengthFields;

    result.ref.fields = fValueFields;
    return result;
  }
}

class IdlInterface {
  IdlInterface({String name, List<IdlInterfaceField> fields})
      : _name = name,
        _fields = fields;

  factory IdlInterface._from(Pointer<_IdlInterface> value) {
    final sValueName = value.ref.name.asString();
    final data = value.ref.fields.ref.data;
    final length = value.ref.fields.ref.length;
    final listSource = data.cast<_IdlInterfaceField>();
    final sValueFields = <IdlInterfaceField>[];
    for (var i = 0; i < length; i += 1) {
      sValueFields.add(IdlInterfaceField._from(listSource.elementAt(i)));
    }

    return IdlInterface(
      name: sValueName,
      fields: sValueFields,
    );
  }

  String _name;

  List<IdlInterfaceField> _fields;

  String get name => _name;
  set name(String value) {
    assert(value != null);
    _name = value;
  }

  List<IdlInterfaceField> get fields => _fields;
  set fields(List<IdlInterfaceField> value) {
    assert(value != null);
    _fields = value;
  }
}

class _IdlInterfaceField extends Struct {
  Pointer<NativeString> fieldName;

  Pointer<_IdlType> idlType;

  Pointer<ArrayType> interfaceAttributes;

  static Pointer<_IdlInterfaceField> from(IdlInterfaceField value) {
    final result = allocate<_IdlInterfaceField>();
    final fValueFieldName = value.fieldName.asNativeString();
    result.ref.fieldName = fValueFieldName;
    final fValueIdlType = _IdlType.from(value.idlType);
    result.ref.idlType = fValueIdlType;
    final fValueInterfaceAttributes = allocate<ArrayType>();
    final lengthInterfaceAttributes = value.interfaceAttributes.length;
    final dataInterfaceAttributes = allocate<Int64>(count: lengthInterfaceAttributes);
    final listSourceInterfaceAttributes = dataInterfaceAttributes.asTypedList(lengthInterfaceAttributes);
    listSourceInterfaceAttributes.setAll(0, value.interfaceAttributes.map((value) => IdlInterfaceAttributes.values.indexOf(value)));

    fValueInterfaceAttributes.ref.data = dataInterfaceAttributes.cast<Void>();
    fValueInterfaceAttributes.ref.length = lengthInterfaceAttributes;

    result.ref.interfaceAttributes = fValueInterfaceAttributes;
    return result;
  }
}

class IdlInterfaceField {
  IdlInterfaceField({String fieldName, IdlType idlType, List<IdlInterfaceAttributes> interfaceAttributes})
      : _fieldName = fieldName,
        _idlType = idlType,
        _interfaceAttributes = interfaceAttributes;

  factory IdlInterfaceField._from(Pointer<_IdlInterfaceField> value) {
    final sValueFieldName = value.ref.fieldName.asString();
    final sValueIdlType = IdlType._from(value.ref.idlType);
    final data = value.ref.interfaceAttributes.ref.data;
    final length = value.ref.interfaceAttributes.ref.length;
    final listSource = data.cast<Int64>().asTypedList(length);
    final sValueInterfaceAttributes = listSource.map((value) => IdlInterfaceAttributes.values[value]).toList();

    return IdlInterfaceField(
      fieldName: sValueFieldName,
      idlType: sValueIdlType,
      interfaceAttributes: sValueInterfaceAttributes,
    );
  }

  String _fieldName;

  IdlType _idlType;

  List<IdlInterfaceAttributes> _interfaceAttributes;

  String get fieldName => _fieldName;
  set fieldName(String value) {
    assert(value != null);
    _fieldName = value;
  }

  IdlType get idlType => _idlType;
  set idlType(IdlType value) {
    assert(value != null);
    _idlType = value;
  }

  List<IdlInterfaceAttributes> get interfaceAttributes => _interfaceAttributes;
  set interfaceAttributes(List<IdlInterfaceAttributes> value) {
    assert(value != null);
    _interfaceAttributes = value;
  }
}

class _IdlStruct extends Struct {
  Pointer<NativeString> name;

  Pointer<ArrayType> fields;

  static Pointer<_IdlStruct> from(IdlStruct value) {
    final result = allocate<_IdlStruct>();
    final fValueName = value.name.asNativeString();
    result.ref.name = fValueName;
    final fValueFields = allocate<ArrayType>();
    final lengthFields = value.fields.length;
    final dataFields = allocate<Pointer<_IdlStructField>>(count: lengthFields);
    for (var i = 0; i < lengthFields; i += 1) {
      final st = _IdlStructField.from(value.fields[i]);
      dataFields.elementAt(i).value = st;
    }

    fValueFields.ref.data = dataFields.cast<Void>();
    fValueFields.ref.length = lengthFields;

    result.ref.fields = fValueFields;
    return result;
  }
}

class IdlStruct {
  IdlStruct({String name, List<IdlStructField> fields})
      : _name = name,
        _fields = fields;

  factory IdlStruct._from(Pointer<_IdlStruct> value) {
    final sValueName = value.ref.name.asString();
    final data = value.ref.fields.ref.data;
    final length = value.ref.fields.ref.length;
    final listSource = data.cast<_IdlStructField>();
    final sValueFields = <IdlStructField>[];
    for (var i = 0; i < length; i += 1) {
      sValueFields.add(IdlStructField._from(listSource.elementAt(i)));
    }

    return IdlStruct(
      name: sValueName,
      fields: sValueFields,
    );
  }

  String _name;

  List<IdlStructField> _fields;

  String get name => _name;
  set name(String value) {
    assert(value != null);
    _name = value;
  }

  List<IdlStructField> get fields => _fields;
  set fields(List<IdlStructField> value) {
    assert(value != null);
    _fields = value;
  }
}

class _IdlStructField extends Struct {
  Pointer<NativeString> fieldName;

  Pointer<_IdlType> idlType;

  static Pointer<_IdlStructField> from(IdlStructField value) {
    final result = allocate<_IdlStructField>();
    final fValueFieldName = value.fieldName.asNativeString();
    result.ref.fieldName = fValueFieldName;
    final fValueIdlType = _IdlType.from(value.idlType);
    result.ref.idlType = fValueIdlType;
    return result;
  }
}

class IdlStructField {
  IdlStructField({String fieldName, IdlType idlType})
      : _fieldName = fieldName,
        _idlType = idlType;

  factory IdlStructField._from(Pointer<_IdlStructField> value) {
    final sValueFieldName = value.ref.fieldName.asString();
    final sValueIdlType = IdlType._from(value.ref.idlType);
    return IdlStructField(
      fieldName: sValueFieldName,
      idlType: sValueIdlType,
    );
  }

  String _fieldName;

  IdlType _idlType;

  String get fieldName => _fieldName;
  set fieldName(String value) {
    assert(value != null);
    _fieldName = value;
  }

  IdlType get idlType => _idlType;
  set idlType(IdlType value) {
    assert(value != null);
    _idlType = value;
  }
}

class _IdlEnum extends Struct {
  Pointer<NativeString> name;

  Pointer<ArrayType> fieldNames;

  static Pointer<_IdlEnum> from(IdlEnum value) {
    final result = allocate<_IdlEnum>();
    final fValueName = value.name.asNativeString();
    result.ref.name = fValueName;
    final fValueFieldNames = allocate<ArrayType>();
    final lengthFieldNames = value.fieldNames.length;
    final dataFieldNames = allocate<Pointer<_IdlEnumField>>(count: lengthFieldNames);
    for (var i = 0; i < lengthFieldNames; i += 1) {
      final st = _IdlEnumField.from(value.fieldNames[i]);
      dataFieldNames.elementAt(i).value = st;
    }

    fValueFieldNames.ref.data = dataFieldNames.cast<Void>();
    fValueFieldNames.ref.length = lengthFieldNames;

    result.ref.fieldNames = fValueFieldNames;
    return result;
  }
}

class IdlEnum {
  IdlEnum({String name, List<IdlEnumField> fieldNames})
      : _name = name,
        _fieldNames = fieldNames;

  factory IdlEnum._from(Pointer<_IdlEnum> value) {
    final sValueName = value.ref.name.asString();
    final data = value.ref.fieldNames.ref.data;
    final length = value.ref.fieldNames.ref.length;
    final listSource = data.cast<_IdlEnumField>();
    final sValueFieldNames = <IdlEnumField>[];
    for (var i = 0; i < length; i += 1) {
      sValueFieldNames.add(IdlEnumField._from(listSource.elementAt(i)));
    }

    return IdlEnum(
      name: sValueName,
      fieldNames: sValueFieldNames,
    );
  }

  String _name;

  List<IdlEnumField> _fieldNames;

  String get name => _name;
  set name(String value) {
    assert(value != null);
    _name = value;
  }

  List<IdlEnumField> get fieldNames => _fieldNames;
  set fieldNames(List<IdlEnumField> value) {
    assert(value != null);
    _fieldNames = value;
  }
}

class _IdlEnumField extends Struct {
  Pointer<NativeString> fieldName;

  static Pointer<_IdlEnumField> from(IdlEnumField value) {
    final result = allocate<_IdlEnumField>();
    final fValueFieldName = value.fieldName.asNativeString();
    result.ref.fieldName = fValueFieldName;
    return result;
  }
}

class IdlEnumField {
  IdlEnumField({String fieldName}) : _fieldName = fieldName;

  factory IdlEnumField._from(Pointer<_IdlEnumField> value) {
    final sValueFieldName = value.ref.fieldName.asString();
    return IdlEnumField(
      fieldName: sValueFieldName,
    );
  }

  String _fieldName;

  String get fieldName => _fieldName;
  set fieldName(String value) {
    assert(value != null);
    _fieldName = value;
  }
}

class _IdlConst extends Struct {
  Pointer<NativeString> name;

  Pointer<ArrayType> fields;

  static Pointer<_IdlConst> from(IdlConst value) {
    final result = allocate<_IdlConst>();
    final fValueName = value.name.asNativeString();
    result.ref.name = fValueName;
    final fValueFields = allocate<ArrayType>();
    final lengthFields = value.fields.length;
    final dataFields = allocate<Pointer<_IdlConstField>>(count: lengthFields);
    for (var i = 0; i < lengthFields; i += 1) {
      final st = _IdlConstField.from(value.fields[i]);
      dataFields.elementAt(i).value = st;
    }

    fValueFields.ref.data = dataFields.cast<Void>();
    fValueFields.ref.length = lengthFields;

    result.ref.fields = fValueFields;
    return result;
  }
}

class IdlConst {
  IdlConst({String name, List<IdlConstField> fields})
      : _name = name,
        _fields = fields;

  factory IdlConst._from(Pointer<_IdlConst> value) {
    final sValueName = value.ref.name.asString();
    final data = value.ref.fields.ref.data;
    final length = value.ref.fields.ref.length;
    final listSource = data.cast<_IdlConstField>();
    final sValueFields = <IdlConstField>[];
    for (var i = 0; i < length; i += 1) {
      sValueFields.add(IdlConstField._from(listSource.elementAt(i)));
    }

    return IdlConst(
      name: sValueName,
      fields: sValueFields,
    );
  }

  String _name;

  List<IdlConstField> _fields;

  String get name => _name;
  set name(String value) {
    assert(value != null);
    _name = value;
  }

  List<IdlConstField> get fields => _fields;
  set fields(List<IdlConstField> value) {
    assert(value != null);
    _fields = value;
  }
}

class _IdlConstField extends Struct {
  Pointer<NativeString> fieldName;

  Pointer<NativeString> fieldValue;

  static Pointer<_IdlConstField> from(IdlConstField value) {
    final result = allocate<_IdlConstField>();
    final fValueFieldName = value.fieldName.asNativeString();
    result.ref.fieldName = fValueFieldName;
    final fValueFieldValue = value.fieldValue.asNativeString();
    result.ref.fieldValue = fValueFieldValue;
    return result;
  }
}

class IdlConstField {
  IdlConstField({String fieldName, String fieldValue})
      : _fieldName = fieldName,
        _fieldValue = fieldValue;

  factory IdlConstField._from(Pointer<_IdlConstField> value) {
    final sValueFieldName = value.ref.fieldName.asString();
    final sValueFieldValue = value.ref.fieldValue.asString();
    return IdlConstField(
      fieldName: sValueFieldName,
      fieldValue: sValueFieldValue,
    );
  }

  String _fieldName;

  String _fieldValue;

  String get fieldName => _fieldName;
  set fieldName(String value) {
    assert(value != null);
    _fieldName = value;
  }

  String get fieldValue => _fieldValue;
  set fieldValue(String value) {
    assert(value != null);
    _fieldValue = value;
  }
}

class _IdlType extends Struct {
  Pointer<Type> object;

  @Int64()
  int objectType;

  static Pointer<_IdlType> from(IdlType value) {
    final result = allocate<_IdlType>();
    final fValueObject = allocate<Type>();
    if (value.object == null) {
      final ptrData = allocate<Int64>();
      ptrData.value = 0;
      fValueObject.ref.ptrData = ptrData.cast<Void>();
      fValueObject.ref.name = 'none'.asNativeString();
    } else if (value.object is String) {
      fValueObject.ref.ptrData = (value.object as String).asNativeString().cast<Void>();
      fValueObject.ref.name = 'string'.asNativeString();
    } else if (value.object is IdlType) {
      fValueObject.ref.ptrData = _IdlType.from(value.object as IdlType).cast<Void>();
      fValueObject.ref.name = 'IdlType'.asNativeString();
    } else if (value.object is IdlMethod) {
      fValueObject.ref.ptrData = _IdlMethod.from(value.object as IdlMethod).cast<Void>();
      fValueObject.ref.name = 'IdlMethod'.asNativeString();
    } else if (value.object is IdlTypeList) {
      fValueObject.ref.ptrData = _IdlTypeList.from(value.object as IdlTypeList).cast<Void>();
      fValueObject.ref.name = 'IdlTypeList'.asNativeString();
    } else if (value.object is IdlTypeTuple) {
      fValueObject.ref.ptrData = _IdlTypeTuple.from(value.object as IdlTypeTuple).cast<Void>();
      fValueObject.ref.name = 'IdlTypeTuple'.asNativeString();
    } else {
      throw Exception('Invalid type in [].');
    }

    result.ref.object = fValueObject;
    final fValueObjectType = IdlTypes.values.indexOf(value.objectType);
    result.ref.objectType = fValueObjectType;
    return result;
  }
}

class IdlType {
  IdlType({dynamic object, IdlTypes objectType})
      : _object = object,
        _objectType = objectType;

  factory IdlType._from(Pointer<_IdlType> value) {
    final typeName = value.ref.object.ref.name.asString();
    final typeValue = value.ref.object.ref.ptrData;
    dynamic sValueObject;
    if (typeName == 'none') {
      if (typeValue.cast<Int64>().value != 0) {
        throw ArgumentError('[object] is not zero.');
      }
      sValueObject = null;
    } else if (typeName == 'string') {
      sValueObject = typeValue.cast<NativeString>().asString();
    } else if (typeName == 'IdlType') {
      sValueObject = IdlType._from(typeValue.cast<_IdlType>());
    } else if (typeName == 'IdlMethod') {
      sValueObject = IdlMethod._from(typeValue.cast<_IdlMethod>());
    } else if (typeName == 'IdlTypeList') {
      sValueObject = IdlTypeList._from(typeValue.cast<_IdlTypeList>());
    } else if (typeName == 'IdlTypeTuple') {
      sValueObject = IdlTypeTuple._from(typeValue.cast<_IdlTypeTuple>());
    } else {
      throw Exception('Invalid type in [object].');
    }
    final sValueObjectType = IdlTypes.values[value.ref.objectType];
    return IdlType(
      object: sValueObject,
      objectType: sValueObjectType,
    );
  }

  dynamic _object;

  IdlTypes _objectType;

  dynamic get object => _object;
  set object(dynamic value) {
    assert(value != null);
    _object = value;
  }

  IdlTypes get objectType => _objectType;
  set objectType(IdlTypes value) {
    assert(value != null);
    _objectType = value;
  }
}

class _IdlTypeList extends Struct {
  Pointer<ArrayType> typeList;

  static Pointer<_IdlTypeList> from(IdlTypeList value) {
    final result = allocate<_IdlTypeList>();
    final fValueTypeList = allocate<ArrayType>();
    final lengthTypeList = value.typeList.length;
    final dataTypeList = allocate<Pointer<_IdlType>>(count: lengthTypeList);
    for (var i = 0; i < lengthTypeList; i += 1) {
      final st = _IdlType.from(value.typeList[i]);
      dataTypeList.elementAt(i).value = st;
    }

    fValueTypeList.ref.data = dataTypeList.cast<Void>();
    fValueTypeList.ref.length = lengthTypeList;

    result.ref.typeList = fValueTypeList;
    return result;
  }
}

class IdlTypeList {
  IdlTypeList({List<IdlType> typeList}) : _typeList = typeList;

  factory IdlTypeList._from(Pointer<_IdlTypeList> value) {
    final data = value.ref.typeList.ref.data;
    final length = value.ref.typeList.ref.length;
    final listSource = data.cast<_IdlType>();
    final sValueTypeList = <IdlType>[];
    for (var i = 0; i < length; i += 1) {
      sValueTypeList.add(IdlType._from(listSource.elementAt(i)));
    }

    return IdlTypeList(
      typeList: sValueTypeList,
    );
  }

  List<IdlType> _typeList;

  List<IdlType> get typeList => _typeList;
  set typeList(List<IdlType> value) {
    assert(value != null);
    _typeList = value;
  }
}

class _IdlTypeTuple extends Struct {
  Pointer<ArrayType> typeList;

  static Pointer<_IdlTypeTuple> from(IdlTypeTuple value) {
    final result = allocate<_IdlTypeTuple>();
    final fValueTypeList = allocate<ArrayType>();
    final lengthTypeList = value.typeList.length;
    final dataTypeList = allocate<Pointer<_IdlTupleEntry>>(count: lengthTypeList);
    for (var i = 0; i < lengthTypeList; i += 1) {
      final st = _IdlTupleEntry.from(value.typeList[i]);
      dataTypeList.elementAt(i).value = st;
    }

    fValueTypeList.ref.data = dataTypeList.cast<Void>();
    fValueTypeList.ref.length = lengthTypeList;

    result.ref.typeList = fValueTypeList;
    return result;
  }
}

class IdlTypeTuple {
  IdlTypeTuple({List<IdlTupleEntry> typeList}) : _typeList = typeList;

  factory IdlTypeTuple._from(Pointer<_IdlTypeTuple> value) {
    final data = value.ref.typeList.ref.data;
    final length = value.ref.typeList.ref.length;
    final listSource = data.cast<_IdlTupleEntry>();
    final sValueTypeList = <IdlTupleEntry>[];
    for (var i = 0; i < length; i += 1) {
      sValueTypeList.add(IdlTupleEntry._from(listSource.elementAt(i)));
    }

    return IdlTypeTuple(
      typeList: sValueTypeList,
    );
  }

  List<IdlTupleEntry> _typeList;

  List<IdlTupleEntry> get typeList => _typeList;
  set typeList(List<IdlTupleEntry> value) {
    assert(value != null);
    _typeList = value;
  }
}

class _IdlTupleEntry extends Struct {
  Pointer<NativeString> name;

  Pointer<_IdlType> idlType;

  static Pointer<_IdlTupleEntry> from(IdlTupleEntry value) {
    final result = allocate<_IdlTupleEntry>();
    final fValueName = value.name.asNativeString();
    result.ref.name = fValueName;
    final fValueIdlType = _IdlType.from(value.idlType);
    result.ref.idlType = fValueIdlType;
    return result;
  }
}

class IdlTupleEntry {
  IdlTupleEntry({String name, IdlType idlType})
      : _name = name,
        _idlType = idlType;

  factory IdlTupleEntry._from(Pointer<_IdlTupleEntry> value) {
    final sValueName = value.ref.name.asString();
    final sValueIdlType = IdlType._from(value.ref.idlType);
    return IdlTupleEntry(
      name: sValueName,
      idlType: sValueIdlType,
    );
  }

  String _name;

  IdlType _idlType;

  String get name => _name;
  set name(String value) {
    assert(value != null);
    _name = value;
  }

  IdlType get idlType => _idlType;
  set idlType(IdlType value) {
    assert(value != null);
    _idlType = value;
  }
}

class _IdlTypeArray extends Struct {
  Pointer<_IdlType> arrayType;

  static Pointer<_IdlTypeArray> from(IdlTypeArray value) {
    final result = allocate<_IdlTypeArray>();
    final fValueArrayType = _IdlType.from(value.arrayType);
    result.ref.arrayType = fValueArrayType;
    return result;
  }
}

class IdlTypeArray {
  IdlTypeArray({IdlType arrayType}) : _arrayType = arrayType;

  factory IdlTypeArray._from(Pointer<_IdlTypeArray> value) {
    final sValueArrayType = IdlType._from(value.ref.arrayType);
    return IdlTypeArray(
      arrayType: sValueArrayType,
    );
  }

  IdlType _arrayType;

  IdlType get arrayType => _arrayType;
  set arrayType(IdlType value) {
    assert(value != null);
    _arrayType = value;
  }
}

class _IdlMethod extends Struct {
  Pointer<_IdlType> args;

  Pointer<_IdlType> returnType;

  static Pointer<_IdlMethod> from(IdlMethod value) {
    final result = allocate<_IdlMethod>();
    final fValueArgs = _IdlType.from(value.args);
    result.ref.args = fValueArgs;
    final fValueReturnType = _IdlType.from(value.returnType);
    result.ref.returnType = fValueReturnType;
    return result;
  }
}

class IdlMethod {
  IdlMethod({IdlType args, IdlType returnType})
      : _args = args,
        _returnType = returnType;

  factory IdlMethod._from(Pointer<_IdlMethod> value) {
    final sValueArgs = IdlType._from(value.ref.args);
    final sValueReturnType = IdlType._from(value.ref.returnType);
    return IdlMethod(
      args: sValueArgs,
      returnType: sValueReturnType,
    );
  }

  IdlType _args;

  IdlType _returnType;

  IdlType get args => _args;
  set args(IdlType value) {
    assert(value != null);
    _args = value;
  }

  IdlType get returnType => _returnType;
  set returnType(IdlType value) {
    assert(value != null);
    _returnType = value;
  }
}

class IdlLanguageServer {
  factory IdlLanguageServer() {
    final resultPtr = allocate<Pointer<_IdlLanguageServer>>();
    try {
      final resultError = IdlLanguageServer._factoryCreateInstance(resultPtr);
      if (resultError != 0) _handleFactoryError(resultError);
      final result = resultPtr.value;
      return IdlLanguageServer._from(result);
    } finally {
      free(resultPtr);
    }
  }

  IdlLanguageServer._from(this._instance);

  final Pointer<_IdlLanguageServer> _instance;

  static final DynamicLibrary _lib = openLibrary('idl_gen');

  static final _ReleaseIdlLanguageServerFunc _releaseInstance = _lib.lookupFunction<_ReleaseIdlLanguageServerNative, _ReleaseIdlLanguageServerFunc>('release_idl_language_server');

  static final _FactoryIdlLanguageServerCreateInstanceFunc _factoryCreateInstance = _lib.lookupFunction<_FactoryIdlLanguageServerCreateInstanceNative, _FactoryIdlLanguageServerCreateInstanceFunc>('factory_idl_language_server_create_instance');

  static final _MethodIdlLanguageServerSyncAddEnumsFunc _methodSyncAddEnums = _lib.lookupFunction<_MethodIdlLanguageServerSyncAddEnumsNative, _MethodIdlLanguageServerSyncAddEnumsFunc>('method_idl_language_server_sync_add_enums');

  static final _MethodIdlLanguageServerSyncAddStructsFunc _methodSyncAddStructs = _lib.lookupFunction<_MethodIdlLanguageServerSyncAddStructsNative, _MethodIdlLanguageServerSyncAddStructsFunc>('method_idl_language_server_sync_add_structs');

  static final _MethodIdlLanguageServerSyncAddInterfacesFunc _methodSyncAddInterfaces = _lib.lookupFunction<_MethodIdlLanguageServerSyncAddInterfacesNative, _MethodIdlLanguageServerSyncAddInterfacesFunc>('method_idl_language_server_sync_add_interfaces');

  static final _MethodIdlLanguageServerSyncAddStringsFunc _methodSyncAddStrings = _lib.lookupFunction<_MethodIdlLanguageServerSyncAddStringsNative, _MethodIdlLanguageServerSyncAddStringsFunc>('method_idl_language_server_sync_add_strings');

  static final _MethodIdlLanguageServerSyncAddIntsFunc _methodSyncAddInts = _lib.lookupFunction<_MethodIdlLanguageServerSyncAddIntsNative, _MethodIdlLanguageServerSyncAddIntsFunc>('method_idl_language_server_sync_add_ints');

  static final _MethodIdlLanguageServerSyncAddFloatsFunc _methodSyncAddFloats = _lib.lookupFunction<_MethodIdlLanguageServerSyncAddFloatsNative, _MethodIdlLanguageServerSyncAddFloatsFunc>('method_idl_language_server_sync_add_floats');

  static final _MethodIdlLanguageServerSyncFinalizeFunc _methodSyncFinalize = _lib.lookupFunction<_MethodIdlLanguageServerSyncFinalizeNative, _MethodIdlLanguageServerSyncFinalizeFunc>('method_idl_language_server_sync_finalize');

  static final _MethodIdlLanguageServerGetAsStringFunc _methodGetAsString = _lib.lookupFunction<_MethodIdlLanguageServerGetAsStringNative, _MethodIdlLanguageServerGetAsStringFunc>('method_idl_language_server_get_as_string');

  static final _MethodIdlLanguageServerReleaseAsStringFunc _methodReleaseAsString = _lib.lookupFunction<_MethodIdlLanguageServerReleaseAsStringNative, _MethodIdlLanguageServerReleaseAsStringFunc>('method_idl_language_server_release_as_string');

  void release() {
    final resultError = IdlLanguageServer._releaseInstance(_instance);
    if (resultError != 0) _handleReleaseError(resultError);
  }

  void _handleError(int error) {
    switch (error) {
      case AbiInternalError.none:
        return;
      case AbiInternalError.invalidArg:
        throw ArgumentError('[IdlLanguageServer]');
      default:
        throw Exception('Error $error, [IdlLanguageServer].');
    }
  }

  static void _handleFactoryError(int error) {
    switch (error) {
      case AbiInternalError.invalidArg:
        throw ArgumentError('[IdlLanguageServer]');
      case AbiInternalError.abort:
      default:
        throw Error();
    }
  }

  static void _handleReleaseError(int error) {
    throw Exception('Error releasing instance: [IdlLanguageServer].');
  }

  void addEnums(List<IdlEnum> enums) {
    final fValueEnums = allocate<ArrayType>();
    final lengthEnums = enums.length;
    final dataEnums = allocate<Pointer<_IdlEnum>>(count: lengthEnums);
    for (var i = 0; i < lengthEnums; i += 1) {
      final st = _IdlEnum.from(enums[i]);
      dataEnums.elementAt(i).value = st;
    }

    fValueEnums.ref.data = dataEnums.cast<Void>();
    fValueEnums.ref.length = lengthEnums;

    try {
      final resultError = IdlLanguageServer._methodSyncAddEnums(_instance, fValueEnums);
      if (resultError != 0) _handleError(resultError);
    } finally {
      fValueEnums.release();
    }
  }

  void addStructs(List<IdlStruct> structs) {
    final fValueStructs = allocate<ArrayType>();
    final lengthStructs = structs.length;
    final dataStructs = allocate<Pointer<_IdlStruct>>(count: lengthStructs);
    for (var i = 0; i < lengthStructs; i += 1) {
      final st = _IdlStruct.from(structs[i]);
      dataStructs.elementAt(i).value = st;
    }

    fValueStructs.ref.data = dataStructs.cast<Void>();
    fValueStructs.ref.length = lengthStructs;

    try {
      final resultError = IdlLanguageServer._methodSyncAddStructs(_instance, fValueStructs);
      if (resultError != 0) _handleError(resultError);
    } finally {
      fValueStructs.release();
    }
  }

  void addInterfaces(List<IdlInterface> interfaces) {
    final fValueInterfaces = allocate<ArrayType>();
    final lengthInterfaces = interfaces.length;
    final dataInterfaces = allocate<Pointer<_IdlInterface>>(count: lengthInterfaces);
    for (var i = 0; i < lengthInterfaces; i += 1) {
      final st = _IdlInterface.from(interfaces[i]);
      dataInterfaces.elementAt(i).value = st;
    }

    fValueInterfaces.ref.data = dataInterfaces.cast<Void>();
    fValueInterfaces.ref.length = lengthInterfaces;

    try {
      final resultError = IdlLanguageServer._methodSyncAddInterfaces(_instance, fValueInterfaces);
      if (resultError != 0) _handleError(resultError);
    } finally {
      fValueInterfaces.release();
    }
  }

  void addStrings(List<IdlConst> strings) {
    final fValueStrings = allocate<ArrayType>();
    final lengthStrings = strings.length;
    final dataStrings = allocate<Pointer<_IdlConst>>(count: lengthStrings);
    for (var i = 0; i < lengthStrings; i += 1) {
      final st = _IdlConst.from(strings[i]);
      dataStrings.elementAt(i).value = st;
    }

    fValueStrings.ref.data = dataStrings.cast<Void>();
    fValueStrings.ref.length = lengthStrings;

    try {
      final resultError = IdlLanguageServer._methodSyncAddStrings(_instance, fValueStrings);
      if (resultError != 0) _handleError(resultError);
    } finally {
      fValueStrings.release();
    }
  }

  void addInts(List<IdlConst> ints) {
    final fValueInts = allocate<ArrayType>();
    final lengthInts = ints.length;
    final dataInts = allocate<Pointer<_IdlConst>>(count: lengthInts);
    for (var i = 0; i < lengthInts; i += 1) {
      final st = _IdlConst.from(ints[i]);
      dataInts.elementAt(i).value = st;
    }

    fValueInts.ref.data = dataInts.cast<Void>();
    fValueInts.ref.length = lengthInts;

    try {
      final resultError = IdlLanguageServer._methodSyncAddInts(_instance, fValueInts);
      if (resultError != 0) _handleError(resultError);
    } finally {
      fValueInts.release();
    }
  }

  void addFloats(List<IdlConst> floats) {
    final fValueFloats = allocate<ArrayType>();
    final lengthFloats = floats.length;
    final dataFloats = allocate<Pointer<_IdlConst>>(count: lengthFloats);
    for (var i = 0; i < lengthFloats; i += 1) {
      final st = _IdlConst.from(floats[i]);
      dataFloats.elementAt(i).value = st;
    }

    fValueFloats.ref.data = dataFloats.cast<Void>();
    fValueFloats.ref.length = lengthFloats;

    try {
      final resultError = IdlLanguageServer._methodSyncAddFloats(_instance, fValueFloats);
      if (resultError != 0) _handleError(resultError);
    } finally {
      fValueFloats.release();
    }
  }

  void finalize() {
    final resultError = IdlLanguageServer._methodSyncFinalize(_instance);
    if (resultError != 0) _handleError(resultError);
  }

  String get asString {
    final fValue = allocate<Pointer<NativeString>>();
    try {
      final resultError = IdlLanguageServer._methodGetAsString(_instance, fValue);
      if (resultError != 0) _handleError(resultError);
      final result = fValue.value.asString();
      final releaseResultError = IdlLanguageServer._methodReleaseAsString(_instance, fValue.value);
      if (releaseResultError != 0) _handleError(releaseResultError);
      return result;
    } finally {
      free(fValue);
    }
  }
}

abstract class _IdlLanguageServer extends Struct {}

extension _IdlInterfacePointer on Pointer<_IdlInterface> {
  _releaseListFields(data, length) {
    for (var i = 0; i < length; i += 1) {
      data.cast<_IdlInterfaceField>().elementAt(i).release();
    }
    free(data);
  }

  release() {
    ref.name.release();
    _releaseListFields(ref.fields.ref.data, ref.fields.ref.length);
    ref.fields.release();
  }
}

extension _IdlInterfaceFieldPointer on Pointer<_IdlInterfaceField> {
  _releaseListInterfaceAttributes(data, length) {
    free(data);
  }

  release() {
    ref.fieldName.release();
    ref.idlType.release();
    _releaseListInterfaceAttributes(ref.interfaceAttributes.ref.data, ref.interfaceAttributes.ref.length);
    ref.interfaceAttributes.release();
  }
}

extension _IdlStructPointer on Pointer<_IdlStruct> {
  _releaseListFields(data, length) {
    for (var i = 0; i < length; i += 1) {
      data.cast<_IdlStructField>().elementAt(i).release();
    }
    free(data);
  }

  release() {
    ref.name.release();
    _releaseListFields(ref.fields.ref.data, ref.fields.ref.length);
    ref.fields.release();
  }
}

extension _IdlStructFieldPointer on Pointer<_IdlStructField> {
  release() {
    ref.fieldName.release();
    ref.idlType.release();
  }
}

extension _IdlEnumPointer on Pointer<_IdlEnum> {
  _releaseListFieldNames(data, length) {
    for (var i = 0; i < length; i += 1) {
      data.cast<_IdlEnumField>().elementAt(i).release();
    }
    free(data);
  }

  release() {
    ref.name.release();
    _releaseListFieldNames(ref.fieldNames.ref.data, ref.fieldNames.ref.length);
    ref.fieldNames.release();
  }
}

extension _IdlEnumFieldPointer on Pointer<_IdlEnumField> {
  release() {
    ref.fieldName.release();
  }
}

extension _IdlConstPointer on Pointer<_IdlConst> {
  _releaseListFields(data, length) {
    for (var i = 0; i < length; i += 1) {
      data.cast<_IdlConstField>().elementAt(i).release();
    }
    free(data);
  }

  release() {
    ref.name.release();
    _releaseListFields(ref.fields.ref.data, ref.fields.ref.length);
    ref.fields.release();
  }
}

extension _IdlConstFieldPointer on Pointer<_IdlConstField> {
  release() {
    ref.fieldName.release();
    ref.fieldValue.release();
  }
}

extension _IdlTypePointer on Pointer<_IdlType> {
  release() {
    final typeNameObject = ref.object.ref.name.asString();
    if (typeNameObject == 'int' || typeNameObject == 'none' || typeNameObject == 'bool') {
      (ref.object.ref.ptrData as Pointer<Int64>).release();
    } else if (typeNameObject == 'string') {
      (ref.object.ref.ptrData as Pointer<NativeString>).release();
    } else if (typeNameObject == 'IdlType') {
      (ref.object.ref.ptrData as Pointer<_IdlType>).release();
    } else if (typeNameObject == 'IdlMethod') {
      (ref.object.ref.ptrData as Pointer<_IdlMethod>).release();
    } else if (typeNameObject == 'IdlTypeList') {
      (ref.object.ref.ptrData as Pointer<_IdlTypeList>).release();
    } else if (typeNameObject == 'IdlTypeTuple') {
      (ref.object.ref.ptrData as Pointer<_IdlTypeTuple>).release();
    }
    ref.object.release();
  }
}

extension _IdlTypeListPointer on Pointer<_IdlTypeList> {
  _releaseListTypeList(data, length) {
    for (var i = 0; i < length; i += 1) {
      data.cast<_IdlType>().elementAt(i).release();
    }
    free(data);
  }

  release() {
    _releaseListTypeList(ref.typeList.ref.data, ref.typeList.ref.length);
    ref.typeList.release();
  }
}

extension _IdlTypeTuplePointer on Pointer<_IdlTypeTuple> {
  _releaseListTypeList(data, length) {
    for (var i = 0; i < length; i += 1) {
      data.cast<_IdlTupleEntry>().elementAt(i).release();
    }
    free(data);
  }

  release() {
    _releaseListTypeList(ref.typeList.ref.data, ref.typeList.ref.length);
    ref.typeList.release();
  }
}

extension _IdlTupleEntryPointer on Pointer<_IdlTupleEntry> {
  release() {
    ref.name.release();
    ref.idlType.release();
  }
}

extension _IdlTypeArrayPointer on Pointer<_IdlTypeArray> {
  release() {
    ref.arrayType.release();
  }
}

extension _IdlMethodPointer on Pointer<_IdlMethod> {
  release() {
    ref.args.release();
    ref.returnType.release();
  }
}

typedef _FactoryIdlLanguageServerCreateInstanceNative = Int64 Function(Pointer<Pointer<_IdlLanguageServer>>);
typedef _FactoryIdlLanguageServerCreateInstanceFunc = int Function(Pointer<Pointer<_IdlLanguageServer>>);
typedef _ReleaseIdlLanguageServerNative = Int64 Function(Pointer<_IdlLanguageServer>);
typedef _ReleaseIdlLanguageServerFunc = int Function(Pointer<_IdlLanguageServer>);
typedef _MethodIdlLanguageServerSyncAddEnumsNative = Int64 Function(Pointer<_IdlLanguageServer>, Pointer<ArrayType>);
typedef _MethodIdlLanguageServerSyncAddEnumsFunc = int Function(Pointer<_IdlLanguageServer>, Pointer<ArrayType>);
typedef _MethodIdlLanguageServerSyncAddStructsNative = Int64 Function(Pointer<_IdlLanguageServer>, Pointer<ArrayType>);
typedef _MethodIdlLanguageServerSyncAddStructsFunc = int Function(Pointer<_IdlLanguageServer>, Pointer<ArrayType>);
typedef _MethodIdlLanguageServerSyncAddInterfacesNative = Int64 Function(Pointer<_IdlLanguageServer>, Pointer<ArrayType>);
typedef _MethodIdlLanguageServerSyncAddInterfacesFunc = int Function(Pointer<_IdlLanguageServer>, Pointer<ArrayType>);
typedef _MethodIdlLanguageServerSyncAddStringsNative = Int64 Function(Pointer<_IdlLanguageServer>, Pointer<ArrayType>);
typedef _MethodIdlLanguageServerSyncAddStringsFunc = int Function(Pointer<_IdlLanguageServer>, Pointer<ArrayType>);
typedef _MethodIdlLanguageServerSyncAddIntsNative = Int64 Function(Pointer<_IdlLanguageServer>, Pointer<ArrayType>);
typedef _MethodIdlLanguageServerSyncAddIntsFunc = int Function(Pointer<_IdlLanguageServer>, Pointer<ArrayType>);
typedef _MethodIdlLanguageServerSyncAddFloatsNative = Int64 Function(Pointer<_IdlLanguageServer>, Pointer<ArrayType>);
typedef _MethodIdlLanguageServerSyncAddFloatsFunc = int Function(Pointer<_IdlLanguageServer>, Pointer<ArrayType>);
typedef _MethodIdlLanguageServerSyncFinalizeNative = Int64 Function(Pointer<_IdlLanguageServer>);
typedef _MethodIdlLanguageServerSyncFinalizeFunc = int Function(Pointer<_IdlLanguageServer>);
typedef _MethodIdlLanguageServerGetAsStringNative = Int64 Function(Pointer<_IdlLanguageServer>, Pointer<Pointer<NativeString>>);
typedef _MethodIdlLanguageServerGetAsStringFunc = int Function(Pointer<_IdlLanguageServer>, Pointer<Pointer<NativeString>>);
typedef _MethodIdlLanguageServerReleaseAsStringNative = Int64 Function(Pointer<_IdlLanguageServer>, Pointer<NativeString>);
typedef _MethodIdlLanguageServerReleaseAsStringFunc = int Function(Pointer<_IdlLanguageServer>, Pointer<NativeString>);