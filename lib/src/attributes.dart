abstract class AttributeFieldCompiler<Result, Field> {
  Result fromObjectField(Field fieldObject);
}