import 'dart:collection';
import 'dart:io';
import 'dart:ffi';
import 'package:test/test.dart';
import 'package:dart_idl_generator/idl_native.dart';

//import '../src/parser.dart';
//import '../src/dart_gn_client.dart';
//import '../src/rust_gn.dart';

void main(List<String> arguments) async {
  //final file = File(Directory.current.path + '/bin/basic_interface.idl');
  final file = File(Directory.current.path + '/src/interface.idl');
  final idlFile = await IdlFile.parseFile(file);
  //final idlObject = IdlObject.compile(idlFile);

  //final idlRust = IdlRust.generate(idlObject);
  //print(idlRust);

//  final idlDart = IdlDart.generate(idlObject);

  //final g_file = File(Directory.current.path + '/../generated_dart/gen.dart');
  final g_file = File(Directory.current.path + '/src/interface.dart');
  //g_file.writeAsStringSync(idlDart.toString());

  return;

  // test('Generate dart code, basic enumeration.', () async {
  //   final file = File(Directory.current.path + '/bin/basic_enum.idl');
  //   final idlFile = await IdlFile.parseFile(file);
  //   final idlObject = IdlObject.compile(idlFile);
  //   final idlDart = IdlDart.generate(idlObject);
  // });

  // test('Basic idl file test, route.', () async {
  //   final file = File(Directory.current.path + '/bin/route.idl');
  //   final idlFile = await IdlFile.parseFile(file);

  //   //await file.writeAsStringSync(idlFile.toString(), flush: true);

  //   final libraryName = 'route_reeee';
  //   final imports = <String>{'primes', 'core'};

  //   final createRouteGuideInterface = () {
  //     final fields = <IdlFileInterfaceObject>[];

  //     final featuresAtt = IdlFileAttributes([
  //       [IdlFileAttribute('deprecated'), 'Not used anymore.'],
  //       [IdlFileAttribute('get')],
  //     ]);
  //     final featuresType = IdlFileType(IdlFileNativeTypeName('float'));
  //     fields.add(IdlFileInterfaceObject('feature', featuresAtt, featuresType));

  //     final recordAtt = IdlFileAttributes([
  //       [
  //         IdlFileAttribute('set'),
  //         IdlFileAttribute('get'),
  //       ]
  //     ]);
  //     final recordType = IdlFileType(IdlFileNativeTypeName('int'));
  //     fields.add(IdlFileInterfaceObject('record', recordAtt, recordType));

  //     final pointsAtt = IdlFileAttributes([
  //       [IdlFileAttribute('stream')],
  //     ]);
  //     final pointsType = IdlFileType(IdlFileTypeName('Point'));
  //     fields.add(IdlFileInterfaceObject('points', pointsAtt, pointsType));

  //     final errorAtt = IdlFileAttributes([
  //       [IdlFileAttribute('stream')],
  //     ]);
  //     final errorType = IdlFileType(IdlFileTypeName('RouteError'));
  //     fields.add(IdlFileInterfaceObject('error', errorAtt, errorType));

  //     return IdlFileInterface('RouteGuide', fields);
  //   };

  //   final createRouteSummaryInterface = () {
  //     final fields = <IdlFileInterfaceObject>[];

  //     final distanceAtt = IdlFileAttributes([
  //       [IdlFileAttribute('get'), IdlFileAttribute('set')],
  //     ]);
  //     final distanceType = IdlFileType(IdlFileTypeTuple(
  //       [IdlFileNativeTypeName('float'), IdlFileNativeTypeName('float')],
  //     ));
  //     fields.add(IdlFileInterfaceObject('distance', distanceAtt, distanceType));

  //     final featureCountAtt = IdlFileAttributes([
  //       [IdlFileAttribute('get')],
  //     ]);
  //     final featureCountType = IdlFileType(IdlFileNativeTypeName('int'));
  //     fields.add(IdlFileInterfaceObject(
  //         'featureCount', featureCountAtt, featureCountType));

  //     final messageAtt = IdlFileAttributes([
  //       [IdlFileAttribute('set')],
  //     ]);
  //     final messageType = IdlFileType(IdlFileTypeTuple([]));
  //     fields.add(IdlFileInterfaceObject('message', messageAtt, messageType));

  //     final elapsedTimeAtt = IdlFileAttributes([
  //       [IdlFileAttribute('get')],
  //     ]);
  //     final elapsedTimeType =
  //         IdlFileType(IdlFileTypeList([IdlFileTypeTuple([]), IdlFileNativeTypeName('int')]));
  //     fields.add(
  //         IdlFileInterfaceObject('elapsedTime', elapsedTimeAtt, elapsedTimeType));

  //     final guideAtt = IdlFileAttributes([
  //       [IdlFileAttribute('get')],
  //     ]);
  //     final guideType = IdlFileType(
  //         IdlFileTypeList([IdlFileNativeTypeName('int'), IdlFileNativeTypeName('bool')]));
  //     fields.add(IdlFileInterfaceObject('guide', guideAtt, guideType));

  //     return IdlFileInterface('RouteSummary', fields);
  //   };

  //   final interfaces = HashMap<String, IdlFileInterface>();
  //   interfaces['RouteGuide'] = createRouteGuideInterface();
  //   interfaces['RouteSummary'] = createRouteSummaryInterface();

  //   final enums = HashMap<String, IdlFileEnum>();

  //   final createRecordTypeEnum = () {
  //     final fields = <IdlFileEnumObject>[];

  //     fields.add(IdlFileEnumObject(
  //         'location',
  //         IdlFileAttributes([
  //           [IdlFileAttribute('deprecated')],
  //         ])));
  //     fields.add(IdlFileEnumObject('distance', null));

  //     return IdlFileEnum('RecordType', fields);
  //   };

  //   final createRouteErrorEnum = () {
  //     final fields = <IdlFileEnumObject>[];

  //     fields.add(IdlFileEnumObject('wrongType', null));

  //     return IdlFileEnum('RouteError', fields);
  //   };

  //   enums['RecordType'] = createRecordTypeEnum();
  //   enums['RouteError'] = createRouteErrorEnum();

  //   final structs = HashMap<String, IdlFileStruct>();

  //   final createPointStruct = () {
  //     final fields = <IdlFileStructObject>[];

  //     final latiduteType = IdlFileType(IdlFileNativeTypeName('int'));
  //     final longitudeType = IdlFileType(IdlFileNativeTypeName('int'));

  //     fields.add(IdlFileStructObject('latidute', latiduteType, null));
  //     fields.add(IdlFileStructObject('longitude', longitudeType, null));

  //     return IdlFileStruct('Point', fields);
  //   };

  //   structs['Point'] = createPointStruct();

  //   expect(idlFile.libraryName, equals(libraryName));
  //   expect(idlFile.imports, equals(imports));
  //   expect(idlFile.interfaces, equals(interfaces));
  //   expect(idlFile.enums, equals(enums));
  //   expect(idlFile.structs, equals(structs));
  // });
}
