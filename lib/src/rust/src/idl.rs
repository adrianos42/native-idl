use crate::idl_enums::*;

pub trait IdlLanguageServer {
    fn add_enums(&mut self, enums: Vec<IdlEnum>);
    fn add_structs(&mut self, structs: Vec<IdlStruct>);
    fn add_interfaces(&mut self, interfaces: Vec<IdlInterface>);
    fn add_strings(&mut self, strings: Vec<IdlConst>);
    fn add_ints(&mut self, ints: Vec<IdlConst>);
    fn add_floats(&mut self, floats: Vec<IdlConst>);
    fn finalize(&mut self);
    fn get_as_string(&self) -> String;
}

pub struct IdlInterface {
    pub name: String,
    pub fields: Vec<IdlInterfaceField>,
}

pub struct IdlInterfaceField {
    pub field_name: String,
    pub idl_type: IdlType,
    pub interface_attributes: Vec<IdlInterfaceAttributes>,
}

pub struct IdlStruct {
    pub name: String,
    pub fields: Vec<IdlStructField>,
}

pub struct IdlStructField {
    pub field_name: String,
    pub idl_type: IdlType,
}

pub struct IdlEnum {
    pub name: String,
    pub field_names: Vec<IdlEnumField>,
}

pub struct IdlEnumField {
    pub field_name: String,
}

pub struct IdlConst {
    pub name: String,
    pub fields: Vec<IdlConstField>,
}

pub struct IdlConstField {
    pub field_name: String,
    pub field_value: String,
}

pub struct IdlType {
    pub object: IdlTypeObject,
    pub object_type: IdlTypes,
}

pub enum IdlTypeObject {
    None,
    String(Box<String>),
    IdlType(Box<IdlType>),
    IdlMethod(Box<IdlMethod>),
    IdlTypeList(Box<IdlTypeList>),
    IdlTypeTuple(Box<IdlTypeTuple>),
}

pub struct IdlTypeList {
    pub type_list: Vec<IdlType>,
}

pub struct IdlTypeTuple {
    pub type_list: Vec<IdlTupleEntry>,
}

pub struct IdlTupleEntry {
    pub name: String,
    pub idl_type: IdlType,
}

pub struct IdlTypeArray {
    pub array_type: IdlType,
}

pub struct IdlMethod {
    pub args: IdlType,
    pub return_type: IdlType,
}
