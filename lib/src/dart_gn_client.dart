import 'package:code_builder/code_builder.dart';
import 'package:dart_style/dart_style.dart';

import 'lexer.dart';

String toSnakeCase(String value) => value.replaceAllMapped(
    RegExp(r'(^[A-Z])|([A-Z]$)|([A-Z])'),
    (match) => match[1] != null
        ? match[1].toLowerCase()
        : match[2] != null
            ? match[2].toLowerCase()
            : '_' + match[3].toLowerCase());

String toPascalCase(String value) => value.replaceAllMapped(
    RegExp(r'(^[a-z])|(?:_(a-z))'),
    (match) =>
        match[1] != null ? match[1].toUpperCase() : match[2].toLowerCase());

class IdlDart {
  IdlDart();

  void _addEnums(IdlObject idlObject) {
    _enums.addAll(idlObject.enums.map((enumValue) {
      var enumStr = enumValue.fieldNames.fold('enum ${enumValue.name} {',
              (p, value) => p + value.fieldName + ',') +
          '}\n\n';

      return Code(enumStr);
    }));
  }

  void _addStrings(IdlObject idlObject) {
    _strings.addAll(idlObject.strings.map((stringValue) {
      return Class((b) => b
        ..name = stringValue.name
        ..fields.addAll(stringValue.fields.map((field) => Field((b) => b
          ..name = field.fieldName
          ..type = refer('String')
          ..static = true
          ..modifier = FieldModifier.constant
          ..assignment = Code('\'${field.fieldValue}\'')))));
    }));
  }

  void _addInts(IdlObject idlObject) {
    _ints.addAll(idlObject.ints.map((intValue) {
      return Class((b) => b
        ..name = intValue.name
        ..fields.addAll(intValue.fields.map((field) => Field((b) => b
          ..name = field.fieldName
          ..type = refer('int')
          ..static = true
          ..modifier = FieldModifier.constant
          ..assignment = Code('${field.fieldValue}')))));
    }));
  }

  void _addFloats(IdlObject idlObject) {
    _floats.addAll(idlObject.floats.map((floatValue) {
      return Class((b) => b
        ..name = floatValue.name
        ..fields.addAll(floatValue.fields.map((field) => Field((b) => b
          ..name = field.fieldName
          ..type = refer('double')
          ..static = true
          ..modifier = FieldModifier.constant
          ..assignment = Code('${field.fieldValue}')))));
    }));
  }

  // Factory fields for dart struct.
  String _ffiToTypeForStructField(IdlStructField field, String resultName) {
    // This is knowing that each type is already well represented as a ffi type.
    // For example, bool is an signed 64-bit integer, so the type to be converted
    // is either a Uint64 or Uint64Pointer.
    switch (field.idlType.objectType) {
      case IdlTypes.nBool:
        return 'final $resultName = value.ref.${field.fieldName} == 0 ? false : true;';
      case IdlTypes.nFloat:
      case IdlTypes.nInt:
        return 'final $resultName = value.ref.${field.fieldName};';
      case IdlTypes.nString:
        return 'final $resultName = value.ref.${field.fieldName}.asString();';
      case IdlTypes.nBytes:
        return 'final $resultName = value.ref.${field.fieldName}.asUint8List();';
      case IdlTypes.nEnum:
        return 'final $resultName = ${field.idlType.object as String}.values[value.ref.${field.fieldName}];';
      case IdlTypes.nStruct:
        return 'final $resultName = ${field.idlType.object as String}._from(value.ref.${field.fieldName});';
      case IdlTypes.nTypeList:
        final typeList = (field.idlType.object as IdlTypeList).typeList;
        var body = '''
          final typeName = value.ref.${field.fieldName}.ref.name.asString();
          final typeValue = value.ref.${field.fieldName}.ref.ptrData;
          dynamic $resultName;
        ''';
        body += typeList.fold('', (p, type) {
          if (p.isNotEmpty) p += 'else ';
          switch (type.objectType) {
            case IdlTypes.nNone:
              return p +
                  '''
              if (typeName == \'none\') { 
                if (typeValue.cast<Int64>().value != 0) {
                  throw ArgumentError(\'[${field.fieldName}] is not zero.\');
                }
                $resultName = null;
              }
            ''';
            case IdlTypes.nBool:
              return p +
                  'if (typeName == \'bool\') { $resultName = typeValue.cast<Int64>().value == 0 ? false : true; }';
            case IdlTypes.nInt:
              return p +
                  'if (typeName == \'int\') { $resultName = typeValue.cast<Int64>().value; }';
            case IdlTypes.nFloat:
              return p +
                  'if (typeName == \'float\') { $resultName = typeValue.cast<Double>().value; }';
            case IdlTypes.nBytes:
              return p +
                  'if (typeName == \'bytes\') { $resultName = typeValue.cast<Bytes>().asUint8List(); }';
            case IdlTypes.nString:
              return p +
                  'if (typeName == \'string\') { $resultName = typeValue.cast<NativeString>().asString(); }';
            case IdlTypes.nEnum:
              return p +
                  'if (typeName == \'${type.object as String}\') { $resultName = ${type.object as String}.values[typeValue.cast<Int64>().value]; }';
            case IdlTypes.nStruct:
              return p +
                  'if (typeName == \'${type.object as String}\') { $resultName =  ${type.object as String}._from(typeValue.cast<_${type.object as String}>()); }';
            default:
              throw Exception(
                  'Type cannot be generated for type list in dart: []');
          }
        });
        body += '''
          else { throw Exception(\'Invalid type in [${field.fieldName}].\'); }
        ''';
        return body;
      case IdlTypes.nArray:
        var body = ''' 
          final data = value.ref.${field.fieldName}.ref.data;
          final length = value.ref.${field.fieldName}.ref.length;
          ${_getDataArray(field.idlType.object as IdlType, resultName)}
        ''';
        return body;
      default:
        throw Exception(
            'Invalid type in struct for dart code: [${field.toString()}].');
    }
  }

  // Struct constructor.
  String _getffiTypeConverstionBodyForStruct(IdlStruct structValue) {
    final fields = structValue.fields;
    var args = '';
    var body = '';

    fields.forEach((field) {
      final fieldNameValue = toPascalCase(field.fieldName);
      final resultName = 'sValue$fieldNameValue';

      body += _ffiToTypeForStructField(field, resultName);

      args += '${field.fieldName}:$resultName,';
    });

    return body + 'return ${structValue.name}($args);';
  }

  // Factory fields for ffi struct.
  String _typeToffiForStructField(IdlStructField field) {
    // This is knowing that each type is already well represented as a ffi type.
    // For example, bool is an signed 64-bit integer, so the type to be converted
    // is either a Int64 or Int64Pointer.
    final fieldNameValue = toPascalCase(field.fieldName);
    final resultName = 'fValue$fieldNameValue';
    final valueName = 'value.${field.fieldName}';

    switch (field.idlType.objectType) {
      case IdlTypes.nBool:
        return 'final $resultName = $valueName ? 1 : 0;';
      case IdlTypes.nFloat:
      case IdlTypes.nInt:
        return 'final $resultName = $valueName;';
      case IdlTypes.nString:
        return 'final $resultName = $valueName.asNativeString();';
      case IdlTypes.nBytes:
        return 'final $resultName = $valueName.asNativeBytes();';
      case IdlTypes.nEnum:
        return 'final $resultName = ${field.idlType.object as String}.values.indexOf($valueName);';
      case IdlTypes.nStruct:
        return 'final $resultName = _${field.idlType.object as String}.from($valueName);';
      case IdlTypes.nTypeList:
        final typeList = (field.idlType.object as IdlTypeList).typeList;
        return _createTypeListToffi(typeList,
            resultName: resultName, valueName: valueName);
      case IdlTypes.nArray:
        return '''
          final $resultName = allocate<ArrayType>();
          ${_setArrayData(field.idlType.object as IdlType, valueName, fieldNameValue)}
          $resultName.ref.data = data$fieldNameValue.cast<Void>();
          $resultName.ref.length = length$fieldNameValue;
        ''';
      default:
        throw Exception(
            'Invalid type in struct for dart code: [${field.toString()}].');
    }
  }

  String _setArrayData(IdlType arrayType,
      [String valueName = 'value', String fieldNameValue = '']) {
    var body = 'final length$fieldNameValue = $valueName.length;';

    switch (arrayType.objectType) {
      case IdlTypes.nInt:
        body += '''
          final data$fieldNameValue = allocate<Int64>(count: length$fieldNameValue);
          final listSource$fieldNameValue = data$fieldNameValue.asTypedList(length$fieldNameValue);
          listSource$fieldNameValue.setAll(0, $valueName);
        ''';
        break;
      case IdlTypes.nFloat:
        body += '''
          final data$fieldNameValue = allocate<Double>(count: length$fieldNameValue);
          final listSource$fieldNameValue = data$fieldNameValue.asTypedList(length$fieldNameValue);
          listSource$fieldNameValue.setAll(0, $valueName);
        ''';
        break;
      case IdlTypes.nBool:
        body += '''
          final data$fieldNameValue = allocate<Int64>(count: length$fieldNameValue);
          final listSource$fieldNameValue = data$fieldNameValue.asTypedList(length$fieldNameValue);
          listSource$fieldNameValue.setAll(0, $valueName.map((value) => value ? 1 : 0));
        ''';
        break;
      case IdlTypes.nBytes:
        body += '''
          final data$fieldNameValue = allocate<Bytes>(count: length$fieldNameValue);
          for (var i = 0; i < length$fieldNameValue; i += 1) {
            final bytes = $valueName[i].asNativeBytes();
            data$fieldNameValue.elementAt(i).ref.length = bytes.ref.length;
            data$fieldNameValue.elementAt(i).ref.data = bytes.ref.data;
          }
        ''';
        break;
      case IdlTypes.nString:
        body += '''
          final data$fieldNameValue = allocate<NativeString>(count: length$fieldNameValue);
          for (var i = 0; i < length$fieldNameValue; i += 1) {
            final nativeString = $valueName[i].asNativeString();
            data$fieldNameValue.elementAt(i).ref.length = nativeString.ref.length;
            data$fieldNameValue.elementAt(i).ref.data = nativeString.ref.data;
          }
        ''';
        break;
      case IdlTypes.nStruct:
        body += '''
          final data$fieldNameValue = allocate<Pointer<_${arrayType.object as String}>>(count: length$fieldNameValue);
          for (var i = 0; i < length$fieldNameValue; i += 1) {
            final st = _${arrayType.object as String}.from($valueName[i]);
            data$fieldNameValue.elementAt(i).value = st;
          }
        ''';
        break;
      case IdlTypes.nEnum:
        body += '''
          final data$fieldNameValue = allocate<Int64>(count: length$fieldNameValue);
          final listSource$fieldNameValue = data$fieldNameValue.asTypedList(length$fieldNameValue);
          listSource$fieldNameValue.setAll(0, $valueName.map((value) => ${arrayType.object as String}.values.indexOf(value)));
        ''';
        break;
      case IdlTypes.nArray:
        final aType = arrayType.object as IdlType;
        body += '''
          final createList = (value) {
              final result = ArrayType();
              ${_setArrayData(aType)}
              result.length = length$fieldNameValue;
              result.data = data$fieldNameValue.cast<Void>();
              return result;
          };
          final data$fieldNameValue = allocate<${_getffiTypeArrayForInterface(arrayType)}>(count: length$fieldNameValue);
          for (var i = 0; i < length$fieldNameValue; i += 1) {
            final st = createList($valueName[i]);
            data$fieldNameValue.elementAt(i).ref.data = st.data;
            data$fieldNameValue.elementAt(i).ref.length = st.length;
          }
        ''';
        break;
      default:
        throw Exception('Invalid type for array in struct code: []');
    }

    return body;
  }

  // ffi struct constructor.
  String _getTypeffiConverstionBodyForStruct(IdlStruct structValue) {
    var body = 'final result=allocate<_${structValue.name}>();';

    return structValue.fields.fold(
            body,
            (p, field) =>
                p +
                '''
                ${_typeToffiForStructField(field)}
                result.ref.${field.fieldName}=fValue${toPascalCase(field.fieldName)};''') +
        'return result;';
  }

  String _getffiReleaseForStruct(IdlStruct structValue) {
    var body = '';

    structValue.fields
        .where((field) => field.idlType.objectType == IdlTypes.nArray)
        .forEach((field) {
      final arrayType = field.idlType.object as IdlType;
      body += ''' 
        _releaseList${toPascalCase(field.fieldName)}(data, length) {
              ${_releaseArrayDataForStruct(arrayType, field)}
        }
      ''';
    });

    body += 'release() {';

    structValue.fields.forEach((field) {
      if (field.idlType.objectType == IdlTypes.nArray) {}
      switch (field.idlType.objectType) {
        case IdlTypes.nTypeList:
          body += _createTypeListToffiRelease(
              (field.idlType.object as IdlTypeList).typeList,
              resultName: 'ref.${field.fieldName}',
              fieldName: field.fieldName);
          break;
        case IdlTypes.nArray:
          body += ''' 
        _releaseList${toPascalCase(field.fieldName)}(ref.${field.fieldName}.ref.data, ref.${field.fieldName}.ref.length);
        ref.${field.fieldName}.release();
      ''';
          break;
        case IdlTypes.nString:
        case IdlTypes.nBytes:
        case IdlTypes.nStruct:
          body += 'ref.${field.fieldName}.release();';
          break;
        default:
      }
    });

    body += '}';

    return body;
  }

  String _releaseArrayDataForStruct(IdlType arrayType, IdlStructField field) {
    switch (arrayType.objectType) {
      case IdlTypes.nInt:
      case IdlTypes.nFloat:
      case IdlTypes.nBool:
      case IdlTypes.nEnum:
        return 'free(data);';
      case IdlTypes.nString:
        return '''
          for (var i = 0; i < length; i += 1) {
            data.cast<NativeString>().elementAt(i).release();
          }
          free(data);
        ''';
      case IdlTypes.nBytes:
        return '''
          for (var i = 0; i < length; i += 1) {
            data.cast<Bytes>().elementAt(i).release();
          }
          free(data);
        ''';
      case IdlTypes.nStruct:
        return '''
          for (var i = 0; i < length; i += 1) {
            data.cast<_${arrayType.object as String}>().elementAt(i).release();
          }
          free(data);
        ''';
      case IdlTypes.nArray:
        return '''
          for (var i = 0; i < length; i += 1) {
            final listSource = data.elementAt(i).cast<ArrayType>().ref;
            _releaseList${toPascalCase(field.fieldName)}(listSource.data, listSource.length);
          }
          free(data);
        ''';
      default:
        throw Exception('Invalid type for array in get code:');
    }
  }

  void _addStructs(IdlObject idlObject) {
    _structs.addAll(idlObject.structs.expand((structValue) {
      final structs = <Class>[];

      structs.add(Class((b) => b
        ..name = '_${structValue.name}'
        ..extend = refer('Struct')
        ..methods.add(Method((b) => b
          ..static = true
          ..name = 'from'
          ..body = Code(_getTypeffiConverstionBodyForStruct(structValue))
          ..returns = refer('Pointer<_${structValue.name}>')
          ..requiredParameters.add(Parameter((b) => b
            ..name = 'value'
            ..type = refer(structValue.name)))))
        ..fields.addAll(structValue.fields.map((field) {
          final annts = <Expression>[];

          switch (field.idlType.objectType) {
            case IdlTypes.nBool:
            case IdlTypes.nEnum:
              annts.add(refer('Int64()'));
              break;
            case IdlTypes.nFloat:
              annts.add(refer('Double()'));
              break;
            case IdlTypes.nInt:
              annts.add(refer('Int64()'));
              break;
            default:
          }

          return Field((b) => b
            ..name = field.fieldName
            ..type = refer(_getDartTypeForInterface(field.idlType))
            ..annotations.addAll(annts));
        }))));

      _structsExtension.add(Code('''
      extension _${structValue.name}Pointer on Pointer<_${structValue.name}> {
          ${_getffiReleaseForStruct(structValue)}
        }
        '''));

      structs.add(Class(
        (b) => b
          ..name = structValue.name
          // The constructor used by dart code to create the data.
          ..constructors.add(Constructor(((b) => b
            ..initializers.addAll(structValue.fields.map((field) {
              return Code('_${field.fieldName}=${field.fieldName}');
            }))
            ..optionalParameters.addAll(structValue.fields.map((field) {
              return Parameter((b) => b
                ..named = true
                ..type = refer(_getTypeName(field.idlType))
                ..name = field.fieldName);
            })))))
          // To convert from the ffi type.
          ..constructors.add(Constructor((b) => b
            ..factory = true
            ..name = '_from'
            ..body = Code(_getffiTypeConverstionBodyForStruct(structValue))
            ..requiredParameters.add(Parameter((b) => b
              ..name = 'value'
              ..type = refer('Pointer<_${structValue.name}>')))))
          ..fields.addAll(
            structValue.fields.map((field) {
              final typeName = _getTypeName(field.idlType);
              return Field((b) => b
                ..name = '_${field.fieldName}'
                ..type = refer(typeName));
            }),
          )
          ..methods.addAll(structValue.fields.expand((field) {
            return [
              Method((b) => b
                ..name = field.fieldName
                ..lambda = true
                ..returns = refer('${_getTypeName(field.idlType)}')
                ..type = MethodType.getter
                ..body = Code('_${field.fieldName}')),
              Method((b) => b
                ..name = field.fieldName
                ..type = MethodType.setter
                ..requiredParameters.add(Parameter((b) => b
                  ..type = refer(_getTypeName(field.idlType))
                  ..name = 'value'))
                ..body =
                    Code('assert(value != null);_${field.fieldName}=value;'))
            ];
          })),
      ));

      return structs;
    }));
  }

  String _getTypeName(IdlType type) {
    switch (type.objectType) {
      case IdlTypes.nVoid:
        return 'void';
      case IdlTypes.nBool:
        return 'bool';
      case IdlTypes.nFloat:
        return 'double';
      case IdlTypes.nInt:
        return 'int';
      case IdlTypes.nString:
        return 'String';
      case IdlTypes.nBytes:
        return 'Uint8List';
      case IdlTypes.nTypeList:
        final typeList = (type.object as IdlTypeList).typeList;
        if (typeList.length == 1) {
          return _getTypeName(typeList.first);
        }
        return 'dynamic';
      case IdlTypes.nArray:
        return 'List<${_getTypeName(type.object)}>';
      case IdlTypes.nStruct:
      case IdlTypes.nInterface:
      case IdlTypes.nEnum:
        return type.object as String;
      default:
        throw Exception(
            'Invalid type in interface for dart code: [${type.toString()}].');
    }
  }

  // get
  String _ffiToTypeForInterfaceFieldGet(
      IdlInterface interfaceValue, IdlInterfaceField field) {
    final getValue = '''
        final resultError = ${interfaceValue.name}._methodGet${toPascalCase(field.fieldName)}(_instance, fValue);
        if (resultError != 0) _handleError(resultError);''';
    final releaseValue = '''
        final releaseResultError = ${interfaceValue.name}._methodRelease${toPascalCase(field.fieldName)}(_instance, fValue.value);
        if (releaseResultError != 0) _handleError(releaseResultError);''';

    var body = '';

    switch (field.idlType.objectType) {
      case IdlTypes.nBool:
        body += '''
          final fValue = allocate<Int64>();
          try { 
            $getValue
            final result = fValue.value == 0 ? false : true;
          ''';
        break;
      case IdlTypes.nFloat:
        body += '''
          final fValue = allocate<Double>();
          try {
            $getValue
            final result = fValue.value;
          ''';
        break;
      case IdlTypes.nInt:
        body += '''
          final fValue = allocate<Int64>();
          try { 
            $getValue
            final result = fValue.value;
          ''';
        break;
      case IdlTypes.nString:
        body += '''
          final fValue = allocate<Pointer<NativeString>>();
          try { 
            $getValue
            final result = fValue.value.asString();
            $releaseValue
          ''';
        break;
      case IdlTypes.nBytes:
        body += '''
          final fValue = allocate<Pointer<Bytes>>();
          try {
            $getValue
            final result = fValue.value.asUint8List();
            $releaseValue
        ''';
        break;
      case IdlTypes.nStruct:
        body += '''
        final fValue = allocate<Pointer<_${field.idlType.object as String}>>();
        try {
          $getValue
          final result = ${field.idlType.object as String}._from(fValue.value);
          $releaseValue 
        ''';
        break;
      case IdlTypes.nEnum:
        body += '''
        final fValue = allocate<Int64>();
        try {
          $getValue
          final result = ${field.idlType.object as String}.values[fValue.value];
        ''';
        break;
      case IdlTypes.nTypeList:
        final typeList = (field.idlType.object as IdlTypeList).typeList;
        assert(typeList.isNotEmpty);
        body += '''
          final fValue = allocate<Pointer<Type>>();
          try {
            $getValue
            final typeName = fValue.value.ref.name.asString();
            final typeValue = fValue.value.ref.ptrData;
            dynamic result;
          ''';
        body += typeList.fold('', (p, type) {
          if (p.isNotEmpty) p += 'else ';
          switch (type.objectType) {
            case IdlTypes.nNone:
              return p +
                  '''
              if (typeName == \'none\') { 
                if (typeValue.cast<Int64>().value != 0) {
                  throw ArgumentError(\'[${field.fieldName}] is not zero.\');
                }
                result = null;
              }
            ''';
            case IdlTypes.nBool:
              return p +
                  'if (typeName == \'bool\') { result = typeValue.cast<Int64>().value == 0 ? false : true; }';
            case IdlTypes.nInt:
              return p +
                  'if (typeName == \'int\') { result = typeValue.cast<Int64>().value; }';
            case IdlTypes.nFloat:
              return p +
                  'if (typeName == \'float\') { result = typeValue.cast<Double>().value; }';
            case IdlTypes.nBytes:
              return p +
                  'if (typeName == \'bytes\') { result = typeValue.cast<Bytes>().asUint8List(); }';
            case IdlTypes.nString:
              return p +
                  'if (typeName == \'string\') { result = typeValue.cast<NativeString>().asString(); }';
            case IdlTypes.nEnum:
              return p +
                  'if (typeName == \'${type.object as String}\') { result = ${type.object as String}.values[typeValue.cast<Int64>().value]; }';
            case IdlTypes.nStruct:
              return p +
                  'if (typeName == \'${type.object as String}\') { result =  ${type.object as String}._from(typeValue.cast<_${type.object as String}>()); }';
            default:
              throw Exception(
                  'Type cannot be generated for type list in dart: [${interfaceValue.name}]');
          }
        });
        body += '''
          else { throw Exception(\'Invalid type in [${interfaceValue.name}.${field.fieldName}].\'); }
          $releaseValue 
        ''';
        break;
      case IdlTypes.nArray:
        body += '''
          final fValue = allocate<Pointer<ArrayType>>();
          try {
            $getValue
            final data = fValue.value.ref.data;
            final length = fValue.value.ref.length;
        ''';
        body += '${_getDataArray(field.idlType.object as IdlType)}';
        body += releaseValue;
        break;
      default:
        throw Exception(
            'Invalid type in interface for dart ffi get code: ${field.toString()}.');
    }

    return body +
        ''' return result; } 
    finally {
      free(fValue);
    } ''';
  }

  String _getDataArray(IdlType arrayType, [resultName = 'result']) {
    switch (arrayType.objectType) {
      case IdlTypes.nInt:
        return 'final $resultName = data.cast<Int64>().asTypedList(length).toList();';
      case IdlTypes.nFloat:
        return 'final $resultName = data.cast<Double>().asTypedList(length).toList();';
      case IdlTypes.nBool:
        return '''
          final listSource = data.cast<Int64>().asTypedList(length);
          final $resultName = listSource.map((value) => value == 0 ? false : true).toList();
        ''';
      case IdlTypes.nBytes:
        return '''
          final listSource = data.cast<Bytes>();
          final $resultName = <Uint8List>[];
          for (var i = 0; i < length; i += 1) {
            $resultName.add(listSource.elementAt(i).asUint8List());
          }
        ''';
      case IdlTypes.nString:
        return '''
        final listSource = data.cast<NativeString>();
        final $resultName = <String>[];
        for (var i = 0; i < length; i += 1) {
          $resultName.add(listSource.elementAt(i).asString());
        }
      ''';
      case IdlTypes.nStruct:
        return '''
          final listSource = data.cast<_${arrayType.object as String}>();
          final $resultName = <${arrayType.object as String}>[];
          for (var i = 0; i < length; i += 1) {
            $resultName.add(${arrayType.object as String}._from(listSource.elementAt(i)));
          }
        ''';
      case IdlTypes.nEnum:
        return '''
          final listSource = data.cast<Int64>().asTypedList(length);
          final $resultName = listSource.map((value) => ${arrayType.object as String}.values[value]).toList();
        ''';
      case IdlTypes.nArray:
        final aType = arrayType.object as IdlType;
        return '''
          final createList = (data, length) {
              ${_getDataArray(aType, resultName)}
              return $resultName;
          };
          final listSource = data.cast<ArrayType>();
          final $resultName = <${_getTypeName(arrayType)}>[];
          for (var i = 0; i < length; i += 1) {
            final vData = listSource.elementAt(i).ref.data;
            final vLength = listSource.elementAt(i).ref.length;
            final value = createList(vData, vLength);
            $resultName.add(value);
          }
        ''';
      default:
        throw Exception('Invalid type for array in get code:');
    }
  }

  // async
  String _getAsyncForInterfaceField(
      IdlInterface interfaceValue, IdlInterfaceField field) {
    final value =
        (field.idlType.object as IdlMethod).returnType.objectType == IdlTypes.nVoid
            ? ''
            : ',value';
    var body = '''
      try {
        _async${toPascalCase(field.fieldName)}[handle.cast<Int64>()](asyncHandle.cast<Int64>()$value);
      } catch (_) {
        return AbiInternalError.callbackException;
      }
    ''';

    return body + 'return AbiInternalError.none;';
  }

  // stream
  String _getStreamForInterfaceField(
      IdlInterface interfaceValue, IdlInterfaceField field) {
    var body = ''' 
      try {
        _stream${toPascalCase(field.fieldName)}[handle.cast<Int64>()](value);
      } catch (_) {
        return AbiInternalError.callbackException;
      }
    ''';

    return body + 'return AbiInternalError.none;';
  }

  String _getReleaseMethodForInterface(IdlInterface interfaceValue) {
    var finallyBlock = '';

    var body = interfaceValue.fields
        .where((field) =>
            field.interfaceAttributes.contains(IdlInterfaceAttributes.mStream))
        .fold('', (p, field) {
      finallyBlock += 'free(_streamHandle${toPascalCase(field.fieldName)});';
      return p +
          '_stream${toPascalCase(field.fieldName)}.remove(_streamHandle${toPascalCase(field.fieldName)});';
    });

    body = interfaceValue.fields
        .where((field) =>
            field.interfaceAttributes.contains(IdlInterfaceAttributes.mAsync))
        .fold(body, (p, field) {
      finallyBlock += 'free(_asyncHandle${toPascalCase(field.fieldName)});';
      return p +
          '_async${toPascalCase(field.fieldName)}.remove(_asyncHandle${toPascalCase(field.fieldName)});';
    });

    body = '''
      final resultError = ${interfaceValue.name}._releaseInstance(_instance);
      if (resultError != 0) _handleReleaseError(resultError);
      $body
    ''';

    if (finallyBlock.isNotEmpty) {
      return '''
        try { $body }
        finally { $finallyBlock }
      ''';
    }

    return body;
  }

  String _setStreamsForInterface(IdlInterface interfaceValue) {
    return interfaceValue.fields
        .where((field) =>
            field.interfaceAttributes.contains(IdlInterfaceAttributes.mStream))
        .fold('', (p, field) {
      final errorOnException = 'AbiInternalError.callbackException';

      var body = '';
      final releaseValue = '''
        final releaseResultError = ${interfaceValue.name}._methodRelease${toPascalCase(field.fieldName)}(_instance, value);
        if (releaseResultError != 0) _handleError(releaseResultError);
      ''';

      switch (field.idlType.objectType) {
        case IdlTypes.nBool:
          body += 'final result = value == 0 ? false : true;';
          break;
        case IdlTypes.nFloat:
          body += 'final result = value;';
          break;
        case IdlTypes.nInt:
          body += 'final result = value;';
          break;
        case IdlTypes.nString:
          body += ''' 
            final result = value.asString();
            $releaseValue
          ''';
          break;
        case IdlTypes.nBytes:
          body += ''' 
            final result = value.asUint8List();
            $releaseValue
          ''';
          break;
        case IdlTypes.nStruct:
          body += ''' 
            final result = ${field.idlType.object as String}._from(value);
            $releaseValue 
          ''';
          break;
        case IdlTypes.nEnum:
          body +=
              'final result = ${field.idlType.object as String}.values[value];';
          break;
        case IdlTypes.nTypeList:
          final typeList = (field.idlType.object as IdlTypeList).typeList;
          assert(typeList.isNotEmpty);
          body += ''' 
            final typeName = value.ref.name.asString();
            final typeValue = value.ref.ptrData;
            dynamic result;
          ''';
          body += typeList.fold('', (p, type) {
            if (p.isNotEmpty) p += 'else ';
            switch (type.objectType) {
              case IdlTypes.nNone:
                return p +
                    '''
              if (typeName == \'none\') { 
                if (typeValue.cast<Int64>().value != 0) {
                  throw ArgumentError(\'[${field.fieldName}] is not zero.\');
                }
                result = null;
              }
            ''';
              case IdlTypes.nBool:
                return p +
                    'if (typeName == \'bool\') { result = typeValue.cast<Int64>().value == 0 ? false : true; }';
              case IdlTypes.nInt:
                return p +
                    'if (typeName == \'int\') { result = typeValue.cast<Int64>().value; }';
              case IdlTypes.nFloat:
                return p +
                    'if (typeName == \'float\') { result = typeValue.cast<Double>().value; }';
              case IdlTypes.nBytes:
                return p +
                    'if (typeName == \'bytes\') { result = typeValue.cast<Bytes>().asUint8List(); }';
              case IdlTypes.nString:
                return p +
                    'if (typeName == \'string\') { result = typeValue.cast<NativeString>().asString(); }';
              case IdlTypes.nEnum:
                return p +
                    'if (typeName == \'${type.object as String}\') { result = ${type.object as String}.values[typeValue.cast<Int64>().value]; }';
              case IdlTypes.nStruct:
                return p +
                    'if (typeName == \'${type.object as String}\') { result =  ${type.object as String}._from(typeValue.cast<_${type.object as String}>()); }';
              default:
                throw Exception(
                    'Type cannot be generated for type list in dart: [${interfaceValue.name}].');
            }
          });
          body += '''
            else { throw Exception(\'Invalid type in [${interfaceValue.name}.${field.fieldName}].\'); }
            $releaseValue 
          ''';
          break;
        case IdlTypes.nArray:
          body += '''
          final data = value.ref.data;
          final length = value.ref.length;
        ''';
          body += '${_getDataArray(field.idlType.object as IdlType)}';
          body += releaseValue;
          break;
        default:
          throw Exception(
              'Invalid type in interface for dart ffi stream code: [${field.toString()}].');
      }

      body += '_streamTarget${toPascalCase(field.fieldName)}.add(result);';

      return p +
          '''
            ${interfaceValue.name}._methodSetStream${toPascalCase(field.fieldName)}(_instance,
            _streamHandle${toPascalCase(field.fieldName)}.cast<Void>(), 
            Pointer.fromFunction(${interfaceValue.name}._onStream${toPascalCase(field.fieldName)}, $errorOnException));
            _stream${toPascalCase(field.fieldName)}[_streamHandle${toPascalCase(field.fieldName)}] = (value) { $body };
        ''';
    });
  }

  String _setAsyncForInterface(
      IdlInterface interfaceValue, IdlInterfaceField field) {
    var body = '';

    final releaseValue = '''
        final releaseResultError = ${interfaceValue.name}._methodRelease${toPascalCase(field.fieldName)}(_instance, value);
        if (releaseResultError != 0) _handleError(releaseResultError);
      ''';

    final retType = (field.idlType.object as IdlMethod).returnType;

    switch (retType.objectType) {
      case IdlTypes.nVoid:
        return 'completer.complete();';
      case IdlTypes.nBool:
        body += 'final result = value == 0 ? false : true;';
        break;
      case IdlTypes.nFloat:
        body += 'final result = value;';
        break;
      case IdlTypes.nInt:
        body += 'final result = value;';
        break;
      case IdlTypes.nString:
        body += ''' 
            final result = value.asString();
            $releaseValue
          ''';
        break;
      case IdlTypes.nBytes:
        body += ''' 
            final result = value.asUint8List();
            $releaseValue
          ''';
        break;
      case IdlTypes.nStruct:
        body += ''' 
            final result = ${retType.object as String}._from(value);
            $releaseValue 
          ''';
        break;
      case IdlTypes.nEnum:
        body += 'final result = ${retType.object as String}.values[value];';
        break;
      case IdlTypes.nTypeList:
        final typeList = (retType.object as IdlTypeList).typeList;
        assert(typeList.isNotEmpty);
        body += ''' 
            final typeName = value.ref.name.asString();
            final typeValue = value.ref.ptrData;
            dynamic result;
          ''';
        body += typeList.fold('', (p, type) {
          if (p.isNotEmpty) p += 'else ';
          switch (type.objectType) {
            case IdlTypes.nNone:
              return p +
                  '''
              if (typeName == \'none\') { 
                if (typeValue.cast<Int64>().value != 0) {
                  throw ArgumentError(\'[${field.fieldName}] is not zero.\');
                }
                result = null;
              }
            ''';
            case IdlTypes.nBool:
              return p +
                  'if (typeName == \'bool\') { result = typeValue.cast<Int64>().value == 0 ? false : true; }';
            case IdlTypes.nInt:
              return p +
                  'if (typeName == \'int\') { result = typeValue.cast<Int64>().value; }';
            case IdlTypes.nFloat:
              return p +
                  'if (typeName == \'float\') { result = typeValue.cast<Double>().value; }';
            case IdlTypes.nBytes:
              return p +
                  'if (typeName == \'bytes\') { result = typeValue.cast<Bytes>().asUint8List(); }';
            case IdlTypes.nString:
              return p +
                  'if (typeName == \'string\') { result = typeValue.cast<NativeString>().asString(); }';
            case IdlTypes.nEnum:
              return p +
                  'if (typeName == \'${type.object as String}\') { result = ${type.object as String}.values[typeValue.cast<Int64>().value]; }';
            case IdlTypes.nStruct:
              return p +
                  'if (typeName == \'${type.object as String}\') { result =  ${type.object as String}._from(typeValue.cast<_${type.object as String}>()); }';
            default:
              throw Exception(
                  'Type cannot be generated for type list in dart: [${interfaceValue.name}]');
          }
        });
        body += '''
            else { throw Exception(\'Invalid type in [${interfaceValue.name}.${field.fieldName}].\'); }
            $releaseValue 
          ''';
        break;
      case IdlTypes.nArray:
        body += '''
          final data = value.ref.data;
          final length = value.ref.length;
        ''';
        body += '${_getDataArray(retType.object as IdlType)}';
        body += releaseValue;
        break;
      default:
        throw Exception(
            'Invalid type in interface for dart ffi async code: [${field.toString()}].');
    }

    return body + 'completer.complete(result);';
  }

  String _createTypeListToffi(
    List<IdlType> typeList, {
    String resultName = 'result',
    String valueName = 'value',
  }) {
    assert(typeList.isNotEmpty);
    var body = '';

    body += '''
            final $resultName = allocate<Type>();
          ''';
    body += typeList.fold('', (p, type) {
      if (p.isNotEmpty) p += 'else ';
      switch (type.objectType) {
        case IdlTypes.nNone:
          return p +
              ''' 
                if ($valueName == null) {
                  final ptrData = allocate<Int64>();
                  ptrData.value = 0;
                  $resultName.ref.ptrData = ptrData.cast<Void>();
                  $resultName.ref.name = 'none'.asNativeString();
                }
              ''';
        case IdlTypes.nBool:
          return p +
              '''
              if ($valueName is bool) {
                final ptrData = allocate<Int64>();
                ptrData.value = $valueName ? 1 : 0;
                $resultName.ref.ptrData = ptrData.cast<Void>();
                $resultName.ref.name = 'bool'.asNativeString();
              }
            ''';
        case IdlTypes.nInt:
          return p +
              '''
              if ($valueName is int) {
                final ptrData = allocate<Int64>();
                ptrData.value = $valueName;
                $resultName.ref.ptrData = ptrData.cast<Void>();
                $resultName.ref.name = 'int'.asNativeString();
              }
            ''';
        case IdlTypes.nFloat:
          return p +
              '''
              if ($valueName is double) {
                final ptrData = allocate<Double>();
                ptrData.value = $valueName;
                $resultName.ref.ptrData = ptrData.cast<Void>();
                $resultName.ref.name = 'float'.asNativeString();
              }
            ''';
        case IdlTypes.nBytes:
          return p +
              '''
              if ($valueName is Uint8List) {
                $resultName.ref.ptrData = ($valueName as Uint8List).asNativeBytes().cast<Void>();
                $resultName.ref.name = 'bytes'.asNativeString();
              }
            ''';
        case IdlTypes.nString:
          return p +
              '''
              if ($valueName is String) {
                $resultName.ref.ptrData = ($valueName as String).asNativeString().cast<Void>();
                $resultName.ref.name = 'string'.asNativeString();
              }
            ''';
        case IdlTypes.nEnum:
          return p +
              '''
              if ($valueName is ${type.object as String}) {
                final ptrData = allocate<Int64>();
                ptrData.value = ${type.object as String}.values.indexOf($valueName);
                $resultName.ref.ptrData = ptrData.cast<Void>();
                $resultName.ref.name = '${type.object as String}'.asNativeString();
              }
            ''';
        case IdlTypes.nStruct:
          return p +
              '''
              if ($valueName is ${type.object as String}) {
                $resultName.ref.ptrData = _${type.object as String}.from($valueName as ${type.object as String}).cast<Void>();
                $resultName.ref.name = '${type.object as String}'.asNativeString();
              }
            ''';
        default:
          throw Exception('Type cannot be generated for type list in dart: []');
      }
    });
    body += '''
          else { throw Exception(\'Invalid type in [].\'); }
        ''';

    return body;
  }

  String _createTypeListToffiRelease(
    List<IdlType> typeList, {
    String resultName = 'result',
    String fieldName = '',
  }) {
    final typeName = 'typeName${toPascalCase(fieldName)}';
    var body = 'final $typeName = $resultName.ref.name.asString();';

    body += typeList.fold('', (p, type) {
      if (p.isNotEmpty) p += 'else ';
      switch (type.objectType) {
        case IdlTypes.nNone:
        case IdlTypes.nBool:
        case IdlTypes.nInt:
          return p +
              ''' 
            if ($typeName == \'int\'
            || $typeName == \'none\'
            || $typeName == \'bool\') {
              ($resultName.ref.ptrData as Pointer<Int64>).release();
            }
          ''';
        case IdlTypes.nFloat:
          return p +
              '''
            if ($typeName == \'float\') {
              ($resultName.ref.ptrData as Pointer<Double>).release();
            }
          ''';
        case IdlTypes.nBytes:
          return p +
              '''
            if ($typeName == \'bytes\') {
              ($resultName.ref.ptrData as Pointer<Bytes>).release();
            }
          ''';
        case IdlTypes.nString:
          return p +
              '''
            if ($typeName == \'string\') {
              ($resultName.ref.ptrData as Pointer<NativeString>).release();
            }
          ''';
        case IdlTypes.nEnum:
          return p +
              '''
            if ($typeName == \'${type.object as String}\') {
              ($resultName.ref.ptrData as Pointer<Int64>).release();
            }
          ''';
        case IdlTypes.nStruct:
          return p +
              '''
            if ($typeName == \'${type.object as String}\') {
              ($resultName.ref.ptrData as Pointer<_${type.object as String}>).release();
            }
          ''';
        default:
      }
    });

    body += '$resultName.release();';

    return body;
  }

  // sync, async
  String _typeToffiForInterfaceFieldSync(
      IdlInterface interfaceValue, IdlInterfaceField field,
      {bool isSync = true}) {
    var body = '';

    final mType = field.idlType.object as IdlMethod;

    final argTypeList = mType.args.object as IdlTypeTuple;

    var setValue;
    var argList = '';
    var finallyBlock = '';

    argTypeList.typeList.forEach((entry) {
      final valueName = entry.name;
      final type = entry.idlType;
      final resultName = 'fValue${toPascalCase(valueName)}';
      final fieldNameValue = toPascalCase(valueName);
      argList += ',$resultName';

      switch (type.objectType) {
        case IdlTypes.nBool:
          body += 'final $resultName = $valueName ? 1 : 0;';
          break;
        case IdlTypes.nFloat:
        case IdlTypes.nInt:
          body += 'final $resultName = $valueName;';
          break;
        case IdlTypes.nString:
          body += 'final $resultName = $valueName.asNativeString();';
          break;
        case IdlTypes.nBytes:
          body += 'final $resultName = $valueName.asNativeBytes();';
          break;
        case IdlTypes.nStruct:
          body +=
              'final $resultName = _${type.object as String}.from($valueName);';
          break;
        case IdlTypes.nEnum:
          body +=
              'final $resultName = ${type.object as String}.values.indexOf($valueName);';
          break;
        case IdlTypes.nTypeList:
          final typeList = (type.object as IdlTypeList).typeList;
          body += _createTypeListToffi(typeList,
              resultName: resultName, valueName: valueName);
          break;
        case IdlTypes.nArray:
          body += '''
          final $resultName = allocate<ArrayType>();
          ${_setArrayData(type.object as IdlType, valueName, fieldNameValue)}
          $resultName.ref.data = data$fieldNameValue.cast<Void>();
          $resultName.ref.length = length$fieldNameValue;
        ''';
          break;
        default:
          throw Exception(
              'Invalid type in interface for dart ffi set code [${type.toString()}].');
      }

      if (!type.objectType.isPrimitive) {
        if (type.objectType == IdlTypes.nTypeList) {
          final typeList = (type.object as IdlTypeList).typeList;
          finallyBlock +=
              _createTypeListToffiRelease(typeList, resultName: resultName);
        } else {
          finallyBlock += '$resultName.release();';
        }
      }
    });

    if (isSync) {
      if (mType.returnType.objectType == IdlTypes.nVoid) {
        setValue = '''
          final resultError = ${interfaceValue.name}._methodSync${toPascalCase(field.fieldName)}(_instance$argList);
          if (resultError != 0) _handleError(resultError);
        ''';
      } else {
        setValue = '''
          final resultError = ${interfaceValue.name}._methodSync${toPascalCase(field.fieldName)}(_instance$argList, fValue);
          if (resultError != 0) _handleError(resultError);
        ''';

        final releaseValue = '''
          final releaseResultError = ${interfaceValue.name}._methodRelease${toPascalCase(field.fieldName)}(_instance, fValue.value);
          if (releaseResultError != 0) _handleError(releaseResultError);
        ''';

        switch (mType.returnType.objectType) {
          case IdlTypes.nBool:
            body += 'final fValue = allocate<Int64>();';
            setValue = '''
          $setValue
          final result = fValue.value == 0 ? false : true;
        ''';
            break;
          case IdlTypes.nFloat:
            body += 'final fValue = allocate<Double>();';
            setValue = '''
          $setValue
          final result = fValue.value;
        ''';
            break;
          case IdlTypes.nInt:
            body += 'final fValue = allocate<Int64>();';
            setValue = '''
          $setValue
          final result = fValue.value;
        ''';
            break;
          case IdlTypes.nString:
            body += 'final fValue = allocate<Pointer<NativeString>>();';
            setValue = '''
          $setValue
          final result = fValue.value.asString();
          $releaseValue
        ''';
            break;
          case IdlTypes.nBytes:
            body += 'final fValue = allocate<Pointer<Bytes>>();';
            setValue = '''
          $setValue
          final result = fValue.value.asUint8List();
          $releaseValue
        ''';
            break;
          case IdlTypes.nStruct:
            body +=
                'final fValue = allocate<Pointer<_${mType.returnType.object as String}>>();';
            setValue = '''
          $setValue
          final result = ${mType.returnType.object as String}._from(fValue.value);
          $releaseValue 
        ''';
            break;
          case IdlTypes.nEnum:
            body += 'final fValue = allocate<Int64>();';
            setValue = '''
          $setValue
          final result = ${mType.returnType.object as String}.values[fValue.value];
        ''';
            break;
          case IdlTypes.nTypeList:
            final typeList = (mType.returnType.object as IdlTypeList).typeList;
            assert(typeList.isNotEmpty);
            body += 'final fValue = allocate<Pointer<Type>>();';
            setValue = '''
          $setValue
          final typeName = fValue.value.ref.name.asString();
          final typeValue = fValue.value.ref.ptrData;
          dynamic result;
        ''';
            setValue += typeList.fold('', (p, type) {
              if (p.isNotEmpty) p += 'else ';
              switch (type.objectType) {
                case IdlTypes.nNone:
                  return p +
                      '''
              if (typeName == \'none\') { 
                if (typeValue.cast<Int64>().value != 0) {
                  throw ArgumentError(\'[${field.fieldName}] is not zero.\');
                }
                result = null;
              }
            ''';
                case IdlTypes.nBool:
                  return p +
                      'if (typeName == \'bool\') { result = typeValue.cast<Int64>().value == 0 ? false : true; }';
                case IdlTypes.nInt:
                  return p +
                      'if (typeName == \'int\') { result = typeValue.cast<Int64>().value; }';
                case IdlTypes.nFloat:
                  return p +
                      'if (typeName == \'float\') { result = typeValue.cast<Double>().value; }';
                case IdlTypes.nBytes:
                  return p +
                      'if (typeName == \'bytes\') { result = typeValue.cast<Bytes>().asUint8List(); }';
                case IdlTypes.nString:
                  return p +
                      'if (typeName == \'string\') { result = typeValue.cast<NativeString>().asString(); }';
                case IdlTypes.nEnum:
                  return p +
                      'if (typeName == \'${type.object as String}\') { result = ${type.object as String}.values[typeValue.cast<Int64>().value]; }';
                case IdlTypes.nStruct:
                  return p +
                      'if (typeName == \'${type.object as String}\') { result =  ${type.object as String}._from(typeValue.cast<_${type.object as String}>()); }';
                default:
                  throw Exception(
                      'Type cannot be generated for type list in dart: [${interfaceValue.name}]');
              }
            });
            setValue += '''
          else { throw Exception(\'Invalid type in [${interfaceValue.name}.${field.fieldName}].\'); }
          $releaseValue 
        ''';
            break;
          case IdlTypes.nArray:
            body += 'final fValue = allocate<Pointer<ArrayType>>();';
            setValue = '''
          $setValue
          final data = fValue.value.ref.data;
          final length = fValue.value.ref.length;
        ''';
            setValue += '${_getDataArray(mType.returnType.object as IdlType)}';
            setValue += releaseValue;
            break;
          default:
            throw Exception(
                'Invalid type in interface for dart ffi get code: [${field.toString()}].');
        }

        setValue += 'return result;';
        finallyBlock += 'free(fValue);';
      }
    } else {
      final value = mType.returnType.objectType == IdlTypes.nVoid ? '' : 'value';

      setValue = '''
        final handle = allocate<Int64>();
        final completer = Completer<${_getTypeName(mType.returnType)}>();
        _asyncHandleCall${toPascalCase(field.fieldName)}[handle] = ($value) {
            ${_setAsyncForInterface(interfaceValue, field)}
            free(handle);
        };
        final resultError = ${interfaceValue.name}._methodAsync${toPascalCase(field.fieldName)}(_instance$argList, handle.cast<Void>());
        if (resultError != 0) _handleError(resultError);
        return completer.future;
      ''';
    }

    if (finallyBlock.isNotEmpty) {
      return '''$body
        try { 
          $setValue
        } finally { 
          $finallyBlock 
        }
      ''';
    }

    return '$body $setValue';
  }

  // set
  String _typeToffiForInterfaceFieldSet(
      IdlInterface interfaceValue, IdlInterfaceField field) {
    final setValue = '''
          final resultError = ${interfaceValue.name}._methodSet${toPascalCase(field.fieldName)}(_instance, result);
          if (resultError != 0) _handleError(resultError);
        ''';
    var body = '';

    switch (field.idlType.objectType) {
      case IdlTypes.nBool:
        body += 'final result = value ? 1 : 0;$setValue';
        break;
      case IdlTypes.nFloat:
      case IdlTypes.nInt:
        body += 'final result = value;$setValue';
        break;
      case IdlTypes.nString:
        body += '''
          final result = value.asNativeString();
          try { $setValue }
          finally { result.release(); }
        ''';
        break;
      case IdlTypes.nBytes:
        body += '''
          final result = value.asNativeBytes();
          try { $setValue }
          finally { result.release(); }
        ''';
        break;
      case IdlTypes.nStruct:
        body += '''
          final result = _${field.idlType.object as String}.from(value);
          try { $setValue }
          finally { result.release(); }
        ''';
        break;
      case IdlTypes.nEnum:
        body += '''
          final result = ${field.idlType.object as String}.values.indexOf(value);
          $setValue
        ''';
        break;
      case IdlTypes.nTypeList:
        final typeList = (field.idlType.object as IdlTypeList).typeList;
        body += '''
          ${_createTypeListToffi(typeList)}
          try { 
            $setValue 
          } finally {
            result.release();
            ptrData.release(); 
          }
        ''';
        break;
      case IdlTypes.nArray:
        body += '''
          final result = allocate<ArrayType>();
          ${_setArrayData(field.idlType.object as IdlType)}
          result.ref.data = data.cast<Void>();
          result.ref.length = length;
          try {
            $setValue
          } finally {
            ${_releaseArrayData(field.idlType.object as IdlType)}
            result.release();
          }
        ''';
        break;
      default:
        throw Exception(
            'Invalid type in interface for dart ffi set code [${field.toString()}].');
    }

    return body;
  }

  String _releaseArrayData(IdlType arrayType) {
    switch (arrayType.objectType) {
      case IdlTypes.nInt:
      case IdlTypes.nFloat:
      case IdlTypes.nBool:
      case IdlTypes.nEnum:
        return 'free(data);';
      case IdlTypes.nString:
        return '''
          for (var i = 0; i < length; i += 1) {
            data.cast<NativeString>().elementAt(i).release();
          }
          free(data);
        ''';
      case IdlTypes.nBytes:
        return '''
          for (var i = 0; i < length; i += 1) {
            data.cast<Bytes>().elementAt(i).release();
          }
          free(data);
        ''';
      case IdlTypes.nStruct:
        return '''
          for (var i = 0; i < length; i += 1) {
            data.cast<_${arrayType.object as String}>().elementAt(i).release();
          }
          free(data);
        ''';
      case IdlTypes.nArray:
        final aType = arrayType.object as IdlType;
        return '''
          final releaseList = (data, length) {
              ${_releaseArrayData(aType)}
          };
          for (var i = 0; i < length; i += 1) {
            final listSource = data.elementAt(i).cast<ArrayType>().ref;
            releaseList(listSource.data, listSource.length);
          }
          free(data);
        ''';
      default:
        throw Exception('Invalid type for array in get code:');
    }
  }

  String _ffiCreateObject(IdlType type, String varName) {
    switch (type.objectType) {
      case IdlTypes.nBool:
      case IdlTypes.nFloat:
      case IdlTypes.nInt:
        return varName;
      case IdlTypes.nString:
        return '$varName.asNativeString()';
      case IdlTypes.nBytes:
        return '$varName.asNativeBytes()';
      case IdlTypes.nStruct:
        return '_${type.object as String}.from($varName)';
      default:
        throw Exception(
            'Invalid type in interface for dart ffi code [${type.toString()}].');
    }
  }

  String _ffiReleaseObject(IdlType type, String varName) {
    switch (type.objectType) {
      case IdlTypes.nBool:
      case IdlTypes.nFloat:
      case IdlTypes.nInt:
        return '';
      case IdlTypes.nString:
      case IdlTypes.nStruct:
      case IdlTypes.nBytes:
        return '$varName.release();';
      default:
        throw Exception(
            'Invalid type in interface for dart ffi code [${type.toString()}].');
    }
  }

  void _addInterfaces(IdlObject idlObject) {
    _interfaces.addAll(idlObject.interfaces.expand((interfaceValue) {
      // Add factories.
      final factoryConstructors = <Constructor>[];
      final condFields = <Field>[];
      final condMethods = <Method>[];
      var condConstructorIntializers = '';

      if (interfaceValue.fields.any((field) =>
          field.interfaceAttributes.contains(IdlInterfaceAttributes.mAsync))) {
        condMethods.add(Method((b) => b
          ..body = Code(interfaceValue.fields
              .where((field) => field.interfaceAttributes
                  .contains(IdlInterfaceAttributes.mAsync))
              .fold('', (p, field) {
            final mType = field.idlType.object as IdlMethod;
            final value =
                mType.returnType.objectType == IdlTypes.nVoid ? '' : 'value';
            final vvalue = ',$value';

            final errorOnException = 'AbiInternalError.callbackException';
            return p +
                '''${interfaceValue.name}._methodSetAsync${toPascalCase(field.fieldName)}(_instance,
                _asyncHandle${toPascalCase(field.fieldName)}.cast<Void>(), 
            Pointer.fromFunction(${interfaceValue.name}._onAsync${toPascalCase(field.fieldName)}, $errorOnException));
            _async${toPascalCase(field.fieldName)}[_asyncHandle${toPascalCase(field.fieldName)}] = (asyncHandle$vvalue) { 
              final asyncCall = _asyncHandleCall${toPascalCase(field.fieldName)}.remove(asyncHandle);
              if (asyncCall != null) asyncCall($value);
             };
          ''';
          }))
          ..returns = refer('void')
          ..name = '_setAsync'));

        condConstructorIntializers += '.._setAsync()';
      }

      if (interfaceValue.fields.any((field) =>
          field.interfaceAttributes.contains(IdlInterfaceAttributes.mStream))) {
        condMethods.add(Method((b) => b
          ..body = Code(_setStreamsForInterface(interfaceValue))
          ..returns = refer('void')
          ..name = '_setStreams'));

        condConstructorIntializers += '.._setStreams()';
      }

      interfaceValue.fields
          .where((field) => field.interfaceAttributes
              .contains(IdlInterfaceAttributes.mFactory))
          .forEach((field) {
        var parameters = <Parameter>[];
        var body = '';

        if (field.idlType.objectType != IdlTypes.nMethod) {
          throw Exception(
              'Factory must have named arguments: [${interfaceValue.name} ${field.fieldName}].');
        }
        final mType = field.idlType.object as IdlMethod;

        if (mType.returnType.objectType != IdlTypes.nVoid) {
          throw Exception(
              'Factory cannot return value: [${interfaceValue.name} ${field.fieldName}].');
        }

        final typeList = mType.args.object as IdlTypeTuple;
        var ffiArgs = '';
        var ffiVars = '';
        var ffiRelease = '';

        typeList.typeList.forEach((entry) {
          final tupleType = entry.idlType;
          final name = entry.name;

          if (tupleType.objectType != IdlTypes.nInterface) {
            parameters.add(Parameter((b) => b
              ..name = '$name'
              ..type = refer(_getTypeName(tupleType))));
            ffiVars +=
                'final fValue$name = ${_ffiCreateObject(tupleType, name)};';
            ffiArgs += ',fValue$name';
            ffiRelease += _ffiReleaseObject(tupleType, 'fValue$name');
          } else {
            throw Exception(
                'Invliad type for factory constructor with tuple: [${interfaceValue.name} factory ${field.fieldName}, ${tupleType.toString()}].');
          }
        });

        body += '''
            $ffiVars
            final resultPtr = allocate<Pointer<_${interfaceValue.name}>>();
            try {
              final resultError = ${interfaceValue.name}._factory${toPascalCase(field.fieldName)}(resultPtr $ffiArgs);
              if (resultError != 0) _handleFactoryError(resultError);
              final result = resultPtr.value;
              return ${interfaceValue.name}._from(result)$condConstructorIntializers;
            } finally {
              $ffiRelease
              free(resultPtr); 
            }''';

        factoryConstructors.add(Constructor((b) => b
          ..name = '${field.fieldName}'
          ..factory = true
          ..requiredParameters.addAll(parameters)
          ..body = Code(body)));

        condFields.add(Field((b) => b
          ..type = refer(
              '_Factory${interfaceValue.name}${toPascalCase(field.fieldName)}Func')
          ..assignment = Code(
              '_lib.lookupFunction<_Factory${interfaceValue.name}${toPascalCase(field.fieldName)}Native, _Factory${interfaceValue.name}${toPascalCase(field.fieldName)}Func>(\'factory_${toSnakeCase(interfaceValue.name)}_${toSnakeCase(field.fieldName)}\')')
          ..static = true
          ..modifier = FieldModifier.final$
          ..name = '_factory${toPascalCase(field.fieldName)}'));
      });

      // Create a default constructor if there are no factories.
      if (factoryConstructors.isEmpty) {
        factoryConstructors.add(Constructor((b) => b
          ..factory = true
          ..body = Code('''
            final resultPtr = allocate<Pointer<_${interfaceValue.name}>>();
            try {
              final resultError = ${interfaceValue.name}._factoryCreateInstance(resultPtr);
              if (resultError != 0) _handleFactoryError(resultError);
              final result = resultPtr.value;
              return ${interfaceValue.name}._from(result)$condConstructorIntializers;
            } finally {
              free(resultPtr);
            }''')));
        condFields.add(Field((b) => b
          ..name = '_factoryCreateInstance'
          ..type = refer('_Factory${interfaceValue.name}CreateInstanceFunc')
          ..assignment = Code(
              '_lib.lookupFunction<_Factory${interfaceValue.name}CreateInstanceNative, _Factory${interfaceValue.name}CreateInstanceFunc>(\'factory_${toSnakeCase(interfaceValue.name)}_create_instance\')')
          ..static = true
          ..modifier = FieldModifier.final$));
      }

      return [
        Class(
          (b) => b
            ..constructors.addAll(factoryConstructors)
            ..constructors.add(Constructor((b) => b
              ..name = '_from'
              ..requiredParameters.add(Parameter((b) => b
                ..toThis = true
                ..name = '_instance'))))
            ..name = interfaceValue.name
            ..fields.addAll([
              Field((b) => b
                ..name = '_instance'
                ..modifier = FieldModifier.final$
                ..type = refer('Pointer<_${interfaceValue.name}>')),
              Field((b) => b
                ..name = '_lib'
                ..modifier = FieldModifier.final$
                ..static = true
                ..type = refer('DynamicLibrary')
                ..assignment =
                    Code('openLibrary(\'${toSnakeCase(idlObject.name)}\')')),
              Field((b) => b
                ..name = '_releaseInstance'
                ..type = refer('_Release${interfaceValue.name}Func')
                ..assignment = Code(
                    '_lib.lookupFunction<_Release${interfaceValue.name}Native, _Release${interfaceValue.name}Func>(\'release_${toSnakeCase(interfaceValue.name)}\')')
                ..static = true
                ..modifier = FieldModifier.final$)
            ])
            ..fields.addAll(condFields)
            // Creates the function variables.
            ..fields.addAll(interfaceValue.fields
                .where((field) => !field.interfaceAttributes
                    .contains(IdlInterfaceAttributes.mFactory))
                .expand((field) {
              final fields = <Field>[];

              final createFinalStaticField = (functionName) {
                final nativeNameFunction =
                    '_Method${interfaceValue.name}${toPascalCase(functionName)}${toPascalCase(field.fieldName)}Native';
                final funcNameFunction =
                    '_Method${interfaceValue.name}${toPascalCase(functionName)}${toPascalCase(field.fieldName)}Func';
                final fieldFunctionName =
                    '_method${toPascalCase(functionName)}${toPascalCase(field.fieldName)}';
                final ffiFunctionName =
                    'method_${toSnakeCase(interfaceValue.name)}_${toSnakeCase(functionName)}_${toSnakeCase(field.fieldName)}';

                return Field((b) => b
                  ..name = fieldFunctionName
                  ..modifier = FieldModifier.final$
                  ..static = true
                  ..type = refer(funcNameFunction)
                  ..assignment = Code(
                      '_lib.lookupFunction<$nativeNameFunction, $funcNameFunction>(\'$ffiFunctionName\')'));
              };
              // stream
              if (field.interfaceAttributes
                  .contains(IdlInterfaceAttributes.mStream)) {
                fields.add(createFinalStaticField('setStream'));
                if (!field.idlType.objectType.isPrimitive) {
                  fields.add(createFinalStaticField('release'));
                }
              } else if (field.interfaceAttributes
                  .contains(IdlInterfaceAttributes.mAsync)) {
                final mType = field.idlType.object as IdlMethod;

                fields.add(createFinalStaticField('async'));
                fields.add(createFinalStaticField('setAsync'));

                if (mType.returnType.objectType != IdlTypes.nVoid) {
                  if (!mType.returnType.objectType.isPrimitive) {
                    fields.add(createFinalStaticField('release'));
                  }
                }
              } else if (field.interfaceAttributes
                  .contains(IdlInterfaceAttributes.mSync)) {
                final mType = field.idlType.object as IdlMethod;
                fields.add(createFinalStaticField('sync'));

                if (!mType.returnType.objectType.isPrimitive) {
                  fields.add(createFinalStaticField('release'));
                }
              } else {
                // get
                if (field.interfaceAttributes
                    .contains(IdlInterfaceAttributes.pGet)) {
                  fields.add(createFinalStaticField('get'));

                  if (!field.idlType.objectType.isPrimitive) {
                    fields.add(createFinalStaticField('release'));
                  }
                }

                // set
                if (field.interfaceAttributes
                    .contains(IdlInterfaceAttributes.pSet)) {
                  fields.add(createFinalStaticField('set'));
                }
              }

              return fields;
            }))
            ..fields.addAll(interfaceValue.fields
                .where((field) => !field.interfaceAttributes
                    .contains(IdlInterfaceAttributes.mFactory))
                .expand((field) {
              final fields = <Field>[];

              if (field.interfaceAttributes
                  .contains(IdlInterfaceAttributes.mStream)) {
                fields.add(Field((b) => b
                  ..name = '_streamHandle${toPascalCase(field.fieldName)}'
                  ..assignment = Code('allocate<Int64>()')
                  ..modifier = FieldModifier.final$));
                fields.add(Field((b) => b
                  ..name = '_streamTarget${toPascalCase(field.fieldName)}'
                  ..modifier = FieldModifier.final$
                  ..assignment = Code(
                      'StreamController<${_getTypeName(field.idlType)}>.broadcast()')));
                fields.add(Field((b) => b
                  ..name = '_stream${toPascalCase(field.fieldName)}'
                  ..modifier = FieldModifier.final$
                  ..static = true
                  ..assignment = Code(
                      'HashMap<Pointer<Int64>, void Function(${_getDartTypeForInterface(field.idlType)})>()')));
              } else if (field.interfaceAttributes
                  .contains(IdlInterfaceAttributes.mAsync)) {
                final mType = field.idlType.object as IdlMethod;

                var retType = '';
                if (mType.returnType.objectType != IdlTypes.nVoid) {
                  retType = _getDartTypeForInterface(mType.returnType);
                }

                fields.add(Field((b) => b
                  ..name = '_asyncHandleCall${toPascalCase(field.fieldName)}'
                  ..modifier = FieldModifier.final$
                  ..assignment = Code(
                      'HashMap<Pointer<Int64>, void Function($retType)>()')));
                fields.add(Field((b) => b
                  ..name = '_asyncHandle${toPascalCase(field.fieldName)}'
                  ..assignment = Code('allocate<Int64>()')
                  ..modifier = FieldModifier.final$));
                fields.add(Field((b) => b
                  ..name = '_async${toPascalCase(field.fieldName)}'
                  ..modifier = FieldModifier.final$
                  ..static = true
                  ..assignment = Code(
                      'HashMap<Pointer<Int64>, void Function(Pointer<Int64>' +
                          (retType.isNotEmpty ? ',$retType' : '') +
                          ')>()')));
              }

              return fields;
            }))
            ..methods.addAll(condMethods)
            ..methods.add(Method((b) => b
              ..name = 'release'
              ..returns = refer('void')
              ..body = Code(_getReleaseMethodForInterface(interfaceValue))))
            ..methods.add(Method((b) => b
              ..name = '_handleError'
              ..returns = refer('void')
              ..body = Code('''
                  switch (error) {
                    case AbiInternalError.none:
                      return;
                    case AbiInternalError.invalidArg:
                      throw ArgumentError(\'[${interfaceValue.name}]\');
                    default:
                      throw Exception(\'Error \$error, [${interfaceValue.name}].\');
                  }
                  ''')
              ..requiredParameters.add(Parameter((b) => b
                ..name = 'error'
                ..type = refer('int')))))
            ..methods.add(Method((b) => b
              ..name = '_handleFactoryError'
              ..returns = refer('void')
              ..body = Code('''
                  switch (error) {
                    case AbiInternalError.invalidArg:
                      throw ArgumentError(\'[${interfaceValue.name}]\');
                    case AbiInternalError.abort:
                    default:
                      throw Error();
                  }
                  ''')
              ..static = true
              ..requiredParameters.add(Parameter((b) => b
                ..name = 'error'
                ..type = refer('int')))))
            ..methods.add(Method((b) => b
              ..name = '_handleReleaseError'
              ..returns = refer('void')
              ..body = Code(
                  'throw Exception(\'Error releasing instance: [${interfaceValue.name}].\');')
              ..static = true
              ..requiredParameters.add(Parameter((b) => b
                ..name = 'error'
                ..type = refer('int')))))
            ..methods.addAll(
              interfaceValue.fields
                  .where((field) =>
                      field.interfaceAttributes
                          .contains(IdlInterfaceAttributes.pGet) ||
                      field.interfaceAttributes
                          .contains(IdlInterfaceAttributes.pSet) ||
                      field.interfaceAttributes
                          .contains(IdlInterfaceAttributes.mStream) ||
                      field.interfaceAttributes
                          .contains(IdlInterfaceAttributes.mSync) ||
                      field.interfaceAttributes
                          .contains(IdlInterfaceAttributes.mAsync))
                  .expand((field) {
                final methods = <Method>[];

                if (field.interfaceAttributes
                    .contains(IdlInterfaceAttributes.mSync)) {
                  final typeName =
                      _getTypeName((field.idlType.object as IdlMethod).returnType);
                  // sync
                  methods.add(Method((b) => b
                    ..name = field.fieldName
                    ..returns = refer(typeName)
                    ..body = Code(
                        '${_typeToffiForInterfaceFieldSync(interfaceValue, field)}')
                    ..requiredParameters.addAll(
                        ((field.idlType.object as IdlMethod).args.object
                                as IdlTypeTuple).typeList
                            .map((entry) {
                      final type = entry.idlType;
                      final argName = entry.name;
                      if (type.objectType == IdlTypes.nInterface) {
                        throw Exception(
                            'Invliad type for method arg type: [${interfaceValue.name}.${field.fieldName}], [${type.toString()}].');
                      }
                      return Parameter((b) => b
                        ..name = argName
                        ..type = refer(_getTypeName(type)));
                    }))));
                } else if (field.interfaceAttributes
                    .contains(IdlInterfaceAttributes.mAsync)) {
                  final typeName =
                      _getTypeName((field.idlType.object as IdlMethod).returnType);

                  // async
                  methods.add(Method((b) => b
                    ..name = field.fieldName
                    ..returns = refer('Future<$typeName>')
                    ..body = Code(
                        '${_typeToffiForInterfaceFieldSync(interfaceValue, field, isSync: false)}')
                    ..requiredParameters.addAll(
                        ((field.idlType.object as IdlMethod).args.object
                                as IdlTypeTuple)
                            .typeList
                            .map((entry) {
                      final type = entry.idlType;
                      final argName = entry.name;
                      if (type.objectType == IdlTypes.nInterface) {
                        throw Exception(
                            'Invalid type for method arg type: [${interfaceValue.name}.${field.fieldName}], [${type.toString()}].');
                      }
                      return Parameter((b) => b
                        ..name = argName
                        ..type = refer(_getTypeName(type)));
                    }))));

                  final parameters = <Parameter>[];
                  final mType = field.idlType.object as IdlMethod;

                  if (mType.returnType.objectType != IdlTypes.nVoid) {
                    parameters.add(Parameter((b) => b
                      ..name = 'value'
                      ..type =
                          refer(_getDartTypeForInterface(mType.returnType))));
                  }

                  methods.add(Method((b) => b
                    ..static = true
                    ..name = '_onAsync${toPascalCase(field.fieldName)}'
                    ..returns = refer('int')
                    ..body = Code(
                        '${_getAsyncForInterfaceField(interfaceValue, field)}')
                    ..requiredParameters.add(Parameter((b) => b
                      ..name = 'handle'
                      ..type = refer('Pointer<Void>')))
                    ..requiredParameters.add(Parameter((b) => b
                      ..name = 'asyncHandle'
                      ..type = refer('Pointer<Void>')))
                    ..requiredParameters.addAll(parameters)));
                } else {
                  final typeName = _getTypeName(field.idlType);

                  if (field.interfaceAttributes
                      .contains(IdlInterfaceAttributes.mStream)) {
                    // stream
                    methods.add(Method((b) => b
                      ..static = true
                      ..name = '_onStream${toPascalCase(field.fieldName)}'
                      ..returns = refer('int')
                      ..body = Code(
                          '${_getStreamForInterfaceField(interfaceValue, field)}')
                      ..requiredParameters.add(Parameter((b) => b
                        ..name = 'handle'
                        ..type = refer('Pointer<Void>')))
                      ..requiredParameters.add(Parameter((b) => b
                        ..name = 'value'
                        ..type =
                            refer(_getDartTypeForInterface(field.idlType))))));
                    methods.add(Method((b) => b
                      ..name = field.fieldName
                      ..body = Code(
                          'return _streamTarget${toPascalCase(field.fieldName)}.stream;')
                      ..type = MethodType.getter
                      ..returns = refer('Stream<$typeName>')));
                  } else {
                    // property
                    // get
                    if (field.interfaceAttributes
                        .contains(IdlInterfaceAttributes.pGet)) {
                      methods.add(Method((b) => b
                        ..name = field.fieldName
                        // This creates a body for a get, calling the ffi function using the interface instance.
                        ..body = Code(
                            '${_ffiToTypeForInterfaceFieldGet(interfaceValue, field)}')
                        ..type = MethodType.getter
                        ..returns = refer(typeName)));
                    }

                    // set
                    if (field.interfaceAttributes
                        .contains(IdlInterfaceAttributes.pSet)) {
                      methods.add(Method((b) => b
                        ..name = field.fieldName
                        // This creates a body for a set, converting the value if necessary, then calling the ffi function using the interface instance.
                        ..body = Code(
                            '${_typeToffiForInterfaceFieldSet(interfaceValue, field)}')
                        ..type = MethodType.setter
                        ..requiredParameters.add((Parameter((b) => b
                          ..name = 'value'
                          ..type = refer(typeName))))));
                    }
                  }
                }

                if (methods.isEmpty) {
                  throw Exception(
                      'Error generating code for field: [${field.fieldName}]');
                }

                return methods;
              }),
            ),
        ),
        Class((b) => b
          ..name = '_${interfaceValue.name}'
          ..extend = refer('Struct')
          ..abstract = true)
      ];
    }));

    _interfacesTypedefs.addAll(
      idlObject.interfaces.expand((interfaceValue) {
        final typedefs = <Code>[];

        final factories = interfaceValue.fields.where((field) => field
            .interfaceAttributes
            .contains(IdlInterfaceAttributes.mFactory));
        if (factories.isNotEmpty) {
          factories.forEach((field) {
            assert(field.interfaceAttributes.length == 1);

            var argTypeNative = '';
            var argType = '';

            final mType = field.idlType.object as IdlMethod;
            final types = mType.args.object as IdlTypeTuple;
            types.typeList.forEach((entry) {
              final nativeType = _getffiTypeForInterface(entry.idlType);
              final dartType = _getDartTypeForInterface(entry.idlType);
              argTypeNative += ',$nativeType';
              argType += ',$dartType';
            });

            final nameFunction =
                '_Factory${interfaceValue.name}${toPascalCase(field.fieldName)}';

            final nativeTypedef =
                'typedef ${nameFunction}Native = Int64 Function(Pointer<Pointer<_${interfaceValue.name}>>$argTypeNative)';

            final funcTypedef =
                'typedef ${nameFunction}Func = int Function(Pointer<Pointer<_${interfaceValue.name}>>$argType)';
            typedefs.add(Code('$nativeTypedef;$funcTypedef;'));
          });
        } else {
          final nameFunction = '_Factory${interfaceValue.name}CreateInstance';
          final nativeTypedef =
              'typedef ${nameFunction}Native = Int64 Function(Pointer<Pointer<_${interfaceValue.name}>>)';
          final funcTypedef =
              'typedef ${nameFunction}Func = int Function(Pointer<Pointer<_${interfaceValue.name}>>)';
          typedefs.add(Code('$nativeTypedef;$funcTypedef;'));
        }

        typedefs.add(Code('''
          typedef _Release${interfaceValue.name}Native = Int64 Function(Pointer<_${interfaceValue.name}>);
          typedef _Release${interfaceValue.name}Func = int Function(Pointer<_${interfaceValue.name}>);
        '''));

        interfaceValue.fields
            .where((field) => field.interfaceAttributes
                .contains(IdlInterfaceAttributes.mSync))
            .forEach((field) {
          assert(field.interfaceAttributes.length == 1);

          var argTypeNative = '';
          var argType = '';

          final mType = field.idlType.object as IdlMethod;

          if (mType.args.objectType != IdlTypes.nTuple) {
            throw Exception(
                'Method args must be a tuple: [${mType.toString()}]');
          }

          (mType.args.object as IdlTypeTuple).typeList.forEach((entry) {
            final nativeType = _getffiTypeForInterface(entry.idlType);
            final dartType = _getDartTypeForInterface(entry.idlType);
            argTypeNative += ',$nativeType';
            argType += ',$dartType';
          });

          final retTypeNative = mType.returnType.objectType != IdlTypes.nVoid
              ? ',Pointer<${_getffiTypeForInterface(mType.returnType)}>'
              : '';

          final nameFunction =
              '_Method${interfaceValue.name}Sync${toPascalCase(field.fieldName)}';

          final nativeTypedef =
              'typedef ${nameFunction}Native = Int64 Function(Pointer<_${interfaceValue.name}>$argTypeNative$retTypeNative)';

          final funcTypedef =
              'typedef ${nameFunction}Func = int Function(Pointer<_${interfaceValue.name}>$argType$retTypeNative)';

          typedefs.add(Code('$nativeTypedef;$funcTypedef;'));
        });

        interfaceValue.fields
            .where((field) => field.interfaceAttributes
                .contains(IdlInterfaceAttributes.mAsync))
            .forEach((field) {
          assert(field.interfaceAttributes.length == 1);

          var argTypeNative = '';
          var argType = '';

          final mType = field.idlType.object as IdlMethod;

          if (mType.args.objectType != IdlTypes.nTuple) {
            throw Exception(
                'Method args must be a tuple: [${mType.toString()}]');
          }

          (mType.args.object as IdlTypeTuple).typeList.forEach((entry) {
            final nativeType = _getffiTypeForInterface(entry.idlType);
            final dartType = _getDartTypeForInterface(entry.idlType);
            argTypeNative += ',$nativeType';
            argType += ',$dartType';
          });

          final nameFunction =
              '_Method${interfaceValue.name}Async${toPascalCase(field.fieldName)}';

          final nativeTypedef =
              'typedef ${nameFunction}Native = Int64 Function(Pointer<_${interfaceValue.name}>$argTypeNative, Pointer<Void>)';

          final funcTypedef =
              'typedef ${nameFunction}Func = int Function(Pointer<_${interfaceValue.name}>$argType, Pointer<Void>)';

          typedefs.add(Code('$nativeTypedef;$funcTypedef;'));
        });

        interfaceValue.fields
            .where((field) =>
                field.interfaceAttributes
                    .contains(IdlInterfaceAttributes.pGet) ||
                field.interfaceAttributes
                    .contains(IdlInterfaceAttributes.pSet) ||
                field.interfaceAttributes
                    .contains(IdlInterfaceAttributes.mStream) ||
                field.interfaceAttributes
                    .contains(IdlInterfaceAttributes.mSync) ||
                field.interfaceAttributes
                    .contains(IdlInterfaceAttributes.mAsync))
            .forEach((field) {
          final createFieldFunctionTypedef =
              (functionName, argTypeNative, argType) {
            final nameFunction =
                '_Method${interfaceValue.name}${toPascalCase(functionName)}${toPascalCase(field.fieldName)}';

            if (argTypeNative.isNotEmpty) argTypeNative = ',$argTypeNative';
            if (argType.isNotEmpty) argType = ',$argType';

            var fTypeDef = '''
                typedef ${nameFunction}Native = Int64 Function(Pointer<_${interfaceValue.name}>$argTypeNative);
                typedef ${nameFunction}Func = int Function(Pointer<_${interfaceValue.name}>$argType);
            ''';

            return Code(fTypeDef);
          };

          if (field.interfaceAttributes
              .contains(IdlInterfaceAttributes.mSync)) {
            final mType = field.idlType.object as IdlMethod;
            var nativeType = '';
            if (mType.returnType.objectType != IdlTypes.nVoid) {
              nativeType = '${_getffiTypeForInterface(mType.returnType)}';
              if (!mType.returnType.objectType.isPrimitive) {
                typedefs.add(createFieldFunctionTypedef(
                    'release', nativeType, nativeType));
              }
            }
          } else if (field.interfaceAttributes
              .contains(IdlInterfaceAttributes.mAsync)) {
            final mType = field.idlType.object as IdlMethod;
            var nativeType = '';

            if (mType.returnType.objectType != IdlTypes.nVoid) {
              nativeType = '${_getffiTypeForInterface(mType.returnType)}';
              if (!mType.returnType.objectType.isPrimitive) {
                typedefs.add(createFieldFunctionTypedef(
                    'release', nativeType, nativeType));
              }
              nativeType = ',$nativeType';
            }
            final funTypeName =
                'Pointer<Void>, Pointer<NativeFunction<_Method${interfaceValue.name}${toPascalCase('onAsync')}${toPascalCase(field.fieldName)}Native>>';
            typedefs.add(createFieldFunctionTypedef(
                'setAsync', funTypeName, funTypeName));
            final argType = 'Pointer<Void>, Pointer<Void>$nativeType';
            final nameFunction =
                '_Method${interfaceValue.name}OnAsync${toPascalCase(field.fieldName)}';
            final fTypeDef =
                'typedef ${nameFunction}Native = Int64 Function($argType);';
            typedefs.add(Code(fTypeDef));
          } else {
            final nativeType = _getffiTypeForInterface(field.idlType);

            if (field.interfaceAttributes
                .contains(IdlInterfaceAttributes.mStream)) {
              final funTypeName =
                  '''Pointer<Void>, Pointer<NativeFunction<_Method${interfaceValue.name}${toPascalCase('onStream')}${toPascalCase(field.fieldName)}Native>>''';
              typedefs.add(createFieldFunctionTypedef(
                  'setStream', funTypeName, funTypeName));

              final argType = 'Pointer<Void>, $nativeType';
              final nameFunction =
                  '_Method${interfaceValue.name}OnStream${toPascalCase(field.fieldName)}';
              final fTypeDef =
                  'typedef ${nameFunction}Native = Int64 Function($argType);';
              typedefs.add(Code(fTypeDef));

              if (!field.idlType.objectType.isPrimitive) {
                typedefs.add(createFieldFunctionTypedef(
                    'release', nativeType, nativeType));
              }
            } else {
              if (field.interfaceAttributes
                  .contains(IdlInterfaceAttributes.pGet)) {
                typedefs.add(createFieldFunctionTypedef(
                    'get', 'Pointer<$nativeType>', 'Pointer<$nativeType>'));
                if (!field.idlType.objectType.isPrimitive) {
                  typedefs.add(createFieldFunctionTypedef(
                      'release', nativeType, nativeType));
                }
              }

              if (field.interfaceAttributes
                  .contains(IdlInterfaceAttributes.pSet)) {
                final dartType = _getDartTypeForInterface(field.idlType);
                typedefs.add(
                    createFieldFunctionTypedef('set', nativeType, dartType));
              }
            }
          }
        });

        return typedefs;
      }),
    );
  }

  String _getDartTypeForInterface(IdlType type) {
    switch (type.objectType) {
      case IdlTypes.nFloat:
        return 'double';
      case IdlTypes.nInt:
      case IdlTypes.nBool:
      case IdlTypes.nEnum:
      case IdlTypes.nNone:
        return 'int';
      case IdlTypes.nString:
        return 'Pointer<NativeString>';
      case IdlTypes.nBytes:
        return 'Pointer<Bytes>';
      case IdlTypes.nTypeList:
        return 'Pointer<Type>';
      case IdlTypes.nTuple:
        return 'Pointer<TupleList>';
      case IdlTypes.nStruct:
      case IdlTypes.nInterface:
        return 'Pointer<_${type.object as String}>';
      case IdlTypes.nArray:
        return 'Pointer<ArrayType>';
      default:
        throw Exception(
            'Invalid type in interface for dart code: [${type.toString()}]');
    }
  }

  String _getffiTypeForInterface(IdlType type) {
    switch (type.objectType) {
      case IdlTypes.nBool:
      case IdlTypes.nEnum:
      case IdlTypes.nNone:
      case IdlTypes.nInt:
        return 'Int64';
      case IdlTypes.nFloat:
        return 'Double';
      case IdlTypes.nString:
        return 'Pointer<NativeString>';
      case IdlTypes.nBytes:
        return 'Pointer<Bytes>';
      case IdlTypes.nTypeList:
        return 'Pointer<Type>';
      case IdlTypes.nTuple:
        return 'Pointer<TupleList>';
      case IdlTypes.nStruct:
      case IdlTypes.nInterface:
        return 'Pointer<_${type.object as String}>';
      case IdlTypes.nArray:
        return 'Pointer<ArrayType>';
      default:
        throw Exception(
            'Invalid type in interface for dart code: [${type.toString()}]');
    }
  }

  String _getffiTypeArrayForInterface(IdlType type) {
    switch (type.objectType) {
      case IdlTypes.nBool:
      case IdlTypes.nEnum:
      case IdlTypes.nInt:
        return 'Int64';
      case IdlTypes.nFloat:
        return 'Double';
      case IdlTypes.nString:
        return 'NativeString';
      case IdlTypes.nBytes:
        return 'Bytes';
      case IdlTypes.nTypeList:
        return 'Type';
      case IdlTypes.nTuple:
        return 'TupleList';
      case IdlTypes.nStruct:
      case IdlTypes.nInterface:
        return '_${type.object as String}';
      case IdlTypes.nArray:
        return 'ArrayType';
      default:
        throw Exception(
            'Invalid type in interface for dart code: [${type.toString()}]');
    }
  }

  void _finalize(IdlObject idlObject) {
    final library = Library((b) => b
      ..body.add(Code('''


        // ------------------------------------------
        // This is generated code.
        // Modifying it is pointless, don't be silly.
        // ------------------------------------------


      '''))
      ..directives.add(Directive.import('dart:ffi'))
      ..directives.add(Directive.import('dart:async'))
      ..directives.add(Directive.import('dart:typed_data'))
      ..directives.add(Directive.import('dart:collection'))
      ..directives.add(Directive.import('package:ffi/ffi.dart'))
      ..directives.add(Directive.import('ffi_internal.dart'))
      ..body.addAll(_strings)
      ..body.addAll(_ints)
      ..body.addAll(_floats)
      ..body.addAll(_enums)
      ..body.addAll(_structs)
      ..body.addAll(_interfaces)
      ..body.addAll(_structsExtension)
      ..body.addAll(_interfacesTypedefs)
      ..body.addAll(_interfacesFactoryTypedefs));

    _library = library;
  }

  final _enums = <Code>[];
  final _interfaces = <Class>[];
  final _interfacesFactoryTypedefs = <Code>[];
  final _interfacesTypedefs = <Code>[];
  final _structs = <Class>[];
  final _strings = <Class>[];
  final _ints = <Class>[];
  final _floats = <Class>[];
  final _structsExtension = <Code>[];
  Library _library;

  factory IdlDart.generate(IdlObject idlObject) {
    return IdlDart()
      .._addEnums(idlObject)
      .._addStructs(idlObject)
      .._addInterfaces(idlObject)
      .._addStrings(idlObject)
      .._addInts(idlObject)
      .._addFloats(idlObject)
      .._finalize(idlObject);
  }

  @override
  String toString() {
    final emitter = DartEmitter();
    return DartFormatter(pageWidth: 8000)
        .format('${_library.accept(emitter)}')
        .trim();
  }
}
